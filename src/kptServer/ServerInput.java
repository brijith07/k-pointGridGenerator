package kptServer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import abinit.AbinitInfile2;
import matsci.io.vasp.POSCAR;
import matsci.location.symmetry.operations.SpaceGroup;
import matsci.structure.Structure;
import matsci.structure.symmetry.SymmetryException;
import qe.QENamelists;

//import qe.QEInputFile;

/**
 * This is a class to store the parameters for a KPOINTS file generation.
 * Keeping it tidy by preferring raw input. Expand this as the number of parameters expands.
 * No data processing should be done here.
 * @author Pandu Wisesa
 *
 */

public class ServerInput {
	
	private double m_MinDistance;
	private boolean m_DefaultMinDistance;
	private String m_IncludeGamma;
	private boolean m_UseDefaultIncludeGamma;
	private String m_Header;
	private boolean m_UseDefaultParams;
	private boolean m_ForceDiagonal;
	private boolean m_BetaMode;
	private boolean m_UseMinTotalPoints;
	// If true, try to detect gap, even if GAPDISTANCE is not specified.
	private boolean m_GapAdjusted; 
	private boolean m_MinDistPrompted;
	private boolean m_MinTotPrompted;
	private boolean m_KPPRAPrompted;
	private boolean m_GapPrompted;
	//private boolean m_UseAutoMode;
	private boolean m_UseKPPRA;
	// BETA_MODE functionality: minimize with diagonal optimizer.
	private boolean m_MinWDiag;
	private boolean m_UsedSelectiveDynamics;
	private boolean m_SeparateOutput;
	private boolean m_FatalINCARProblem;
	private boolean m_INCARIncluded;
	private String m_Format;
	private String m_ClientVersion;
	private String m_FilePath;
	private String m_RemoveSymmetry;
	private String m_RemoveSymmetryUsed;
	private boolean m_RemoveSymmetryProvided;
	private int m_MinTotalPoints;
	private int m_KPPRA;
	private int m_GapReducedMinTotalPoints;
	// The number of periodic dimensions. E.g. a slab structure only has 2 periodic dimensions.
	private int m_RealDimension;
	private double m_GapDistance;
	private int m_LowSymmetrySearchDepth; // Deprecated.
    private int m_TriclinicSearchDepth;
    private int m_MonoclinicSearchDepth;
    private boolean m_LowSymmetrySearchDepthProvided; 
    private boolean m_TriclinicSearchDepthProvided; // whether this flag has been provided.
    private boolean m_MonoclinicSearchDepthProvided;
	private POSCAR m_POSCAR;
	private INCAR m_INCAR;
	private SpaceGroup m_AlteredSpaceGroup;
	private Structure m_AlteredStructure;
	private long m_GridGenerationTime;
	private long m_TotalGenerationTime;
	private ArrayList<String> m_Unparseable;
	private ArrayList<String> m_Unrecognizeable;
	private AbinitInfile2 m_Abinit;
	//private QEInputFile m_QE; //obsolete, deprecated
	private QENamelists m_QEData;
	private boolean m_UseSuperToDirect;
	private String m_FileName;
	private Long m_GapAdjustTime;
	private Long m_MagAdjustTime;
	private Long m_SelectiveDynAdjustTime;
	private Long m_ReadINCARTime;
	private Long m_ReadStructureTime;
	private Long m_ReadPRECALCTime;
	private Long m_KPOINTSIOTime;
	private Long m_SetUpTime;
	private Long m_MAGMOMNCLAdjustTime;
	private double m_UsedMinDistance;
	private boolean m_PrecisionAdjusted;
	private boolean m_KPointsAuto; // If true, write generating vectors in output.
	private int m_ScalingTimes;
	
	public ServerInput() {
		//CartesianBasis.setPrecision(3E-6); //precision, 3 times as accurate as vasp 1/3 less accurate than what most people would set
		//CartesianBasis.setAngleToleranceDegrees(1E-3);
		m_MinDistance = 28.1;
		m_UsedMinDistance = 28.1;
		m_GapDistance = 7;
		m_LowSymmetrySearchDepth = 729;
        m_TriclinicSearchDepth = 729;
        m_MonoclinicSearchDepth = 1728;
		m_MinTotalPoints = 1;
		m_KPPRA = 1;
		m_BetaMode = false;
		m_GapAdjusted = true; 
		m_UseMinTotalPoints = false;
		m_UseKPPRA = false;
		m_IncludeGamma = "AUTO";
		m_Header = "SIMPLE";
		m_ForceDiagonal = false;
		m_UseDefaultParams = true;
		m_DefaultMinDistance = true;
		m_UseDefaultIncludeGamma = true;
		//m_UseAutoMode = false;
		m_MinDistPrompted = false;
		m_MinTotPrompted = false;
		m_KPPRAPrompted = false;
		m_GapPrompted = false;
		m_LowSymmetrySearchDepthProvided = false;
        m_TriclinicSearchDepthProvided = false;
        m_MonoclinicSearchDepthProvided = false;
		m_UsedSelectiveDynamics = false;
		m_SeparateOutput = true; // stand-alone application default to true. 
		m_RealDimension = 3;
		m_Format = "-vasp"; //VASP is set as the default because it is the first packaged we support, so the older users get backwards compatibility.
		m_ClientVersion = "C2020.11.25"; //set explicitly for stand-alone version;
		m_INCARIncluded = false;
		m_RemoveSymmetry = "none";
		m_RemoveSymmetryUsed = "none";
		m_RemoveSymmetryProvided = false;
		m_UseSuperToDirect = true;
		m_GapAdjustTime = null;
		m_MagAdjustTime = null;
		m_SelectiveDynAdjustTime = null;
		m_KPointsAuto = false;
		m_ScalingTimes = 1;
	}
	
	public void selectiveDynamicsIsUsed(boolean used) {
		m_UsedSelectiveDynamics = used;
	}
	public boolean usingSelectiveDynamics() {
		return m_UsedSelectiveDynamics;
	}
	
	public void setMinDistance(double value) {
		m_MinDistance = value;
		m_UsedMinDistance = value; //this is set here to initialize
		m_UseDefaultParams = false;
		m_DefaultMinDistance = false;
	}
	
	public double getMinDistance() {
		return m_MinDistance;
	}
	
	public void setShift(String includeGamma) {
		m_IncludeGamma = includeGamma;
		m_UseDefaultParams = false;
		m_UseDefaultIncludeGamma = false;
	}
	
	public String includeGamma() {
		return m_IncludeGamma;
	}
	public void setHeaderVerbosity (String verbose) {
		m_Header = verbose;
		m_UseDefaultParams = false;
	}
	public String headerVerbosity() {
		return m_Header;
	}
	public void forceSetDefault(boolean defaultPrms){
		m_UseDefaultParams = defaultPrms;
	}
	public boolean isDefault() {
		return m_UseDefaultParams;
	}
	public void setPOSCAR(POSCAR inPoscar) {
		m_POSCAR = inPoscar;
	}
	public POSCAR getPOSCAR() {
		return m_POSCAR;
	}
	public void setINCAR(INCAR incar) {
		m_INCAR = incar;
	}
	public INCAR getINCAR() {
		return m_INCAR;
	}
	public void setForceDiagonal(boolean diagonal) {
		m_ForceDiagonal = diagonal;
		m_UseDefaultParams= false;
	}
	public boolean isDiagonalGrid() {
		return m_ForceDiagonal;
	}
	public boolean defaultMinDistance() {
		return m_DefaultMinDistance;
	}
	public boolean defaultIncludeGamma() {
		return m_UseDefaultIncludeGamma;
	}
	public boolean isBetaMode(){
		return m_BetaMode;
	}
	public void useBetaMode(boolean betaModeOn) {
		m_BetaMode = betaModeOn;
		m_UseDefaultParams=false;
	}
	public double gapDistance() {
		return m_GapDistance;
	}
	public void setGapDistance(double gap) {
		m_GapDistance = gap;
		m_UseDefaultParams=false;
		m_GapAdjusted = true;
	}
	
	public void setLowSymmetrySearchDepth(int depth) {
	    this.m_LowSymmetrySearchDepth = depth;
	}
	
	public int getLowSymmetrySearchDepth() {
	    return this.m_LowSymmetrySearchDepth;
	}
	   
    public void setTriclinicSearchDepth(int depth) {
        this.m_TriclinicSearchDepth = depth;
    }
    
    public int getTriclinicSearchDepth() {
        return this.m_TriclinicSearchDepth;
    }
    
    public void setMonoclinicSearchDepth(int depth) {
        this.m_MonoclinicSearchDepth = depth;
    }
    
    public int getMonoclinicSearchDepth() {
        return this.m_MonoclinicSearchDepth;
    }

	public int getMinTotalPoints() {
		return m_MinTotalPoints;
	}
	public void setMinTotalPoints(int minTot) {
		m_MinTotalPoints = minTot;
		m_UseDefaultParams=false;
		m_UseMinTotalPoints = true;
	}
	public boolean minTotalPointsUsed() {
		return m_UseMinTotalPoints;
	}
	public boolean gapAdjusted() {
		return m_GapAdjusted;
	}
	/*public boolean usingAutoMode(){
		return m_UseAutoMode;
	}
	public void useAutoMode(boolean setAutoMode) {
		m_UseAutoMode = setAutoMode;
	}*/
	public boolean usingKPPRA() {
		return m_UseKPPRA;
	}
	public int getKPPRA() {
		return m_KPPRA;
	}
	public void setKPPRA(int KPPRA){
		m_KPPRA = KPPRA;
		m_UseDefaultParams=false;
		m_UseKPPRA = true;
	}
	public void promptMinDist(boolean prompt) {
		m_MinDistPrompted = prompt;
	}
	public void promptMinTot(boolean prompt) {
		m_MinTotPrompted = prompt;
	}
	public void promptKPPRA(boolean prompt) {
		m_KPPRAPrompted = prompt;
	}
	public void promptGap(boolean prompt) {
		m_GapPrompted = prompt;
	}
	
	public void promptLowSymmetrySearchDepth(boolean prompt) {
	    this.m_LowSymmetrySearchDepthProvided = prompt;
	}
	
	public void promptTriclinicSearchDepth(boolean prompt) {
	    this.m_TriclinicSearchDepthProvided = prompt;
    }
	   
    public void promptMonoclinicSearchDepth(boolean prompt) {
        this.m_MonoclinicSearchDepthProvided = prompt;
    }
	    
	public boolean isMinDistProvided() {
		return m_MinDistPrompted;
	}
	public boolean isMinTotProvided() {
		return m_MinTotPrompted;
	}
	public boolean isKPPRAProvided() {
		return m_KPPRAPrompted;
	}
	public boolean IsGapPrompted() {
		return m_GapPrompted;
	}
	public int getGapReducedMinTotalPoints() {
		return m_GapReducedMinTotalPoints;
	}
	public void setGapReducedMinTotalPoints(int reducedPoints) {
		m_GapReducedMinTotalPoints = reducedPoints;
	}
	
	public boolean isLowSymmetrySearchDepthProvided() {
	    return this.m_LowSymmetrySearchDepthProvided;
	}
	   
    public boolean isTriclinicSearchDepthProvided() {
        return this.m_TriclinicSearchDepthProvided;
    }
    
    public boolean isMonoclinicSearchDepthProvided() {
        return this.m_MonoclinicSearchDepthProvided;
    }

	public boolean minimizeGridWithDiagonal() {
		return m_MinWDiag;
	}
	public void useDiagonalToMinimize(boolean use){
		m_MinWDiag = use;
	}
	public ArrayList<String> unparseableList() {
		return m_Unparseable;
	}
	public void setUnparseableList(ArrayList<String> unparseable) {
		m_Unparseable = new ArrayList<>();
		for (int a=0; a < unparseable.size(); a++) {
			m_Unparseable.add(unparseable.get(a));
		}
	}
	public void setUnrecognizeableList(ArrayList<String> unparseable) {
		m_Unrecognizeable = new ArrayList<>();
		for (int a=0; a < unparseable.size(); a++) {
			m_Unrecognizeable.add(unparseable.get(a));
		}
	}
	public ArrayList<String> unrecognizeableList() {
		return m_Unrecognizeable;
	}
	
	/*public boolean ignoreSymmetryWarning() {
		return m_InputIgnoreSymmetryWarning;
	}
	public void setIgnoreSymmetryWarning(boolean use){
		m_InputIgnoreSymmetryWarning = use;
	}*/
	
	public boolean isOutputSeparated() {
		return m_SeparateOutput;
	}
	public void separateOutput(boolean value) {
		m_SeparateOutput = value;
	}
	public int getRealDimension() {
		return m_RealDimension;
	}
	public void setRealDimension(int realDimension) {
		m_RealDimension = realDimension;
	}
	public String getFormat() {
		return m_Format;
	}
	public void setFormat(String format) {
		m_Format = format;
	}
	public String getClientVersion(){
		return m_ClientVersion;
	}
	public void clientVersion(String version) {
		m_ClientVersion = version;
	}
	public void fatalINCARProblem(boolean value) {
		m_FatalINCARProblem = value;
	}
	public boolean anyFatalINCARProblem() {
		return m_FatalINCARProblem;
	}
	public void setFilePath(String path) {
		m_FilePath = path;
	}
	public String filePath() {
		return m_FilePath;
	}
	public boolean isINCARIncluded() {
		return m_INCARIncluded;
	}
	public void includeINCAR(boolean value) {
		m_INCARIncluded = value;
	}
	public void setGridRemoveSymmetrySetting(String value) {
		m_RemoveSymmetry = value;
	}
	public String getGridRemoveSymmetrySetting() {
		return m_RemoveSymmetry;
	}
	public void setGridRemoveSymmetrySettingUsed(String value) {
		m_RemoveSymmetryUsed = value;
	}
	public String getGridRemoveSymmetrySettingUsed() {
		return m_RemoveSymmetryUsed;
	}
	public boolean gridRemoveSymmetrySettingProvided() {
		return m_RemoveSymmetryProvided;
	}
	public void setGridIgnoreSymmetryProvided(boolean value) {
		m_RemoveSymmetryProvided = value;
	}
	public long getGridGenerationTime() {
		return m_GridGenerationTime;
	}
	public void setGridGenerationTime(long value) {
		m_GridGenerationTime=value;
	}
	public long getTotalGenerationTime() {
		return m_TotalGenerationTime;
	}
	public void setTotalGenerationTime(long value) {
		m_TotalGenerationTime=value;
	}
	public void setSetUpTime(long value) {
		m_SetUpTime = value;
	}
	public long getSetUpTime(){
		return m_SetUpTime;
	}
	public AbinitInfile2 getAbinitFile() {
		return m_Abinit;
	}
	public void setAbinitFile(AbinitInfile2 abinit) {
		m_Abinit = abinit;
	}
	/*public QEInputFile getQEInputFile() {
		return m_QE;
	}
	public void putQEInputFile(QEInputFile qe) {
		m_QE = qe;
	}*/
	public QENamelists getQEData() {
		return m_QEData;
	}
	public void putQEData(QENamelists QEs) {
		m_QEData = QEs;
	}
	public Structure[] getStructure() {
		if (this.getFormat().contains("vasp")) {
			//System.out.println("0 ArithNum: " + new Structure(this.getPOSCAR()).getDefiningSpaceGroup().getArithmeticCrystalClassNumber(3));
			return new Structure[] {new Structure (this.getPOSCAR())};
		} else if (this.getFormat().contains("qe")){
			//return new Structure[] {new Structure (this.getQEInputFile())};
			return new Structure[] {new Structure(this.getQEData())};
		} else if (this.getFormat().contains("abinit")) {
			int[] dataSetNums = this.getAbinitFile().getDataSetNumbers();
			Structure[] allStructures = new Structure[dataSetNums.length];
			for (int a = 0; a < dataSetNums.length; a++) {
				allStructures[a] = new Structure(this.getAbinitFile().getStructure(dataSetNums[a]));
			}
			return allStructures;
		} else {
			return null;
		}
	}
	public boolean kPointUseSuperToDirect() {
		return m_UseSuperToDirect;
	}
	public void setKPointUseSuperToDirect(boolean useSuperToDirect) {
		m_UseSuperToDirect = useSuperToDirect;
	}
	public String getFileName() {
		return m_FileName;
	}
	public void setFileName(String fileName) {
		m_FileName = fileName;
	}
	public void setGapAdjustTime(Long time) {
		m_GapAdjustTime = time;
	}
	public Long gapAdjustTime() {
		return m_GapAdjustTime;
	}
	public void setMagMomAdjustTime(Long time) {
		m_MagAdjustTime = time;
	}
	public Long magMomAdjustTime() {
		return m_MagAdjustTime;
	}
	public void setSelectiveDynamicsAdjustTime(Long time) {
		m_SelectiveDynAdjustTime = time;
	}
	public Long selectiveDynamicsAdjustTime() {
		return m_SelectiveDynAdjustTime;
	}
	public void setMAGMOMNCLAdjustTime(Long time) {
		m_MAGMOMNCLAdjustTime = time;
	}
	public Long MAGMOMNCLAdjustTime() {
		return m_MAGMOMNCLAdjustTime;
	}

	public void setReadINCARTime(Long time) {
		m_ReadINCARTime = time;
	}
	public Long readINCARTime() {
		return m_ReadINCARTime;
	}
	public void setReadPRECALCTime(Long time) {
		m_ReadPRECALCTime = time;
	}
	public Long readPRECALCTime() {
		return m_ReadPRECALCTime;
	}
	public void setStructureTime(Long time) {
		m_ReadStructureTime = time;
	}
	public Long readStructureTime() {
		return m_ReadStructureTime;
	}
	
	public void setKPointsIOTime(Long time) {
		m_KPOINTSIOTime = time;
	}
	public Long readKPointsIOTime() {
		return m_KPOINTSIOTime;
	}
	public void setUsedMinDistance(double value) { //the used value then is overridden here
		m_UsedMinDistance = value; 
	}
	public double getUsedMinDistance() {
		return m_UsedMinDistance;
	}
	public void precisionAdjusted(boolean value) {
		m_PrecisionAdjusted = value;
	}
	public boolean isPrecisionAdjusted() {
		return m_PrecisionAdjusted;
	}
	public void setAlteredSpaceGroup(SpaceGroup alteredSpaceGroup) {
		m_AlteredSpaceGroup = alteredSpaceGroup;
	}
	
	/**
     * This method has a loop to adaptively adjust the precision to make sure a 
     * space group will be found. It is safer to be called than 
     * Structure.getDefinigSpaceGroup(). 
     *  
     * @return SpaceGroup The space group of the m_AlteredStructure.      
     */
	public SpaceGroup getAlteredSpaceGroup() {
		if (m_AlteredSpaceGroup == null) {
			if (m_AlteredStructure == null) {
				return null;
			}
			boolean processOK = false;
			int tryCounter = 0;
			while (!processOK) {
				try {
					m_AlteredSpaceGroup = m_AlteredStructure.getDefiningSpaceGroup(); //TODO Test without this//.getSymmorphicGroup();
					processOK = true;
					break;
				} catch (SymmetryException e) {
	                m_AlteredStructure = m_AlteredStructure.scaleLatticeConstant(2);
	                this.setUsedMinDistance(this.getUsedMinDistance() * 2);
	                this.precisionAdjusted(true);
	                this.setScalingTimes();
	                this.setAlteredStructure(m_AlteredStructure);
				}
				tryCounter++;
	    		if (tryCounter > 4) {
	    			throw new RuntimeException("Failed to find space group due to precision.");
	    		}
			}
		}
		return m_AlteredSpaceGroup;
	}

	public void setAlteredStructure(Structure alteredStructure) {
		m_AlteredStructure = alteredStructure;
		m_AlteredSpaceGroup = null; //to prevent an inconsistent space group
	}
	public Structure getAlteredStructure() {
		return m_AlteredStructure;
	}
	public void setKPointsAutoSetting(boolean value) {
		m_KPointsAuto = value;
	}
	public boolean kPointsAutoSetting() {
		return m_KPointsAuto;
	}
	
    public void setScalingTimes() {
        m_ScalingTimes *= 2;
    }
    
    public int getScalingTimes() {
        return m_ScalingTimes;
    }
    
	/**
	 * Writes the input parameters to a file. Make sure everything is updated as the number of params expands!
	 * @param fileName
	 */
	public void writeFile(String fileName) {
		FileWriter fileWriter;
		try {
			fileWriter = new FileWriter(fileName);
			BufferedWriter writer = new BufferedWriter(fileWriter);
			writer.write("MINDISTANCE=" + this.getMinDistance());
			writer.newLine();
			writer.write("INCLUDEGAMMA=" + this.includeGamma());
			writer.newLine();
			writer.write("HEADER=" + this.headerVerbosity());
			writer.newLine();
			writer.write("MINTOTALKPOINTS=" + this.getMinTotalPoints());
			writer.newLine();
			writer.write("GAPDISTANCE=" + this.gapDistance());
			writer.newLine();
			writer.write("KPPRA=" + this.getKPPRA());
			writer.close();
		} catch (IOException e) {
			System.err.println("Failed to write the file, please check write access and path.");
		}
	}

}
