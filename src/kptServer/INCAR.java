package kptServer;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parses VASP INCAR file from a given path or reader object. <br>
 * Arbitrary parameters can be stored and retrieved by the get*Value methods.<br>
 * Otherwise, only currently relevant parameters and their default values are hardcoded. <br>
 * 
 * @author Pandu W
 *
 */

public class INCAR {

	private ArrayList<String> m_RawINCAR;
	private LinkedHashMap<String, Object> m_INCAR;
	private LinkedHashMap<String, Object> m_Default;
	
	public INCAR() {
		m_Default = new LinkedHashMap<String, Object>();
		initializeDefaultValues(m_Default);
		initializeDefaultValues(m_INCAR);
	}
	
	public INCAR (String path) throws IOException {
		m_Default = new LinkedHashMap<String, Object>();
	    initializeDefaultValues(m_Default);
		m_RawINCAR = new ArrayList<String>();
		m_INCAR = new LinkedHashMap<String,Object>();
		FileReader reader = new FileReader(path);
		readINCAR(reader);
	}
	
	public INCAR(Reader reader) throws IOException{
		m_Default = new LinkedHashMap<String, Object>();
	    initializeDefaultValues(m_Default);
		m_RawINCAR = new ArrayList<String>();
		m_INCAR = new LinkedHashMap<String,Object>();
		readINCAR(reader);
	}
	
	private void readINCAR(Reader reader) {
		//the length of INCAR can be vary widely.
		Scanner scanner = new Scanner(reader);
		String del = "[=]";
		while (scanner.hasNextLine()) {
			String currentLine = scanner.nextLine();
			m_RawINCAR.add(currentLine);
			
			String cleanLine = stripComments(currentLine);
			if (cleanLine.length() == 0 || cleanLine == null) {
				 continue;
	     	 }
			 String[] split = cleanLine.split(del);
			 String param = split[0].trim();
			 if (param.length() == 0) {
				 continue;
			 }
			 if (split.length < 2) {
				 continue;
			 }
			 //this will store all INCAR flag/value parameters as String
			 //HOWEVER, comments with whitespace as delimiters will be included
			 //the value fetching method is supposed to return the value in its correct format
			 //VASP INCAR takes the first value stated by user.
			 if (m_INCAR.containsKey(param)) {
				 continue ;
			 }
			 m_INCAR.put(param, split[1].trim());
		}
		scanner.close();
	}
	
	public int ISYM() {
		String key = "ISYM";
		int defaultVal = (int) m_Default.get(key);
		if (!m_INCAR.containsKey(key)) {
			return defaultVal; //default
		}
		if (m_INCAR.get(key) instanceof Integer) {
			return (Integer) m_INCAR.get(key);
		}
		try {
			Integer value = getIntValue((String) m_INCAR.get(key));
			m_INCAR.put(key, value);
			return value;
		} catch (NumberFormatException e) {
			return defaultVal;
		}
	}
	
	public int ISPIN() {
		String key = "ISPIN";
		int defaultVal = (int) m_Default.get(key);
		if (!m_INCAR.containsKey(key)) {
			return defaultVal; //default
		}
		if (m_INCAR.get(key) instanceof Integer) {
			return (Integer) m_INCAR.get(key);
		}
		try {
			Integer value = getIntValue((String) m_INCAR.get(key));
			m_INCAR.put(key, value);
			return value;
		} catch (NumberFormatException e) {
			return defaultVal;
		}
	}
	
	public int ICHARG() {
		String key = "ICHARG";
		int defaultVal = (int) m_Default.get(key);
		if (!m_INCAR.containsKey(key)) {
			return defaultVal; //default
		}
		if (m_INCAR.get(key) instanceof Integer) {
			return (Integer) m_INCAR.get(key);
		}
		try {
			Integer value = getIntValue((String) m_INCAR.get(key));
			m_INCAR.put(key, value);
			return value;
		} catch (NumberFormatException e) {
			return defaultVal;
		}
	}
	
	public boolean LNONCOLLINEAR() {
		if (this.LSORBIT()) {
			return true;
		}
		String key = "LNONCOLLINEAR";
		boolean defaultVal = (boolean) m_Default.get(key);
		if (!m_INCAR.containsKey(key)){
			return defaultVal;
		}
		if (m_INCAR.get(key) instanceof Boolean) {
			return (Boolean) m_INCAR.get(key);
		}
		Boolean value = getBooleanValue((String) m_INCAR.get(key));
		if (value == null) {
			return defaultVal;
		}
		m_INCAR.put(key, value);
		return value;
	}
	
	public boolean LSORBIT() {
		String key = "LSORBIT";
		boolean defaultVal = (boolean) m_Default.get(key);
		if (!m_INCAR.containsKey(key)){
			return defaultVal;
		}
		if (m_INCAR.get(key) instanceof Boolean) {
			return (Boolean) m_INCAR.get(key);
		}
		Boolean value = getBooleanValue((String) m_INCAR.get(key));
		if (value == null) {
			return defaultVal;
		}
		m_INCAR.put(key, value);
		return value;
	}
	
	public String MAGMOMString() {
		String magVal = (String) m_INCAR.get("MAGMOM");
		if (magVal == null) {
			return null;
		}
		//This strip of code below removes the comment that is only delimited by whitespace, while retaining the input style of MAGMOM
		 //String patternStr = "[a-zA-Z]"; //the old pattern checker, remove all but alphabets, tripped up by special chars
		 String patternStr = "[^0-9*.\\s\\t]"; //reverse pattern checker, ONLY accepts numbers, periods, stars, whitespaces, and tabs; much safer
		 Pattern pattern = Pattern.compile(patternStr);
		 Matcher matcher = pattern.matcher(magVal);
		 try {
			 int index = matcher.start();
			 if (index >= 0) {
				 magVal = magVal.substring(0, index);
				 magVal.trim();
			 }
		 } catch (IllegalStateException e) {
			 //no comments in the value.
		 }
		 return magVal;
	}
	
	/**
	 * Parse the MAGMOM flag for NONCOLLINEAR calculations.
	 * @return an ArrayList with triples of doubles as magnetic moments in three directions.
	 */
	public ArrayList<Double> MAGMOM() {
		String magmomValue = this.MAGMOMString();
	    String[] magneticValues = magmomValue.split("\\s+");
	    ArrayList<Integer> specifiedAtoms = new ArrayList<Integer>();
	    ArrayList<Double> specifiedMoments = new ArrayList<Double>();
	    for (int a = 0; a < magneticValues.length; a++) {
	    	String pair = magneticValues[a];
	    	//Does it contain a multiplication?
	    	if (pair.contains("*")) {
	    		String[] atmomPair = pair.split("[*]");
	    		try {
		    		Integer at = Integer.parseInt(atmomPair[0]);
		    		specifiedAtoms.add(at);
		    		specifiedMoments.add(Double.parseDouble(atmomPair[1]));
	    		} catch (NumberFormatException | NullPointerException e) {
	    			throw new NumberFormatException(
	    			        "MAGMOM values are not directly adjacent to \"*\".");
	    		}
	    	} else { //No, then it is the moment.
	    		specifiedAtoms.add(1);
	    		specifiedMoments.add(Double.parseDouble(pair));
	    	}
	    }
    	//Turn the values into individual numbers
    	ArrayList<Double> momentValues = new ArrayList<>();
    	for (int a = 0; a < specifiedAtoms.size(); a++) {
    		int specifiedAtomNum = specifiedAtoms.get(a);
    		double currentVal = specifiedMoments.get(a);
    		for (int b = 0; b < specifiedAtomNum; b++) {
    			momentValues.add(currentVal);
    		}
    	}
		return momentValues;
	}
	
	/**
	 * @return the string containing the three doubles indicating the spin quatization axis.
	 */
	public String SAXIS() {
		String saxisVal = (String) m_INCAR.get("SAXIS");
		if (saxisVal == null) {
			return null;
		}
		// This strip of code below removes the comment that is only delimited by whitespace, 
		// while retaining the input style of MAGMOM.
		// Reverse pattern checker. 
		// ONLY accepts numbers, periods, stars, whitespaces, and tabs; much safer
		String patternStr = "[^0-9*.\\s\\t]"; 
		Pattern pattern = Pattern.compile(patternStr);
		Matcher matcher = pattern.matcher(saxisVal);
		try {
	        int index = matcher.start();
	        if (index >= 0) {
	            saxisVal = saxisVal.substring(0, index);
	            saxisVal.trim();
	        }
		} catch (IllegalStateException e) {
		    //no comments in the value.
		}
		return saxisVal;
	}
	
	public Integer IBRION() {
		return this.getIntegerValue("IBRION", 0);
	}
	
	public int getIntegerValue(String key, int defaultValue) {
		if (!m_INCAR.containsKey(key)) {
			return defaultValue; //default
		}
		if (m_INCAR.get(key) instanceof Integer) {
			return (Integer) m_INCAR.get(key);
		}
		try {
			Integer value = getIntValue((String) m_INCAR.get(key));
			m_INCAR.put(key, value);
			return value;
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}
	
	public double getDoubleValue(String key, double defaultValue) {
		if (!m_INCAR.containsKey(key)) {
			return defaultValue; //default
		}
		if (m_INCAR.get(key) instanceof Double) {
			return (Double) m_INCAR.get(key);
		}
		try {
			Double value = getDoubleValue((String) m_INCAR.get(key));
			m_INCAR.put(key, value);
			return value;
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}
	
	public boolean getBooleanValue(String key, Boolean defaultValue) {
		if (!m_INCAR.containsKey(key)){
			return defaultValue;
		}
		if (m_INCAR.get(key) instanceof Boolean) {
			return (Boolean) m_INCAR.get(key);
		}
		Boolean value = getBooleanValue((String) m_INCAR.get(key));
		if (value == null) {
			return defaultValue;
		}
		m_INCAR.put(key, value);
		return value;
	}
	
	public String getStringValue(String key, String defaultValue) {
		if (!m_INCAR.containsKey(key)){
			return defaultValue;
		}
		String value = getStringValue((String) m_INCAR.get(key));
		if (value == null) {
			return defaultValue;
		}
		m_INCAR.put(key, value);
		return value;
	}
	
	public boolean keyExists(String key) {
		if (m_INCAR.containsKey(key)) {
			return true;
		}
		return false;
	}
	/**
	 * A simple way of storing values, do note that any existing values will be replaced. <br>
	 * The stored value will need to be casted as String because String will encapsulate the value as-is. <br>
	 * @param flag The parameter
	 * @param value The value
	 */
	public void setValue(String flag, String value) {
		m_INCAR.put(flag, value);
	}
	
	private Integer getIntValue(String value) {
		//VASP can use whitespace as comment delimiter
		String[] split = value.split("\\s+");
		if (split.length == 0) {
			throw new NumberFormatException();
		}
		Integer val = Integer.parseInt(split[0].trim());
		return val;
	}
	private Double getDoubleValue(String value) {
		//VASP can use whitespace as comment delimiter
		String[] split = value.split("\\s+");
		if (split.length == 0) {
			throw new NumberFormatException();
		}
		Double val = Double.parseDouble(split[0].trim());
		return val;
	}
	private Boolean getBooleanValue(String value) {
		//VASP can use whitespace as comment delimiter
		//remove periods (e.g. .TRUE.)
		value = value.replaceAll("\\.", "");
		String[] split = value.split("\\s+");
		if (split.length == 0) {
			return null;
		}
		Boolean val = Boolean.parseBoolean(split[0].trim());
		return val;
	}
	private String getStringValue(String value) {
		//VASP can use whitespace as comment delimiter
		String[] split = value.split("\\s+");
		if (split.length == 0) {
			return null;
		}
		String val = split[0].trim();
		return val;
	}
	
	private void initializeDefaultValues(LinkedHashMap<String, Object> map) {
		map.put("ISYM", 2);
		map.put("LSORBIT", false);
		map.put("LNONCOLLINEAR", false);
		map.put("ISPIN", 1);
		map.put("MAGMOM", null);
		map.put("ICHARG", 2);
	}
	
	/*public int symmetrySettingVASP() {
		return m_VASPISYM;
	}
	public void setVASPSymmetrySetting(int use){
		m_VASPISYM = use;
	}
	public boolean VASPsymmetrySettingTriggered() {
		return m_VASPISYMTriggered;
	}
	public void triggerVASPSymmetrySetting(boolean use){
		m_VASPISYMTriggered = use;
	}
	public boolean magneticMomentSpecified() {
		return m_UseMagneticMoment;
	}
	public void useMagneticMoment(boolean use){
		m_UseMagneticMoment = use;
	}
	public void setISYMProblem(boolean value) {
		m_InvalidVASPISYM = value;
	}
	public boolean isISYMInvalid() {
		return m_InvalidVASPISYM;
	}
	public void setSpinOrbitCoupling(boolean value) {
		m_UseSpinOrbitCoupling = value;
	}
	public boolean useSpinOrbitCoupling() {
		return m_UseSpinOrbitCoupling;
	}
	public void setSpinOrbitCouplingTriggered(boolean value) {
		m_UseSpinOrbitCouplingTriggered = value;
	}
	public boolean useSpinOrbitCouplingTriggered() {
		return m_UseSpinOrbitCouplingTriggered;
	}
	public void setNonCollinearUse(boolean value) {
		m_NonCollinear = value;
	}
	public boolean useNonCollinear() {
		return m_NonCollinear;
	}
	public void setNonCollinearTriggered(boolean value) {
		m_NonCollinearTriggered = value;
	}
	public boolean useNonCollinearTriggered() {
		return m_NonCollinearTriggered;
	}
	public void setSAxis(boolean value) {
		m_SAxis = value;
	}
	public boolean useSAxis() {
		return m_SAxis;
	}
	public void setSAxisTriggered(boolean value) {
		m_SAxisTriggered = value;
	}
	public boolean useSAxisTriggered() {
		return m_SAxisTriggered;
	}
	public int ISPIN() {
		return m_ISpin;
	}
	public void setISPIN(int value) {
		m_ISpin = value;
	}
	public void setISPINTriggered(boolean value) {
		m_ISPINTriggered = value;
	}
	public boolean useISPINTriggered() {
		return m_ISPINTriggered;
	}
	public void storeMagneticMoments(String magneticMoments) {
		m_MagneticMoments = magneticMoments;
	}
	public String magneticMoments() {
		return m_MagneticMoments;
	}
	//These 2 flags are for cases when symmetry is on, but LSORBIT or LNONCOLLINEAR is used.
	//Those cases are not currently supported.
	//TODO: When those cases are supported, these flags are to be removed.
	public void setLSorbitWithoutSymmetryOff(boolean value) {
		m_LSOrbitWithoutSymmetry = value;
	}
	public boolean LSorbitWithoutSymmetryOff() {
		return m_LSOrbitWithoutSymmetry;
	}
	public void setLNoncollinearWithoutReducedSymmetry(boolean value) {
		m_LNoncollinearWithoutReducedSymmetry = value;
	}
	public boolean LNoncollinearWithoutReducedSymmetry() {
		return m_LNoncollinearWithoutReducedSymmetry;
	}*/
	
	public void writeFile (String fileName) {
		try {
		      FileWriter fileWriter = new FileWriter(fileName);
		      BufferedWriter writer = new BufferedWriter(fileWriter);
		      this.writeINCAR(writer);
		      writer.flush();
		      writer.close();
		    } catch (IOException e) {
		      throw new RuntimeException("Failed to write file " + fileName, e);
		    } 
		
	}
	private void writeINCAR (Writer writer) throws IOException {
		for (Map.Entry<String, Object> entry : m_INCAR.entrySet()) {
		    String key = entry.getKey();
		    String value = (String) entry.getValue();
		    writer.write(key + "=" + value);
		    writer.write(System.lineSeparator());
		}
	}
	
	private String stripComments(String line) {
		int index = line.indexOf("#");
	    if (index >= 0) {
	      line = line.substring(0, index);
	    }
	    int index2 = line.indexOf("!");
	    if (index2 >= 0) {
	      line = line.substring(0, index2);
	    }
	    return line;
	}
	
		
}
