package main;

//import generators.KPointLatticeGenerator;
import generators.KPointLibrary;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import generators.DynamicLatticeGenerator;
import generators.KPointLatticeGenerator;

import matsci.io.vasp.KPOINTS;
import matsci.io.vasp.POSCAR;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.symmetry.operations.SpaceGroup;
import matsci.structure.Structure;
import matsci.structure.BravaisLattice;
import matsci.structure.reciprocal.KPointLattice;
import matsci.structure.superstructure.SuperLattice;
import matsci.util.MSMath;

public class Test_KPointLatticeGenerator {
    
    private static String sprt = System.getProperty("file.separator");
    
    public static void main(String[] argv) {
        String poscar = "D:\\JHU\\Research\\Machine_Learning_For_K-point_Prediction\\"
                      + "Projects\\kplib_implementation\\orthorhombic\\56545\\POSCAR";
        String output = "D:\\JHU\\Research\\Machine_Learning_For_K-point_Prediction\\"
                      + "Projects\\kplib_implementation\\orthorhombic\\56545\\KPOINTS_debug";
        double[][] conventionalVectors = 
            {{1.7461288701411337, -5.1358805374612846, 0.0000000000000000},
             {-3.4922577402822674, 0.0000000000000000, 0.0000000000000000},
             {-0.0000000000000000, 0.0000000000000000, -4.2214935508733049}};
        debug_KPointLatticeGenerator(poscar, conventionalVectors, true, 24.9999, 1, output);
        
        String outputDir = System.getProperty("user.dir") + sprt + "tests_local" 
                         + sprt + "demo_KPointLatticeGenerator";
        demo_DynamicLatticeGenerator(outputDir);
        demo_KPointLatticeGenerator(outputDir);
    }
    
    /**
     * This routine uses the custom conventional lattice vectors to in initialize the 
     * KPointLatticeGenerator, for debugging the QE and C++ implementation of the 
     * KPointLatticeGenerator. 
     */
    private static void debug_KPointLatticeGenerator(String poscarFile, 
            double[][] conventionalVectors, boolean includeGamma, double minDistance, 
            int minTotalKPoints, String outputFile) {
        POSCAR poscar = new POSCAR(poscarFile);
        Structure struct =  new Structure(poscar);
        // getLatticePreservingPointOperators() return operators in all dimension, 
        // while getLatticePointOperators() discard the operators that are not in the 
        // periodic planes. These two should have no difference for crystals, but use the former
        // for slabs and clusters.
        KPointLatticeGenerator generator = new KPointLatticeGenerator(
                struct.getDefiningLattice().getLatticeBasis().getBasisToCartesian(),
                conventionalVectors,
                struct.getDefiningSpaceGroup().getLatticePointOperators(true),
                (struct.getDefiningSpaceGroup().getCrystalFamily() ==
                        BravaisLattice.CrystalFamily.HEXAGONAL_3D));
        
        generator.includeGamma(includeGamma ? KPointLibrary.INCLUDE_GAMMA.TRUE
                                            : KPointLibrary.INCLUDE_GAMMA.FALSE);
        KPointLatticeGenerator.KPointLattice kptLattice = generator.getKPointLattice(
                minDistance, minTotalKPoints, Integer.MAX_VALUE, 1);
        // getSymmorphicGroup() uses the getLatticePreservingPointOperators().
        outputLattice(kptLattice, struct.getDefiningSpaceGroup().getSymmorphicGroup(), outputFile);
        
    }
    
    /**
     * Demonstrate how to use the KPointLatticeGenerator separately as a module, instead of
     * integrated with the K-Points Server.
     * 
     * @param outputDir
     */
    private static void demo_KPointLatticeGenerator(String outputDir) {
        SpaceGroup space = SpaceGroup.getSymmorphicSpaceGroup(3, 48);
        SpaceGroup symmorphicSpaceGroup = space.getSymmorphicGroup(); 
        double minPeriodicDistance = 50.0; // as demonstration.
        /**
         * Second way to denote the size of a grid. 
         * For simplicity, we test minPeriodicDistance first.
         */
        int minTotalKPoints = 1; 
        int maxTotalKPoints = Integer.MAX_VALUE; // Deal with overflow.
        int scaleFactor = 1; // For now, we don't need to translate scale factor.
        
        
        KPointLatticeGenerator generator = new KPointLatticeGenerator(symmorphicSpaceGroup);
        // Gamma-centered Grid:
        generator.includeGamma(KPointLibrary.INCLUDE_GAMMA.TRUE);
        KPointLatticeGenerator.KPointLattice latticeGamma = generator.getKPointLattice(
                minPeriodicDistance, minTotalKPoints, maxTotalKPoints, scaleFactor);
        String output = outputDir + "KPOINTS.gamma";
        outputLattice(latticeGamma, symmorphicSpaceGroup, output);

        // Shifted Grid:
        generator.includeGamma(KPointLibrary.INCLUDE_GAMMA.FALSE);
        KPointLatticeGenerator.KPointLattice latticeShifted = generator.getKPointLattice(
                minPeriodicDistance, minTotalKPoints, maxTotalKPoints, scaleFactor);
        output = outputDir + "KPOINTS.shifted";
        outputLattice(latticeShifted, symmorphicSpaceGroup, output);
    }
    
    /**
     * An IO routine to output the k-point grid in a file.
     * @param lattice
     * @param symmorphicSpaceGroup
     * @param output    Output destination.
     */
    private static void outputLattice(KPointLatticeGenerator.KPointLattice lattice, 
            SpaceGroup symmorphicSpaceGroup, String output) {
        boolean gamma = (lattice.getShift()[0] + lattice.getShift()[1] + lattice.getShift()[2] < 0.001)
                      ? true
                      : false;
        
        int [][] superToDirect = lattice.getSuperToDirect();
        double actualPeriodicDistance = lattice.getMinPeriodicDistance();
        int distinctKpts = lattice.getNumDistinctKPoints();
        
        /**
         * Create the wrapper object of KPointLattice type, which has functions to iterate over all
         * distinct kpoints.
         */
        
        // Get the real space superlattice.
        SuperLattice directSpaceSuperLattice = new SuperLattice(symmorphicSpaceGroup.getLattice(), 
                                                                superToDirect);
        // Get the reciprocal lattice of the superlattice, which is smaller than the reciprocal
        // lattice of the primitive lattice in real space.
        BravaisLattice kPointLattice = directSpaceSuperLattice.getInverseLattice();
        if (!gamma) { 
            kPointLattice = kPointLattice.translateBy(new Vector(lattice.getShift(), 
                                                      kPointLattice.getLatticeBasis()));
        }
        
        // Get the reciprocal lattice of the primitive cell in real space. The kPointLattice is
        // now a primitive cell of this lattice.
        SuperLattice reciprocalLattice = new SuperLattice(kPointLattice, 
                                                          MSMath.transpose(superToDirect));
        
        KPointLattice wrapperLattice = new KPointLattice(reciprocalLattice, 
                                                         symmorphicSpaceGroup.getOperations());
        Coordinates[] coords = wrapperLattice.getDistinctKPointCoords();
        double[] weights = wrapperLattice.getDistinctKPointWeights();
        
        // Output.
        File KPOINTS;
        if (gamma) {
            KPOINTS = new File(output);
        } else {
            KPOINTS = new File(output);
        }
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(KPOINTS))) {
            writer.write("Actual Periodic Distance: " + actualPeriodicDistance);
            writer.newLine();
            writer.write("Number of distinct kpoints: " + distinctKpts);
            writer.newLine();
            writer.write("Super to direct matrix: ");
            writer.newLine();
            writer.write(superToDirect[0][0] + " " 
                       + superToDirect[0][1] + " " 
                       + superToDirect[0][2]);
            writer.newLine();
            writer.write(superToDirect[1][0] + " " 
                       + superToDirect[1][1] + " " 
                       + superToDirect[1][2]);
            writer.newLine();
            writer.write(superToDirect[2][0] + " " 
                       + superToDirect[2][1] + " " 
                       + superToDirect[2][2]);
            writer.newLine();
            writer.write("Coordinates (fractional) and weight:");
            writer.newLine();
            double[][] cartesian2Basis = reciprocalLattice.getLatticeBasis().getCartesianToBasis();
            for (int i = 0; i < coords.length; i++) {
                double[] c = MSMath.vectorTimesMatrix(coords[i].getCartesianArray(), 
                                                      cartesian2Basis);
                c[0] -= Math.floor(c[0]); // move every coordinate to [0 ~ 1]
                c[1] -= Math.floor(c[1]);
                c[2] -= Math.floor(c[2]);
                writer.write(String.format("%15.12f %15.12f %15.12f %.2f", 
                                           c[0], c[1], c[2], weights[i]));
                writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
    
    /**
     * The DynamicLatticeGenerator is indeed a wrapper of the KPointLatticeGenerator. It takes
     * the KPointLatticeGenerator.KPointLattice object and create a 
     * matsci.structure.reciprocal.KPointLattice object. Then we wrap the KPointLattice object
     * in a IO type matsci.io.vasp.KPOINTS which outputs the coordinates, weights, and tetrahedrons.
     * To see how we do above wrappings, check DynamicLatticeGenerator.getKPointLattice().
     * @param outputDir
     */
    private static void demo_DynamicLatticeGenerator(String outputDir) {
        // Get one of the stored arithmetic crystal classes which has inversion.
        SpaceGroup space = SpaceGroup.getSymmorphicSpaceGroup(3, 48);
        SpaceGroup symmorphicSpaceGroup = space.getSymmorphicGroup(); 
        
        DynamicLatticeGenerator gamma = 
                new DynamicLatticeGenerator(KPointLibrary.INCLUDE_GAMMA.TRUE, 5832);
        DynamicLatticeGenerator shifted = 
                new DynamicLatticeGenerator(KPointLibrary.INCLUDE_GAMMA.FALSE, 5832);
        
        KPointLattice gammaLattice = gamma.getKPointLattice(symmorphicSpaceGroup, 50.0, 1, 5832);
        KPointLattice shiftedLattice =shifted.getKPointLattice(symmorphicSpaceGroup, 50.0, 1, 5832);
        
        KPOINTS gammaKPoints = new KPOINTS(gammaLattice, 
                symmorphicSpaceGroup.getLattice().getInverseLattice().getLatticeBasis());
        KPOINTS shiftedKPoints = new KPOINTS(shiftedLattice, 
                symmorphicSpaceGroup.getLattice().getInverseLattice().getLatticeBasis());
        
        String sprt = System.getProperty("file.separator");
        File path = new File(outputDir + sprt + "demo_dynamicLatticeGenerator");
        if (! path.exists()) { path.mkdirs(); }
        
        gammaKPoints.writeFile(outputDir + sprt + "KPOINTS.gamma");
        shiftedKPoints.writeFile(outputDir + sprt + "KPOINTS.shifted");
    }
    
}
