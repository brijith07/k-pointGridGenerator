package main;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.JSONException;

import generators.KPointLatticeCollection;
import generators.KPointLibrary;
import generators.LowSymmetryGenerator;
import matsci.location.basis.CartesianBasis;
import matsci.location.symmetry.operations.SpaceGroup;
import matsci.structure.symmetry.LatticeSymmetryFinder;
import matsci.structure.symmetry.StructureSymmetryFinder;

public class TransferToBinary { 

    public static void main(String[] args) {
        
        CartesianBasis.setPrecision(3E-6); 
        CartesianBasis.setAngleToleranceDegrees(1E-3);
        StructureSymmetryFinder.allowArtificialGroupCompletion(false);
        LatticeSymmetryFinder.allowArtificialGroupCompletion(false);
        System.out.println("Loading minDistanceCollection...");
        
        int[] arithNums = new int[] {1, 2, 7, 8, 18, 19, 20, 21, 26, 27, 
                36, 37, 40, 41, 48, 49, 50, 53, 58, 62, 63, 64, 71, 72, 73};
        //We need to set -DLATTICE_COLLECTIONS to the path of minDistanceCollection
        String collectionDirectory = 
                System.getProperty("LATTICE_COLLECTIONS") + 
                System.getProperty("file.separator") + 
                "minDistanceCollections";
        System.out.println(collectionDirectory);
        
        for (int a = 0; a < arithNums.length; a++) {
            int maxNumKpts = 5832;
            int numDim = 3;
            SpaceGroup space = SpaceGroup.getSymmorphicSpaceGroup(numDim, arithNums[a]);
            
            int arithNum = arithNums[a];
            if (arithNum < 9) {
                LowSymmetryGenerator gammaGenerator = new LowSymmetryGenerator (729, true, false, false);
                LowSymmetryGenerator shiftedGenerator = new LowSymmetryGenerator (729, false, false, false);
            } else {
                KPointLatticeCollection gammaCollLoad = null;
                KPointLatticeCollection shiftedCollLoad = null;
                try {
                    reWriteCollection(collectionDirectory, space, numDim, maxNumKpts, true);
                    reWriteCollection(collectionDirectory, space, numDim, maxNumKpts, false);
                } catch (JSONException e) {
                    System.err.println("WARNING!! There is a problem with the JSON format of the lattice collections!");
                    e.printStackTrace();
                    //TODO: Shouldn't we exit from here?
                    //System.exit(1);
                } catch (FileNotFoundException e) {
                    System.err.println("ERROR: Lattice Collections not found! " 
                            + System.lineSeparator()
                            + "Please check whether the LATTICE_COLLECTIONS variable is set correctly.");
                    System.exit(1);
                } catch (IOException e) {
                    System.err.println("ERROR: Fail to load the lattice collection!");
                    e.printStackTrace();
                    System.exit(1);
                }
            }
        }    
    }

    private static void reWriteCollection(String collLoc, SpaceGroup space, int numDim, int maxNumKpts, boolean includeGamma) throws JSONException, IOException {
        int arithNum = space.getArithmeticCrystalClassNumber(numDim);
        String spt = System.getProperty("file.separator");
        String shiftPath = includeGamma ? 
                spt + "Gamma" + spt + "lattices_G_" : 
                spt + "Shifted" + spt + "lattices_S_";
        String filePath = collLoc + shiftPath + arithNum + "_" + maxNumKpts;
        KPointLatticeCollection collLoad = new KPointLatticeCollection(filePath);        
        collLoad.useBinaryFile(true);
        collLoad.writeFile(filePath + "_binary");
    }
    
}
