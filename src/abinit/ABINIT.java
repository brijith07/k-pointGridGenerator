package abinit;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Scanner;

import abinit.AbinitInfile2;

public class ABINIT {
	
	private AbinitInfile2 m_AbinitStructure;
	private AbinitInputParameters m_AbinitParams;
	private AbinitKPoints m_AbinitKPoints;
	private ArrayList<String> m_FileContent;
	//I would like to return the "same" file as input only with the kpoints section being different
	private ArrayList<String> m_ReturnedContent;
	
	public ABINIT (Reader reader) {
		try {
			readFile(reader);
	    } catch (Exception e) {
	    	throw new RuntimeException("Error reading from reader " + reader, e);
		}
	}
	public ABINIT (String fileName) {
		try {
			readFile(fileName);
	    } catch (Exception e) {
	    	throw new RuntimeException("Error reading from file " + fileName, e);
		}
	}
	public void updateWithKPoints (AbinitKPoints kpoints, boolean useSuperToDirect) {
		m_AbinitKPoints = kpoints;
		ArrayList<String> appender = kpoints.toArrayList(useSuperToDirect);
		m_ReturnedContent.addAll(appender);
	}
	
	public void writeFile(String fileName) {
	    try {
	      FileWriter fileWriter = new FileWriter(fileName);
	      BufferedWriter writer = new BufferedWriter(fileWriter);
	      this.write(writer);
	      writer.flush();
	      writer.close();
	    } catch (IOException e) {
	      throw new RuntimeException("Failed to write file " + fileName, e);
	    } 
	  }
	
	public void write (Writer writer) {
		try {
			writeReturned(writer);
		} catch (IOException e) {
			throw new RuntimeException("Failed to write abinit input file.", e);
		}
	}
	
	private void writeReturned (Writer writer) throws IOException {
		for (int a = 0; a < m_ReturnedContent.size(); a++) {
			writer.write(m_ReturnedContent.get(a));
			writer.write(System.lineSeparator());
		}
	}
	
	public AbinitInfile2 getAbinitStructure() {
		return m_AbinitStructure;
	}
	
    protected void readFile(String fileName) throws IOException {
       FileReader fileReader = new FileReader(fileName);
	   this.readFile(fileReader);
	   fileReader.close();
	}
    
    protected void readFile(Reader reader) throws IOException {
		m_AbinitParams = new AbinitInputParameters(reader);
		m_FileContent = new ArrayList<String>();
		m_ReturnedContent = new ArrayList<String>();
		for (int a = 0; a < m_AbinitParams.rawInput().size(); a++) {
			m_FileContent.add(m_AbinitParams.rawInput().get(a));
			m_ReturnedContent.add(m_AbinitParams.rawInput().get(a));
		}
		//TODO: ask Tim to change the AbinitStructure constructor so it can read from ArrayList
		ArrayList<String> stringList = m_AbinitParams.rawInput();
		StringBuilder buffer = new StringBuilder();
		for(String current : stringList) {
		    buffer.append(current).append("\n");
		}

		BufferedReader br = new BufferedReader(new StringReader(buffer.toString()));
		m_AbinitStructure = new AbinitInfile2(br);
		/*
		for (int a = 0; a < m_FileContent.size(); a++) {
			m_ReturnedContent.add(m_FileContent.get(a));
		}*/
    }
	
	private class AbinitInputParameters {
		
		private ArrayList<String> m_RawInput;
		private LinkedHashMap<String, Object> m_Param;
		
		private AbinitInputParameters(Reader reader) {
			m_RawInput = new ArrayList<String>();
			m_Param = new LinkedHashMap<String,Object>();
			readFile(reader);
		}
		
		public int getIntegerValue(String key, int defaultValue) {
			if (!m_Param.containsKey(key)) {
				return defaultValue; //default
			}
			if (m_Param.get(key) instanceof Integer) {
				return (Integer) m_Param.get(key);
			}
			try {
				Integer value = getIntValue((String) m_Param.get(key));
				m_Param.put(key, value);
				return value;
			} catch (NumberFormatException e) {
				return defaultValue;
			}
		}
		
		public double getDoubleValue(String key, double defaultValue) {
			if (!m_Param.containsKey(key)) {
				return defaultValue; //default
			}
			if (m_Param.get(key) instanceof Double) {
				return (Double) m_Param.get(key);
			}
			try {
				Double value = getDoubleValue((String) m_Param.get(key));
				m_Param.put(key, value);
				return value;
			} catch (NumberFormatException e) {
				return defaultValue;
			}
		}
		
		public boolean getBooleanValue(String key, Boolean defaultValue) {
			if (!m_Param.containsKey(key)){
				return defaultValue;
			}
			if (m_Param.get(key) instanceof Boolean) {
				return (Boolean) m_Param.get(key);
			}
			Boolean value = getBooleanValue((String) m_Param.get(key));
			if (value == null) {
				return defaultValue;
			}
			m_Param.put(key, value);
			return value;
		}
		
		public String getStringValue(String key, String defaultValue) {
			if (!m_Param.containsKey(key)){
				return defaultValue;
			}
			String value = getStringValue((String) m_Param.get(key));
			if (value == null) {
				return defaultValue;
			}
			m_Param.put(key, value);
			return value;
		}
		
		public boolean keyExists(String key) {
			if (m_Param.containsKey(key)) {
				return true;
			}
			return false;
		}
		
		public ArrayList<String> rawInput() {
			return m_RawInput;
		}
		
		//Abinit uses whitespace as delimiter
		private Integer getIntValue(String value) {
			String[] split = value.split("\\s+");
			if (split.length == 0) {
				throw new NumberFormatException();
			}
			Integer val = Integer.parseInt(split[0].trim());
			return val;
		}
		private Double getDoubleValue(String value) {
			String[] split = value.split("\\s+");
			if (split.length == 0) {
				throw new NumberFormatException();
			}
			double isNeg = 1.0;
			String line = split[0].trim();
			//abinit recognizes sqrt and -sqrt
			if (line.contains("sqrt")) {
				if (line.contains("-")) {
					isNeg = -1.0;
					line = line.replaceAll("-","");
				}
				//removes the word "sqrt" and the brackets
				String v = line.replaceAll("sqrt", "").replaceAll("[()]","");
				Double out = parseFraction(v);
				Double ret = Math.sqrt(out) * isNeg;
				return ret;
			}
			Double val = parseFraction(line);
			Double ret = val * isNeg;
			return ret;
		}
		
		private Double parseFraction (String fraction) {
			if (!fraction.contains("/")) {
				return Double.parseDouble(fraction);
			}
			String[] split = fraction.split("/");
			Double numerator = Double.parseDouble(split[0]);
			Double denominator = Double.parseDouble(split[1]);
			if (denominator == 0) {
				throw new IllegalArgumentException("Fail to parse fraction. Argument denominator is 0");
			}
			return numerator/denominator;
		}
		private Boolean getBooleanValue(String value) {
			//remove periods (e.g. .TRUE.)
			value = value.replaceAll("\\.", "");
			String[] split = value.split("\\s+");
			if (split.length == 0) {
				return null;
			}
			Boolean val = Boolean.parseBoolean(split[0].trim());
			return val;
		}
		private String getStringValue(String value) {
			String[] split = value.split("\\s+");
			if (split.length == 0) {
				return null;
			}
			String val = split[0].trim();
			return val;
		}
		
		private String stripComments(String line) {
			int index = line.indexOf("#");
		    if (index >= 0) {
		      line = line.substring(0, index);
		    }
		    int index2 = line.indexOf("!");
		    if (index2 >= 0) {
		      line = line.substring(0, index2);
		    }
		    return line;
		}
		
		private void readFile(Reader reader) {
			Scanner scanner = new Scanner(reader);
			String del = "\\s+";
			while (scanner.hasNextLine()) {
				String currentLine = scanner.nextLine();
				m_RawInput.add(currentLine);
				
				String cleanLine = stripComments(currentLine);
				if (cleanLine.length() == 0 || cleanLine == null) {
					 continue;
		     	 }
				 String[] split = cleanLine.split(del);
				 if (split.length == 0) {
					 continue;
				 }
				 String param = split[0].trim();
				 if (param.length() == 0) {
					 continue;
				 }
				 if (split.length < 2) {
					 continue;
				 }
				 //TODO: Check what happens when the same parameter is specified twice
				 if (m_Param.containsKey(param)) {
					 continue ;
				 }
				 m_Param.put(param, split[1].trim());
			}
			scanner.close();
		}
		
	}

}
