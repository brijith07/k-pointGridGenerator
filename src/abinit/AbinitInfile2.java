/*
 * Created on Nov 14, 2005
 *
 */
package abinit;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;
import java.io.Writer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.StringTokenizer;

import matsci.Species;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.AbstractBasis;
import matsci.location.basis.LinearBasis;
import matsci.location.basis.CartesianBasis;
import matsci.location.basis.CartesianScaleBasis;
import matsci.structure.IStructureData;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;

public class AbinitInfile2 implements IStructureData {

  private String m_Description = "Unknown System";
  private double m_FormatPrecision = 1E-15;
  //private CartesianScaleBasis m_BohrBasis = new CartesianScaleBasis(0.529177249); // Not sure where this came from.  Used before 4/22/2016.
  private CartesianScaleBasis m_BohrBasis = new CartesianScaleBasis(0.52917720859); // From ABINIT 7.10.5
  
  private boolean[] m_IsVectorPeriodic = new boolean[] {true, true, true}; // Default value.
  //private LinearBasis  m_DirectBasis;
  
  // TODO probably move this all to the m_AllValues arrays and dynamically extract structure info
  // Printing a file should just print this array.
  //private Species[] m_Species;
  //private int[] m_NumSitesPerSpecies;
  //private Coordinates[] m_SiteCoords;
  //private Vector[] m_CellVectors;
  //private double[] m_ScaleFactors;
  
  // Each entry represents a separate data set
  // They don't necessarily have to be consecutively numbered.  Ugh.
  private HashMap<String, HashMap<String, double[]>> m_ValuesByDataset = new HashMap<String, HashMap<String, double[]>>();
  private AbinitKPoints m_AbinitKPoints;
  private ArrayList<String> m_FileContent;
  //I would like to return the "same" file as input only with the kpoints section being different
  private ArrayList<String> m_ReturnedContent;
  
  
  public AbinitInfile2(IStructureData structure) {
    m_Description = structure.getDescription();
    this.readLatticeVectors(structure);
    this.readSites(structure);
    
  }
  
  public AbinitInfile2(String fileName) {
    
    try {
      m_ReturnedContent = new ArrayList<String>();
      m_FileContent = new ArrayList<String>();
      FileReader reader = new FileReader(fileName);
      this.readFile(reader);
      reader.close();
    } catch (Exception e) {
      throw new RuntimeException("Error reading from file " + fileName + ".", e);
    }
  }
  
  public AbinitInfile2(Reader reader) {
    
    try {
      m_ReturnedContent = new ArrayList<String>();
      m_FileContent = new ArrayList<String>();
      this.readFile(reader);
    } catch (Exception e) {
      throw new RuntimeException("Error reading ABINIT input.", e);
    }
  }
  
  protected void readSites(IStructureData structure) {

    //m_Species = new Species[0];
    double[] znucl = new double[0];
    //m_NumSitesPerSpecies = new int[0];
    double[] typat = new double[structure.numDefiningSites()];
    double[] xred = new double[structure.numDefiningSites() * 3];
    LinearBasis directBasis = new LinearBasis(structure.getCellVectors());
    for (int siteNum = 0; siteNum < structure.numDefiningSites(); siteNum++) {
      Species siteSpecies = structure.getSiteSpecies(siteNum);
      int atomicNumber = siteSpecies.getElement().getAtomicNumber();
      int specIndex = ArrayUtils.findIndex(znucl, atomicNumber);
      if (specIndex < 0) {
        specIndex = znucl.length;
        znucl = ArrayUtils.appendElement(znucl, atomicNumber);
      }
      typat[siteNum] = specIndex + 1;
      double[] coordArray = structure.getSiteCoords(siteNum).getCoordArray(directBasis);
      System.arraycopy(coordArray, 0, xred, siteNum * 3, 3);
    }
    
    HashMap<String, double[]> map = this.getMapForDataset(0);
    map.put("znucl", znucl);
    map.put("xred", xred);
    map.put("typat", typat);
  }
  
  protected void readLatticeVectors(IStructureData data) {
    
    Vector[] cellVectors = data.getCellVectors();
    m_IsVectorPeriodic = data.getVectorPeriodicity();
   
    fixTripleProduct(cellVectors);
    HashMap<String, double[]> parameterMap = this.getMapForDataset(0);
    double[] rprim = new double[0];
    for (int vecNum = 0; vecNum < cellVectors.length; vecNum++) {
      rprim = ArrayUtils.appendArray(rprim, cellVectors[vecNum].getDirectionArray(m_BohrBasis));
    }
    parameterMap.put("rprim", rprim);
  }
  
  /**
   * For some reason, ABINIT insists that the triple product of the lattice vectors is positive.
   * This ensures that it is.
   *
   */
  protected static void fixTripleProduct(Vector[] cellVectors) {
    
    double tripleProduct = cellVectors[0].tripleProductCartesian(cellVectors[1], cellVectors[2]);
    if (tripleProduct < 0) {
      cellVectors[2] = cellVectors[2].multiply(-1);
    }
    
  }
  
  
  protected void readFile(Reader reader) throws IOException {
    LineNumberReader lineReader = new LineNumberReader(reader);
    
    String line = lineReader.readLine();
    String dataSetLabel = "0";
    String key = null;
    double[] values = new double[0];
    while (line != null) {
      m_FileContent.add(line);
      m_ReturnedContent.add(line);
      line = stripComments(line);
      StringTokenizer st = new StringTokenizer(line);
      while (st.hasMoreTokens()) {
        String token = st.nextToken();
        if (Character.isLetter(token.charAt(0)) && !token.startsWith("sqrt(") && !token.startsWith("-sqrt(")) {
          token = token.toLowerCase();
          if (scaleValues(values, token)) {continue;} // It's a units token
          this.storeValues(dataSetLabel, key, values);
          int dataSetIndex = findDataSetIndex(token);
          dataSetLabel = dataSetIndex < 0 ? "0" : token.substring(dataSetIndex);
          key = dataSetIndex < 0 ? token : token.substring(0, dataSetIndex);
          values = new double[0];
        } else {
          double[] newValues = parseValue(token);
          values = ArrayUtils.appendArray(values, newValues);
        }
      }
      line = lineReader.readLine();
    }
    this.storeValues(dataSetLabel, key, values);
    this.processLoops();
  }
  
  private void processLoops() {
    
    HashMap<String, double[]> defaultDataSet = this.getMapForDataset("0");
    
    // First single loops
    double[] ndtset = defaultDataSet.get("ndtset");
    if (ndtset != null) {
      HashMap<String, double[]> startMap = this.getMapForDataset(":");
      HashMap<String, double[]> addMap = this.getMapForDataset("+");
      HashMap<String, double[]> multMap = this.getMapForDataset("*");
      
      Set<String> startParams = startMap.keySet();
      Iterator<String> startIterator = startParams.iterator();
      while (startIterator.hasNext()) {
        String param = startIterator.next();
        double[] startValues = startMap.get(param);
        double[] addValues = addMap.get(param);
        double[] multValues = multMap.get(param);
        if (addValues != null) {
          for (int dataSetNum = 1; dataSetNum <= ndtset[0]; dataSetNum++) {
            HashMap<String, double[]> dataSet = this.getMapForDataset(dataSetNum);
            if (dataSet.containsKey(param)) {continue;} // Explicitly set keys always win
            double[] newArray = MSMath.arrayMultiply(addValues, dataSetNum - 1);
            newArray = MSMath.fastArrayAdd(newArray, startValues);
            dataSet.put(param, newArray);
          }
        } else if (multValues != null) {
          for (int dataSetNum = 1; dataSetNum <= ndtset[0]; dataSetNum++) {
            HashMap<String, double[]> dataSet = this.getMapForDataset(dataSetNum);
            if (dataSet.containsKey(param)) {continue;} // Explicitly set keys always win
            double[] newArray = MSMath.arrayPower(multValues, dataSetNum - 1);
            newArray = MSMath.fastArrayMultiply(newArray, startValues);
            dataSet.put(param, newArray);
          }
        }
      }
    }
    
    double[] udtset = defaultDataSet.get("udtset");
    if (udtset == null) {return;} // No double loop
    
    // First look for wildcards
    for (int dataSetNum1 = 1; dataSetNum1 <= udtset[0]; dataSetNum1++) {
      String wildCard = dataSetNum1 + "?";
      HashMap<String, double[]> wildCardMap = this.getMapForDataset(wildCard);
      Iterator<String> wildCardIterator = wildCardMap.keySet().iterator();
      while (wildCardIterator.hasNext()) {
        String param = wildCardIterator.next();
        double[] wildCardValues = wildCardMap.get(param);
        for (int dataSetNum2 = 1; dataSetNum2 <= udtset[1]; dataSetNum2++) {
          String dataSetNum = dataSetNum1 + "" + dataSetNum2;
          HashMap<String, double[]> dataSet = this.getMapForDataset(dataSetNum);
          if (dataSet.get(param) != null) {continue;} // Explicitly set params always win
          dataSet.put(dataSetNum, wildCardValues);
        }
      }
    }
    
    // Now wildcards in the second position
    for (int dataSetNum2 = 1; dataSetNum2 <= udtset[1]; dataSetNum2++) {
      String wildCard = "?" + dataSetNum2;
      HashMap<String, double[]> wildCardMap = this.getMapForDataset(wildCard);
      Iterator<String> wildCardIterator = wildCardMap.keySet().iterator();
      while (wildCardIterator.hasNext()) {
        String param = wildCardIterator.next();
        double[] wildCardValues = wildCardMap.get(param);
        for (int dataSetNum1 = 1; dataSetNum1 <= udtset[0]; dataSetNum1++) {
          String dataSetNum = dataSetNum1 + "" + dataSetNum2;
          HashMap<String, double[]> dataSet = this.getMapForDataset(dataSetNum);
          if (dataSet.get(param) != null) {continue;} // Explicitly set params always win
          dataSet.put(dataSetNum, wildCardValues);
        }
      }
    }
    
    // Now consider loops
    // With wildcards in the first position
    HashMap<String, double[]> startMap = this.getMapForDataset("?:");
    HashMap<String, double[]> addMap = this.getMapForDataset("?+");
    HashMap<String, double[]> multMap = this.getMapForDataset("?*");
    
    Set<String> startParams = startMap.keySet();
    Iterator<String> startIterator = startParams.iterator();
    while (startIterator.hasNext()) {
      String param = startIterator.next();
      double[] startValues = startMap.get(param);
      double[] addValues = addMap.get(param);
      double[] multValues = multMap.get(param);
      if (addValues != null) {
        for (int dataSetNum1 = 1; dataSetNum1 <= udtset[0]; dataSetNum1++) {
          for (int dataSetNum2 = 1; dataSetNum2 <= udtset[1]; dataSetNum2++) {
            HashMap<String, double[]> dataSet = this.getMapForDataset(dataSetNum1 + "" + dataSetNum2);
            if (dataSet.containsKey(param)) {continue;} // Explicitly set keys always win
            double[] newArray = MSMath.arrayMultiply(addValues, dataSetNum2 - 1);
            newArray = MSMath.fastArrayAdd(newArray, startValues);
            dataSet.put(param, newArray);
          }
        }
      } else if (multValues != null) {
        for (int dataSetNum1 = 1; dataSetNum1 <= udtset[0]; dataSetNum1++) {
          for (int dataSetNum2 = 1; dataSetNum2 <= udtset[1]; dataSetNum2++) {
            HashMap<String, double[]> dataSet = this.getMapForDataset(dataSetNum1 + "" + dataSetNum2);
            if (dataSet.containsKey(param)) {continue;} // Explicitly set keys always win
            double[] newArray = MSMath.arrayPower(multValues, dataSetNum2 - 1);
            newArray = MSMath.fastArrayMultiply(newArray, startValues);
            dataSet.put(param, newArray);          
          }
        }
      }
    }
    
    // With wildcards in the second position
    startMap = this.getMapForDataset(":?");
    addMap = this.getMapForDataset("+?");
    multMap = this.getMapForDataset("*?");
    
    startParams = startMap.keySet();
    startIterator = startParams.iterator();
    while (startIterator.hasNext()) {
      String param = startIterator.next();
      double[] startValues = startMap.get(param);
      double[] addValues = addMap.get(param);
      double[] multValues = multMap.get(param);
      if (addValues != null) {
        for (int dataSetNum1 = 1; dataSetNum1 <= udtset[0]; dataSetNum1++) {
          for (int dataSetNum2 = 1; dataSetNum2 <= udtset[1]; dataSetNum2++) {
            HashMap<String, double[]> dataSet = this.getMapForDataset(dataSetNum1 + "" + dataSetNum2);
            if (dataSet.containsKey(param)) {continue;} // Explicitly set keys always win
            double[] newArray = MSMath.arrayMultiply(addValues, dataSetNum1 - 1);
            newArray = MSMath.fastArrayAdd(newArray, startValues);
            dataSet.put(param, newArray);
          }
        }
      } else if (multValues != null) {
        for (int dataSetNum1 = 1; dataSetNum1 <= udtset[0]; dataSetNum1++) {
          for (int dataSetNum2 = 1; dataSetNum2 <= udtset[1]; dataSetNum2++) {
            HashMap<String, double[]> dataSet = this.getMapForDataset(dataSetNum1 + "" + dataSetNum2);
            if (dataSet.containsKey(param)) {continue;} // Explicitly set keys always win
            double[] newArray = MSMath.arrayPower(multValues, dataSetNum1 - 1);
            newArray = MSMath.fastArrayMultiply(newArray, startValues);
            dataSet.put(param, newArray);          
          }
        }
      }
    }
    
  }
  
  private boolean scaleValues(double[] values, String units) {
    
    // I got these values from the ABINIT source code v5.4.4.
    units = units.toLowerCase().trim();
    if (units.startsWith("angstr")) {
      //MSMath.arrayDivideInPlace(values, 0.5291772108); // From ABINIT 5.4.4
      MSMath.arrayDivideInPlace(values, 0.52917720859); // From ABINIT 7.10.5
    } else if (units.equals("ry")) {
      MSMath.arrayDivideInPlace(values, 2); 
    } else if (units.equals("ev")) {
      //MSMath.arrayDivideInPlace(values, 27.2113845); // From ABINIT 5.4.4
      MSMath.arrayDivideInPlace(values, 27.21138386); // From ABINIT 7.10.5
    } else if (units.equals("k")) {
      //MSMath.arrayDivideInPlace(values, 27.2113845 * 11600); // From ABINIT 5.4.4
      MSMath.arrayDivideInPlace(values, 315774.65);  // From ABINIT 7.10.5.  This is better
    } else {
      return false;
    }
    
    return true;
  }
    
  private void storeValues(String dataSetLabel, String key, double[] values) {
    if ((key != null) && (values != null) && (values.length > 0)) {
      HashMap dataSet = getMapForDataset(dataSetLabel);
      dataSet.put(key, values);
    }
  }
  
  private int findDataSetIndex(String s) {
    int returnValue = 0;
    for (returnValue = 0; returnValue < s.length(); returnValue++) {
      char currChar = s.charAt(returnValue);
      if (Character.isDigit(currChar) || currChar == '?' || currChar == ':' || currChar == '*' || currChar == '+') {
        return returnValue;
      }
    }
    return -1;
  }
  
  private HashMap<String, double[]> getMapForDataset(int dataSet) {
    return this.getMapForDataset(dataSet + "");
  }
  
  private HashMap<String, double[]> getMapForDataset(String dataSet) {
    HashMap<String, double[]> returnSet = m_ValuesByDataset.get(dataSet);
    if (returnSet == null) {
      returnSet = new HashMap<String, double[]>();
      m_ValuesByDataset.put(dataSet + "", returnSet);
    }
    return returnSet;
  }
  
  private static String stripComments(String line) {
    int index = line.indexOf("#");
    if (index >= 0) {
      line = line.substring(0, index);
    }
    
    int index2 = line.indexOf("!");
    if (index2 >= 0) {
      line = line.substring(0, index2);
    }
    
    return line;

  }
  
  public void setDescription(String description) {
    m_Description = description;
  }

  public String getDescription() {
    return m_Description;
  }

  public Vector[] getCellVectors() {
    
    /*Vector[] returnArray = new Vector[0];
    for (int vecNum = 0; vecNum < m_IsVectorPeriodic.length; vecNum++) {
      if (m_IsVectorPeriodic[vecNum]) {
        returnArray = (Vector[]) ArrayUtils.appendElement(returnArray, m_CellVectors[vecNum]);
      }
    }
    return returnArray;*/
    //return (Vector[]) ArrayUtils.copyArray(m_CellVectors);
    return this.getCellVectors(0);
    
  }
  
  public Vector[] getCellVectors(int dataSetNum) {
    
    double[][] rprimd = this.getRprimd(dataSetNum);

    Vector[] returnArray = new Vector[3];
    for (int vecNum = 0; vecNum < returnArray.length; vecNum++) {
      returnArray[vecNum] = new Vector(rprimd[vecNum], m_BohrBasis);
    }
    
    return returnArray;
  }
  
  private double[] getScalecart(int dataSetNum) {
    HashMap<String, double[]> dataSet = this.getMapForDataset(dataSetNum);
    double[] scalecart = dataSet.get("scalecart");
    if (scalecart == null) {
      return new double[] {1, 1, 1};
    } else if (scalecart.length == 1) { // The * convention was used
      return new double[] {scalecart[0], scalecart[0], scalecart[0]};
    } 
    return scalecart;
    
  }
  
  private double[] getACell(int dataSetNum) {
    HashMap<String, double[]> dataSet = this.getMapForDataset(dataSetNum);
    double[] acell = dataSet.get("acell");
    if (acell == null) {
      return (dataSetNum == 0) ? new double[] {1, 1, 1} : this.getACell(0);
    } else if (acell.length == 1) { // The * convention was used
      return new double[] {acell[0], acell[0], acell[0]};
    } 
    return acell;
    
  }
  
  private double[][] getRprimd(int dataSetNum) {
    
    double[][] rprim = this.getRprim(dataSetNum);
    double[] acell = this.getACell(dataSetNum);
    double[] scalecart = this.getScalecart(dataSetNum);
    
    for (int rowNum = 0; rowNum < rprim.length; rowNum++) {
      double[] row = rprim[rowNum];
      for (int colNum = 0; colNum < row.length; colNum++) {
        row[colNum] *= acell[rowNum] * scalecart[colNum];
      }
    }
    
    return rprim;
    
  }
  
  private double[][] getRprim(int dataSetNum) {
    
    HashMap<String, double[]> dataSet = this.getMapForDataset(dataSetNum);
    double[] rprimArray = dataSet.get("rprim");
    if (rprimArray != null) {
      return new double[][] {
          {rprimArray[0], rprimArray[1], rprimArray[2]},
          {rprimArray[3], rprimArray[4], rprimArray[5]},
          {rprimArray[6], rprimArray[7], rprimArray[8]},          
      };
    } 
    
    double[] angdeg = this.getAngdeg(dataSetNum);
    if (angdeg == null) {
      return (dataSetNum == 0) ? ArrayUtils.identityMatrixDouble(3) : this.getRprim(0);
    }
    
    double[][] rprim = new double[3][3];
    
    double ang1 = Math.toRadians(angdeg[0]);
    double ang2 = Math.toRadians(angdeg[1]);
    double ang3 = Math.toRadians(angdeg[2]);
    
    // Adapted from https://github.com/abinitgui/abinitgui/blob/master/src/parser/AbinitGeometry.java
    if (ang1 == ang2 && ang1 == ang3 && angdeg[0] != 90.0) {
      Double cosang = Math.cos(ang1);
      Double a2 = 2.0 / 3.0 * (1.0 - cosang);
      Double aa = Math.sqrt(a2);
      Double cc = Math.sqrt(1.0 - a2);
      rprim[0][0] = aa;
      rprim[0][1] = 0.0;
      rprim[0][2] = cc;
      rprim[1][0] = -0.5 * aa;
      rprim[1][1] = Math.sqrt(3.0) * 0.5 * aa;
      rprim[1][2] = cc;
      rprim[2][0] = -0.5 * aa;
      rprim[2][1] = -Math.sqrt(3.0) * 0.5 * aa;
      rprim[2][2] = cc;
    } else {
      rprim[0][0] = 1; // rprim(1,1) OK
      
      rprim[1][0] = Math.cos(ang3); // rprim(1,2) OK
      rprim[1][1] = Math.sin(ang3);
      
      rprim[2][0] = Math.cos(ang2); // rprim(1,3) OK
      rprim[2][1] = Math.cos(ang1) - rprim[1][0] * rprim[2][0] / rprim[1][1];
      rprim[2][2] = Math.sqrt(1.0 - rprim[2][0] * rprim[2][0] - rprim[2][1] * rprim[2][1]);
    }
    
    return rprim;
    
  }
  
  private double[] getAngdeg(int dataSetNum) {
    HashMap<String, double[]> dataSet = this.getMapForDataset(dataSetNum);
    double[] angdeg = dataSet.get("angdeg");
    if (angdeg == null) {
      return (dataSetNum == 0) ? null : this.getAngdeg(0);
    } else if (angdeg.length == 1) { // The * convention was used
      return new double[] {angdeg[0], angdeg[0], angdeg[0]};
    } 
    return angdeg;
    
  }
  
  
  /*
  public Vector[] getNonPeriodicVectors() {
    
    Vector[] returnArray = new Vector[0];
    for (int vecNum = 0; vecNum < m_IsVectorPeriodic.length; vecNum++) {
      if (!m_IsVectorPeriodic[vecNum]) {
        returnArray = (Vector[]) ArrayUtils.appendElement(returnArray, m_CellVectors[vecNum]);
      }
    }
    return returnArray;
  }*/
  
  public boolean[] getVectorPeriodicity() {
    return ArrayUtils.copyArray(m_IsVectorPeriodic);
  }

  public int numDefiningSites() {
    return this.numDefiningSites(0);
  }

  public Coordinates getSiteCoords(int index) {
    
    return this.getSiteCoords(0, index);
    
  }
  
  public Coordinates getSiteCoords(int dataSetNum, int index) {
    
    Coordinates[] siteCoords = this.getAllSiteCoords(dataSetNum);
    if (siteCoords != null) {
      return siteCoords[index];
    }
    return null;
    
  }
  
  public int numDataSets() {
    
    double[] ndtset = this.getMapForDataset(0).get("ndtset");
    if (ndtset != null) {return (int) Math.round(ndtset[0]);}
    
    double[] udtset = this.getMapForDataset(0).get("udtset");
    if (udtset != null) {return (int) Math.round(udtset[0] * udtset[1]);}
    
    return 0;
  }
  
  public int[] getDataSetNumbers() {
    
    int numDataSets = this.numDataSets();
    if (numDataSets == 0) {
      return new int[] {0};
    }
    
    // Make sure we cover all the possible data sets, even if 
    // the variables for the data sets aren't explicitly given.
    double[] udtset = this.getMapForDataset(0).get("udtset");
    if (udtset == null) {
      double[] jdtset = this.getMapForDataset(0).get("jdtset");
      if (jdtset == null) {
        jdtset = new double[numDataSets];
        for (int setIndex = 0; setIndex < jdtset.length; setIndex++) {
          jdtset[setIndex] = setIndex + 1;
        }
      }
      for (int setIndex = 0; setIndex < jdtset.length; setIndex++) {
        int dataSetNum = (int) Math.round(jdtset[setIndex]);
        this.getMapForDataset(dataSetNum); // This creates the dataset if it didn't already exist.
      }
    }

    Set<String> dataSetLabels = m_ValuesByDataset.keySet();
    
    int[] returnArray = new int[dataSetLabels.size()];
    int returnIndex = 0;
    
    Iterator<String> labelIterator = dataSetLabels.iterator();
    while (labelIterator.hasNext()) {
      String label = labelIterator.next();
      if (label.equals("0")) {continue;}
      try {
        returnArray[returnIndex] = Integer.parseInt(label);
        returnIndex++;
      } catch (NumberFormatException e) {
        continue;
      }
    }
    
    returnArray = ArrayUtils.truncateArray(returnArray, returnIndex);
    Arrays.sort(returnArray);
    return returnArray;
    
  }
  
  public Coordinates[] getAllSiteCoords(int dataSetNum) {
    
    HashMap<String, double[]> map = this.getMapForDataset(dataSetNum);
    
    double[] xredArray = map.get("xred");
    double[] xcartArray = map.get("xcart");
    double[] xangstArray = map.get("xangst");
    
    if (xredArray != null && xcartArray != null) {
      String error = "Found both xred and xcart";
      error = error + ((dataSetNum == 0) ? "." : " in data set number " + dataSetNum + ".");
      throw new RuntimeException(error);
    }
    
    if (xredArray != null && xangstArray != null) {
      String error = "Found both xred and xangst";
      error = error + ((dataSetNum == 0) ? "." : " in data set number " + dataSetNum + ".");
      throw new RuntimeException(error);
    }
    
    if (xcartArray != null && xangstArray != null) {
      String error = "Found both xcart and xangst";
      error = error + ((dataSetNum == 0) ? "." : " in data set number " + dataSetNum + ".");
      throw new RuntimeException(error);
    }
    
    AbstractBasis basis = null;
    double[] coordArray = null;
    if (xredArray != null) {
      coordArray = xredArray;
      basis = this.getDirectBasis(dataSetNum);
    } else if (xcartArray != null) {
      coordArray = xcartArray;
      basis = m_BohrBasis;
    } else if (xangstArray != null) {
      coordArray = xangstArray;
      basis = CartesianBasis.getInstance();
    } else if (dataSetNum != 0) {
      return this.getAllSiteCoords(0);
    } else {
      return new Coordinates[0];
    }
    
    Coordinates[] siteCoords = new Coordinates[coordArray.length / 3];
    for (int siteNum = 0; siteNum < siteCoords.length; siteNum++) {
      double[] siteCoordArray = new double[] {
          coordArray[siteNum * 3],
          coordArray[siteNum * 3 + 1],
          coordArray[siteNum * 3 + 2]
      };
      siteCoords[siteNum] = new Coordinates(siteCoordArray, basis);
    }
    
    return siteCoords;
  }
  
  /**
   * TODO cache these so they can be directly compared.
   * @param dataSetNum
   * @return
   */
  public LinearBasis getDirectBasis(int dataSetNum) {
    Vector[] cellVectors = this.getCellVectors(dataSetNum);
    return new LinearBasis(cellVectors);
  }
  
  public int numDefiningSites(int dataSetNum) {
    HashMap<String, double[]> map = this.getMapForDataset(dataSetNum);
    double[] xred = map.get("xred");
    if (xred != null) {
      return xred.length / 3;
    }
    
    double[] xcart = map.get("xcart");
    if (xcart != null) {
      return xcart.length / 3;
    }
    
    double[] xangst = map.get("xangst");
    if (xangst != null) {
      return xangst.length / 3;
    }
    
    if (dataSetNum != 0) {
      return this.numDefiningSites(0);
    }
    
    return 0;
  }

  public Species getSiteSpecies(int index) {
    return this.getSiteSpecies(0, index);
  }
  
  public Species getSiteSpecies(int dataSetNum, int index) {
    Species[] species = this.getSpecies();
    int[] typat = this.getTypat(dataSetNum);
    if (index > typat.length - 1) {
      if (typat.length == 1) { // Probably the * convention, or the default value
        return species[typat[0] - 1];
      }
      throw new RuntimeException("Unable to find species for site " + index + " with only " + typat.length + " sites specified in typat.");
    }
    return species[typat[index] - 1];
  }
  
  public int[] getTypat(int dataSetNum) {
    
    HashMap<String, double[]> map = this.getMapForDataset(dataSetNum);
    double[] typat = map.get("typat");
    if (typat != null) {
      int[] returnArray = new int[typat.length];
      for (int siteNum = 0; siteNum < returnArray.length; siteNum++) {
        returnArray[siteNum] = (int) Math.round(typat[siteNum]);
      }
      return returnArray;
    }
    
    if (dataSetNum != 0) {
      return this.getTypat(0);
    }
    
    return new int[] {1};
    
  }
  
  public Species[] getSpecies() {
    HashMap<String, double[]> map = this.getMapForDataset(0);
    double[] znucl = map.get("znucl");
    Species[] returnArray = new Species[znucl.length];
    for (int specNum = 0; specNum < znucl.length; specNum++) {
      int atomicNumber = (int) Math.round(znucl[specNum]);
      returnArray[specNum] = Species.get(atomicNumber);
    }
    return returnArray;
  }
  
  public int getNTypat() {
    HashMap<String, double[]> map = this.getMapForDataset(0);
    double[] znucl = map.get("znucl");
    return znucl.length;
  }
  
  public void writeFile(String fileName) {
    writeFile(fileName, true);
  }
  
  public void writeFile(String fileName, boolean putSitesInPrimCell) {
    
    // TODO support writing multi-datasets
    double[] acell = this.getACell(0);
    double[][] rprim = this.getRprim(0);
    LinearBasis directBasis = this.getDirectBasis(0);
    int natom = this.numDefiningSites(0);
    Coordinates[] allSiteCoords = this.getAllSiteCoords(0);
    Species[] species = this.getSpecies();
    int[] typat = this.getTypat(0);
    
    try {
      BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
      writer.write("# " + m_Description);
      newLine(writer);
      newLine(writer);
      writer.write("## Structure Definition ##");
      newLine(writer);
      
      writer.write("acell");
      for (int dimNum = 0; dimNum < acell.length; dimNum++) {
        double value = MSMath.roundWithPrecision(acell[dimNum], m_FormatPrecision);
        writer.write(" " + value);
      }
      newLine(writer);
      
      writer.write("rprim");
      this.newLine(writer);
      String formatString = "0.0000000000000000";
      if (m_FormatPrecision < 1) {
        formatString = formatString.substring(0, 2-((int) Math.round(Math.log10(m_FormatPrecision))));
      } else {
        formatString = "0";
      }
      DecimalFormat formatter = new DecimalFormat(formatString);
      //AbstractLinearBasis scaleBasis = m_BohrBasis;
      for (int vecNum = 0; vecNum < rprim.length; vecNum++) {
        double[] vec = rprim[vecNum];
        writer.write("   ");
        for (int coordNum = 0; coordNum < 3; coordNum++) {
          double value = vec[coordNum] / acell[coordNum];
          value = MSMath.roundWithPrecision(value, m_FormatPrecision);
          String output = formatter.format(value);
          writer.write("  ");
          writer.write(output);
        }
        newLine(writer);
      }
      
      writer.write("ntypat " + species.length);
      newLine(writer);
      
      writer.write("znucl");
      for (int specNum = 0; specNum < species.length; specNum++) {
        writer.write(" " + species[specNum].getElement().getAtomicNumber());
      }
      newLine(writer);
      
      writer.write("natom " + natom);
      newLine(writer);
      
      writer.write("typat");
      newLine(writer);
      String line = "";
      for (int siteNum = 0; siteNum < typat.length; siteNum++) {
        line = line  + " " + typat[siteNum];
        if (line.length() > 80) { // ABINIT doesn't allow lines longer than 132 characters.  
          writer.write(line);
          newLine(writer);
          line = "";
        }
      }
      writer.write(line);
      newLine(writer);
      
      writer.write("xred");
      newLine(writer);
      for (int siteNum = 0; siteNum < allSiteCoords.length; siteNum++) {
        Coordinates siteCoords = allSiteCoords[siteNum].getCoordinates(directBasis);
        writer.write(" ");
        for (int dimNum = 0; dimNum < siteCoords.numCoords(); dimNum++) {
          double value = MSMath.roundWithPrecision(siteCoords.coord(dimNum), m_FormatPrecision);
          if (putSitesInPrimCell) {value -= Math.floor(value);}
          writer.write("  " + formatter.format(value));
        }
        newLine(writer);
      }
      newLine(writer);
      writer.close();
    } catch (IOException e) {
      throw new RuntimeException("Failed to write file", e);
    } 
  }
  
	public void updateWithKPoints (AbinitKPoints kpoints, boolean useSuperToDirect, boolean messageList) {
		m_AbinitKPoints = kpoints;
		ArrayList<String> appender = kpoints.toArrayList(useSuperToDirect);
		ArrayList<String> commented = new ArrayList<String>();
		commented.add(kpoints.getHeader()); //First add the "header" that's to be split
		commented.addAll(commentKPointRelatedLines(m_ReturnedContent)); //then the commented file
		commented.addAll(appender); //finally the k-points information
		//shallow, should be ok //TODO: Check if this is true
		m_ReturnedContent = commented;
		//m_ReturnedContent.addAll(appender);
	}
	
	public void writeFileWithReturnedContent(String fileName) {
	    try {
	      FileWriter fileWriter = new FileWriter(fileName);
	      BufferedWriter writer = new BufferedWriter(fileWriter);
	      this.writeWithReturnedContent(writer);
	      writer.flush();
	      writer.close();
	    } catch (IOException e) {
	      throw new RuntimeException("Failed to write file " + fileName, e);
	    } 
	  }
	
	public void writeWithReturnedContent (Writer writer) {
		try {
			writeReturned(writer);
		} catch (IOException e) {
			throw new RuntimeException("Failed to write abinit input file.", e);
		}
	}
	
	private void writeReturned (Writer writer) throws IOException {
		for (int a = 0; a < m_ReturnedContent.size(); a++) {
			writer.write(m_ReturnedContent.get(a));
			writer.write(System.lineSeparator());
		}
	}
	
	private ArrayList<String> commentKPointRelatedLines(ArrayList<String> returnedContent) {
		ArrayList<String> ret = new ArrayList<String>();
		//String[] skipList = new String[] {"ngkpt", "kptopt", "shiftk", "shiftk", "kptrlen", "kptrlatt", };
		boolean followKPt = false;
		for (int a = 0; a < returnedContent.size(); a++) {
			String line = returnedContent.get(a);
			String lineNoComments = stripComments(line);
			if (lineNoComments.trim().isEmpty()) { 
				//this is only a comment
				ret.add(returnedContent.get(a));
				continue;
			} else if (Character.isDigit(lineNoComments.trim().charAt(0))) {
				//this is a follow up value
				if (followKPt) {
					//this is a follow up value to a k-point related parameter, comment out
					ret.add(commentLine(returnedContent.get(a)));
					continue;
				} else {
					//this is an ordinary follow up value
					ret.add(returnedContent.get(a));
					continue;
				}
			} else {
				//this is a parameter
				String[] spl = lineNoComments.trim().split("\\s+");
				if (spl[0].toLowerCase().startsWith("kpt") 
						|| spl[0].toLowerCase().endsWith("shiftk") 
						|| spl[0].toLowerCase().contains("ngkpt") //this way won't comment prtkpt
						|| spl[0].toLowerCase().contains("nkpt")
						|| spl[0].toLowerCase().trim().equals("wtk")) {
				//if (lineNoComments.toLowerCase().contains("kpt") || lineNoComments.toLowerCase().contains("shift")) {
					//this is a parameter related to k-points, comment out
					ret.add(commentLine(returnedContent.get(a)));
					followKPt = true;
					continue;
				} else {
					//this is a parameter not related to k-points
					ret.add(returnedContent.get(a));
					followKPt = false;
					continue;
				}
			}
		}
		return ret;
	}
	
	private String commentLine (String line) {
		return (commentLine(line, ""));
	}
	private String commentLine(String line, String additionalComments) {
		String ret = "# " + line + " " + additionalComments; 
		return ret;
	}
  
  protected void newLine(BufferedWriter writer) throws IOException {
    writer.write("\n");
  }
  
  public void setFormatPrecision(double value) {
    m_FormatPrecision = value;
  }
  
  public double getFormatPrecision() {
    return m_FormatPrecision;
  }

  private static double[] parseValue(String s) {
    
    // If the star is the first character, we only store it once.
    // We'll leave it up to the rest of the code to figure out to repeat it.
    // TODO this will cause problems when re-writing.  Fix it.
    int starPos = s.indexOf("*");
    int numVars = starPos < 1 ? 1 : Integer.parseInt(s.substring(0, starPos));
    double[] returnArray = new double[numVars];
    double value = parseDouble(s.substring(starPos + 1));
    Arrays.fill(returnArray, value);
    return returnArray;
  }
  
  private static double parseDouble(String s) {
    
    // Deal with fractions
    int slashPos = s.indexOf("/");
    if (slashPos > -1) {
      return parseDouble(s.substring(0, slashPos)) / parseDouble(s.substring(slashPos + 1));
    }

    s = s.trim();
    
    if (s.startsWith("sqrt(")) {
      String valueString = s.substring(5, s.length() - 1);
      return Math.sqrt(parseDouble(valueString));
    }
    
    if (s.startsWith("-sqrt(")) {
      String valueString = s.substring(6, s.length() - 1);
      return -Math.sqrt(parseDouble(valueString));
    }
    
    s = s.replaceAll("[a-zA-Z]+$", ""); // This gets rid of trailing d's or f's.  (If that's even possible in an ABINIT file)
    s = s.replace("d", "e");
    s = s.replace("D", "E");
    return Double.parseDouble(s);
  }
  
  // From https://github.com/abinitgui/abinitgui/blob/master/src/parser/AbinitParser.java
  // Implements the C function atof (I think)
  /*private static double parseDouble(String s) {

    int i = 0;
    int sign = 1;
    double r = 0;     // integer part
    double f = 0;     // fractional part
    double p = 1;     // exponent of fractional part
    int state = 0;    // 0 = int part, 1 = frac part
    int nbNo = 0;     // 0 = first number, 1 = second number (should divide at the end) !
    double nb1 = 0.0;
    while ((i < s.length()) && Character.isWhitespace(s.charAt(i))) {
      i++;
    }
    if ((i < s.length()) && (s.charAt(i) == '-')) {
      sign = -1;
      i++;
    } else if ((i < s.length()) && (s.charAt(i) == '+')) {
      i++;
    }
    while (i < s.length()) {
      char ch = s.charAt(i);
      if (('0' <= ch) && (ch <= '9')) {
        if (state == 0) {
          r = r * 10 + ch - '0';
        } else if (state == 1) {
          p /= 10;
          r += p * (ch - '0');
        }
      } else if (ch == '.') {
        if (state == 0) {
          state = 1;
        } else {
            if(nbNo == 0)
            {
                return sign * r;
            }
            else
            {
                return nb1 / (sign*r);
            }
        }
      } else if ((ch == 'e') || (ch == 'E') || (ch == 'd') || (ch == 'D')) {
        long e = (int) parseLong(s.substring(i + 1), 10);
        if(s.contains("/"))
        {
            int ind = s.indexOf('/');
            e = (int) parseLong(s.substring(i+1,ind), 10);
            
            double val = parseDouble(s.substring(ind+1));
            return sign*r*Math.pow(10,e)/val;
        }
        else
        {
            return sign * r * Math.pow(10, e);
        }
      } else if ((ch == '/')) {
        nb1 = sign*r;
        nbNo = 1;
        r = 0;
        sign = 1;
      } else {
            if(nbNo == 0)
            {
                return sign * r;
            }
            else
            {
                return nb1 / (sign*r);
            }
      }
      i++;
    }
    
    if(nbNo == 0)
    {
        return sign * r;
    }
    else
    {
        return nb1 / (sign*r);
    }
  }
  
  
  // From https://github.com/abinitgui/abinitgui/blob/master/src/parser/AbinitParser.java
  // This is called by parseDouble
  private static long parseLong(String s, int base) {
  
    int i = 0;
    int sign = 1;
    long r = 0;
  
    while ((i < s.length()) && Character.isWhitespace(s.charAt(i))) {
      i++;
    }
    if ((i < s.length()) && (s.charAt(i) == '-')) {
      sign = -1;
      i++;
    } else if ((i < s.length()) && (s.charAt(i) == '+')) {
      i++;
    }
    while (i < s.length()) {
      char ch = s.charAt(i);
      if (('0' <= ch) && (ch < '0' + base)) {
        r = r * base + ch - '0';
      } else if (('A' <= ch) && (ch < 'A' + base - 10)) {
        r = r * base + ch - 'A' + 10;
      } else if (('a' <= ch) && (ch < 'a' + base - 10)) {
        r = r * base + ch - 'a' + 10;
      } else {
        return r * sign;
      }
      i++;
    }
    return r * sign;
  }*/
  
  public AbinitInfile2.Structure getStructure(int dataSetNum) {
    return new Structure(dataSetNum);
  }
  
  public class Structure implements IStructureData {
    
    private int m_DataSetNum;
    
    private Structure(int dataSetNum) {
      m_DataSetNum = dataSetNum;
    }

    public String getDescription() {
      return AbinitInfile2.this.getDescription();
    }

    public Vector[] getCellVectors() {
      return AbinitInfile2.this.getCellVectors(m_DataSetNum);
    }

    public boolean[] getVectorPeriodicity() {
      return AbinitInfile2.this.getVectorPeriodicity();
    }

    public int numDefiningSites() {
      return AbinitInfile2.this.numDefiningSites(m_DataSetNum);
    }

    public Coordinates getSiteCoords(int index) {
      return AbinitInfile2.this.getSiteCoords(m_DataSetNum, index);
    }

    public Species getSiteSpecies(int index) {
      return AbinitInfile2.this.getSiteSpecies(m_DataSetNum, index);
    }
    
  }
}

