package abinit;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.DecimalFormat;
import java.util.ArrayList;

import matsci.location.Coordinates;
import matsci.location.basis.AbstractBasis;
import matsci.location.basis.CartesianBasis;
import matsci.location.basis.LinearBasis;
import matsci.structure.BravaisLattice;
import matsci.structure.reciprocal.KPointLattice;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;

public class AbinitKPoints {
	  
	  private String m_Comment = "Abinit k-points";
	  private String m_Header = "";
	  private Coordinates[] m_Coordinates;
	  private AbstractBasis m_Basis; //Abinit uses reciprocal lattice basis and not Cartesian
	  //http://www.abinit.org/doc/helpfiles/for-v8.0/input_variables/html_automatically_generated/varbas.html#kpt
	  private double[] m_Weights;
	  private int[][] m_SuperToDirect;
	  private double[] m_Shifts;
	 // private int[][] m_Tetrahedra;
	 // private int[] m_TetrahedraWeights;
	 // private double m_TetrahedraVolumeFraction;
	  
	  
	  public AbinitKPoints(Coordinates[] coords, double[] weights) {
	    m_Coordinates = (Coordinates[]) ArrayUtils.copyArray(coords);
	    m_Weights = ArrayUtils.copyArray(weights);
	  }
	  /*Abinit uses the reciprocal space primitive translation
	   *The basis should be based on the structure 
	   *This constructor might not be the best for this class
	   * 
	  public AbinitKPoints(Coordinates[] coords, double[] weights, LinearBasis basis) {
		m_Coordinates = (Coordinates[]) ArrayUtils.copyArray(coords);
		m_Weights = ArrayUtils.copyArray(weights);
		// Shift to a Cartesian Origin because that's what VASP uses
		m_Basis = new LinearBasis(CartesianBasis.getInstance().getOrigin(), basis.getBasisToCartesian());
		    
	  }*/
	  
	  public AbinitKPoints(int[][] superToDirect, double[] shifts) {
		  m_SuperToDirect = ArrayUtils.copyArray(superToDirect);
		  m_Shifts = ArrayUtils.copyArray(shifts);
	  }
	  
	  public AbinitKPoints(KPointLattice lattice, LinearBasis basis) {
	    m_Coordinates = lattice.getDistinctKPointCoords();
	    m_Weights = lattice.getDistinctKPointWeights();
	    m_Basis = basis;
	    /*m_Tetrahedra = lattice.getDistinctTetrahedaIndices();
	    m_TetrahedraWeights = lattice.getDistinctTetrahedraWeights();*/
	    //m_Tetrahedra = lattice.getReducedTetraheda();
	    //m_TetrahedraWeights = lattice.getReducedTetrahedraWeights();
	    //m_TetrahedraVolumeFraction = 1.0 / (lattice.numTotalKPoints() * lattice.numTetPerPrimCell());
	  }
	  
	  public AbinitKPoints(KPointLattice lattice, BravaisLattice definingLattice) {
		  m_Coordinates = lattice.getDistinctKPointCoords();
		  m_Weights = lattice.getDistinctKPointWeights();
		  m_Basis = definingLattice.getInverseLattice().getLatticeBasis();
		  m_Shifts = lattice.getShiftArray(definingLattice);
		  m_SuperToDirect = lattice.getSuperToDirect(definingLattice);
	  }
	  
	  public AbinitKPoints(KPointLattice lattice) {
	    this(lattice, lattice.getReciprocalSpaceLattice().getLatticeBasis());
	  }
	  
	  public void setHeader (String header) {
		  m_Header = header;
	  }
	  
	  public String getHeader() {
		  return m_Header;
	  }
	  
	  public void setDescription(String comment) {
	    m_Comment = comment;
	  }
	  
	  /*public String setComment() {
	    return m_Comment;
	  }*/
	  
	  public ArrayList<String> toArrayList(boolean useSuperToDirect) {
		  ArrayList<String> list = new ArrayList<String>();
		  list = toStringList(useSuperToDirect);
		  return list;
	  }
	  
	  public void writeFile(String fileName, boolean useSuperToDirect) {
	    
	    try {
	      FileWriter fileWriter = new FileWriter(fileName);
	      BufferedWriter writer = new BufferedWriter(fileWriter);
	      this.writeCoordinates(writer, useSuperToDirect);
	      writer.flush();
	      writer.close();
	    } catch (IOException e) {
	      throw new RuntimeException("Failed to write file " + fileName, e);
	    } 
	  }
	  
	  public void writeCoordinates(Writer writer, boolean useSuperToDirect) {
	    try {
	      //writeManual(writer);
	      writeFromList(writer, useSuperToDirect);
	    } catch (IOException e) {
	      throw new RuntimeException("Failed to write kpoints.", e);
	    } 
	  }
	  /*
	  private void writeManual(Writer writer) throws IOException {
		writer.write("# " + m_Comment);
		newLine(writer);
	    writer.write("nkpt " + m_Coordinates.length + "");
	    this.newLine(writer);
	    
	    writer.write("kpt");
	    this.newLine(writer);
	    	    
	    String formatString = "0.00000000000000";
	    DecimalFormat formatter = new DecimalFormat(formatString);
	    
	    for (int pointNum = 0; pointNum < m_Coordinates.length; pointNum++) {
	      double[] coordArray = m_Coordinates[pointNum].getCoordArray(m_Basis);
	      // Ask Tim about this arrangement
	        for (int dimNum = 0; dimNum < coordArray.length; dimNum++) {
	          coordArray[dimNum] -= Math.floor(coordArray[dimNum]);
	        }
	        
	      writer.write(formatter.format(coordArray[0]) + " ");
	      writer.write(formatter.format(coordArray[1]) + " ");
	      writer.write(formatter.format(coordArray[2]) + " ");
	      writer.write(" # " + (pointNum + 1)); //Just here to count
	      this.newLine(writer);
	    }
	    writer.write("wtk");
	    newLine(writer);
	    for (int weightNum = 0; weightNum < m_Weights.length; weightNum++) {
	    	writer.write(m_Weights[weightNum] + " # " + (weightNum + 1));	
	    	newLine(writer);
	    }
	    // Abinit does not allow specifying of tetrahedra
	    //this.writeTetrahedra(writer);
	  }*/
	  
	  private void writeFromList(Writer writer, boolean useSuperToDirect) throws IOException {
		  ArrayList<String> list = new ArrayList<String>();
		  list = toStringList(useSuperToDirect);
		  for (int a = 0; a < list.size(); a++) {
			  writer.write(list.get(a));
			  writer.write(System.lineSeparator());
		  }
	  }
	  
	  private ArrayList<String> toStringList(boolean useSuperToDirect) {
		  ArrayList<String> list = new ArrayList<String>();
		  list.add("# " + m_Comment);
		  
		  if(useSuperToDirect) {
			  list.add("kptopt 1");
			  list.add("kptrlatt");
			  for (int a = 0; a < m_SuperToDirect.length; a++) {
				  String printLine = "";
				  for (int b = 0; b < m_SuperToDirect[a].length; b++) {
					  printLine = printLine + m_SuperToDirect[a][b] + " ";
				  }
				  list.add(printLine);
			  }
			  list.add("nshiftk 1");
			  list.add("shiftk");
			  String shiftLine = MSMath.roundWithPrecision(m_Shifts[0],0.1) + " " + MSMath.roundWithPrecision(m_Shifts[1],0.1) + " " + MSMath.roundWithPrecision(m_Shifts[2],0.1);
			  list.add(shiftLine);
		  } else {
			  list.add("kptopt 0");
			  list.add("nkpt " + m_Coordinates.length + "");
			  list.add("kpt");
			      
			  String formatString = "0.00000000000000";
			  DecimalFormat formatter = new DecimalFormat(formatString);
			    
			  for (int pointNum = 0; pointNum < m_Coordinates.length; pointNum++) {
			    double[] coordArray = m_Coordinates[pointNum].getCoordArray(m_Basis);
			      for (int dimNum = 0; dimNum < coordArray.length; dimNum++) {
			        coordArray[dimNum] -= Math.floor(coordArray[dimNum]);
			      }
			      
			    list.add(formatter.format(coordArray[0]) + " " + formatter.format(coordArray[1]) + " "
			    		+ formatter.format(coordArray[2]) + " " + " # " + (pointNum + 1));
			  }
			  list.add("wtk");
			  for (int weightNum = 0; weightNum < m_Weights.length; weightNum++) {
			  	list.add(m_Weights[weightNum] + " # " + (weightNum + 1));	
			  }
		  }
		  return list;
	  }	  
	 /* private void writeTetrahedra(Writer writer) throws IOException {
	    if (m_Tetrahedra == null) {return;}
	    writer.write("Tetrahedra");
	    this.newLine(writer);
	    writer.write(m_Tetrahedra.length + "    " + m_TetrahedraVolumeFraction);
	    this.newLine(writer);
	    for (int tetNum = 0; tetNum < m_Tetrahedra.length; tetNum++) {
	      writer.write(m_TetrahedraWeights[tetNum] + "    ");
	      int[] tet = m_Tetrahedra[tetNum];
	      for (int pointNum = 0; pointNum < tet.length; pointNum++) {
	        writer.write((tet[pointNum]+1) + "   ");
	      }
	      this.newLine(writer);
	    }
	  }*/
	  
	  private void newLine(Writer writer) throws IOException {
	    writer.write("\n");
	  }

}
