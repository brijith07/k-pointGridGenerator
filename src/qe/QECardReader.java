package qe;

import java.util.ArrayList;
import java.util.LinkedHashMap;

//import expr.Expr;
//import expr.Parser;
//import expr.SyntaxException;
import matsci.Species;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.AbstractLinearBasis;
import matsci.location.basis.CartesianBasis;
import matsci.location.basis.CartesianScaleBasis;

/**
 * Process the data from QENamelists.
 * Assume all lines already had its comments stripped.
 * @author PW
 *
 */

public class QECardReader {
	
	private String m_CoordsAtomicUnit;
	private String m_LatticeVectorUnit;
	private Species[] m_SiteSpecies;
	private Species[] m_UniqueSpecies;
	private boolean[][] m_UseSelectiveDynamics; //initiate with default = all false
	private double[][] m_AtomicCoordinates;
	private int m_NumAtoms;
	private double m_BohrScaling = 0.52917720859; //TODO: check what value used in QE
	private Vector[] m_LatticeVectors;

	
	
	public QECardReader(LinkedHashMap<String, ArrayList<String>> readData, int numAtoms, int ibrav) {
		m_NumAtoms = numAtoms;
		readAtomicSpecies(readData.get("atomic_species"));
		readAtomicPositions(readData.get("atomic_positions"));
		if (ibrav == 0) {
			readLatticeVectors(readData.get("cell_parameters"));
		}
	}
	
	private void readAtomicSpecies(ArrayList<String> species) {
		ArrayList<Species> spec = new ArrayList<Species>();
		for (int a = 0; a < species.size(); a++) {
			/*if (species.get(a).contains("atomic_species") && a != 0) {
				throw new RuntimeException("Incorrect placement of card name: ATOMIC_SPECIES");
			}*/
			if (a != 0) {
				String line = species.get(a);
				String[] split = line.split("\\s+");
				String speciesSymbol = split[0];
				spec.add(Species.get(speciesSymbol));
			}
		}
		m_UniqueSpecies = spec.toArray(new Species[spec.size()]);
	}
	
	private void readAtomicPositions(ArrayList<String> atomicPositions) {
		m_SiteSpecies = new Species[m_NumAtoms];
		m_AtomicCoordinates = new double[m_NumAtoms][3];
		m_UseSelectiveDynamics = new boolean[m_NumAtoms][3];
		for (int a = 0; a < atomicPositions.size(); a++) {
			/*if (atomicPositions.get(a).contains("atomic_positions") && a != 0) {
				throw new RuntimeException("Incorrect placement of card name: ATOMIC_POSITIONS");
			}
			if (a == 0) {
				String[] spl = atomicPositions.get(a).replaceAll("(", "").replaceAll(")", "").split("\\s+");
				String value = spl[1];
				if (spl.length == 1) {
					m_CoordsAtomicUnit = "alat";
				} else if (value.equalsIgnoreCase("crystal")) {
					m_CoordsAtomicUnit = "crystal";
				} else if (value.equalsIgnoreCase("bohr")) {
					m_CoordsAtomicUnit = "bohr";
				} else if (value.equalsIgnoreCase("angstrom")) {
					m_CoordsAtomicUnit = "angstrom";
				} else if (value.equalsIgnoreCase("alat")) {
					m_CoordsAtomicUnit = "alat";
				} else {
					m_CoordsAtomicUnit = "alat";
				}*/
			if (a == 0) {
				String value = atomicPositions.get(a);
				if (value == null) {
					m_CoordsAtomicUnit = "alat";
				} else if (value.equalsIgnoreCase("crystal")) {
					m_CoordsAtomicUnit = "crystal";
				} else if (value.equalsIgnoreCase("bohr")) {
					m_CoordsAtomicUnit = "bohr";
				} else if (value.equalsIgnoreCase("angstrom")) {
					m_CoordsAtomicUnit = "angstrom";
				} else if (value.equalsIgnoreCase("alat")) {
					m_CoordsAtomicUnit = "alat";
				} else {
					m_CoordsAtomicUnit = "alat";
				}
			} else {
			    //Split over one or more whitespaces.
				String[] spl = atomicPositions.get(a).split("\\s+");
				String speciesSymbol = spl[0]; 
				int siteNum = a - 1;
				m_SiteSpecies[siteNum] = Species.get(speciesSymbol);
				for (int coordNum = 1; coordNum < 4; coordNum++) {
					/*
				    Expr expr;
					try {
					    expr = Parser.parse(spl[coordNum].trim()); 
					} catch (SyntaxException e) {
					    throw new RuntimeException(e.explain());
					}
					m_AtomicCoordinates[siteNum][coordNum-1] = expr.value();
					*/
				    //TODO: implement the home-made expression parser.
				    try {
				    m_AtomicCoordinates[siteNum][coordNum-1] = 
				            Double.parseDouble(spl[coordNum].trim());
				    } catch (NumberFormatException e) {
				        throw new NumberFormatException(spl[coordNum]
				                + " cannot be parsed as a double! Expression"
				                + " is not supported for now. We have put it in"
				                + " the feature request list. Sorry about any"
				                + " inconvenience!");
				    }
				}
				if (spl.length > 4) { //selective dynamics is specified
					for (int selNum = 4; selNum < 7; selNum++) {
						Integer value;
						try {
							value = Integer.parseInt(spl[selNum]);
						} catch (NumberFormatException e) {
							throw new RuntimeException("Selective dynamics format is not recognized.");
						}
						if (value == 0) {
							m_UseSelectiveDynamics[siteNum][selNum-4] = true;
						}	
					}
				}
			}
			
		}
	}
	private void readLatticeVectors(ArrayList<String> cellParameters) {
		double[][] vectorArray = new double[3][3]; //TODO: Perhaps someday this will need to be changed...
		for (int a = 0; a < cellParameters.size(); a++) {
			/*if (cellParameters.get(a).contains("cell_parameters") && a != 0) {
				throw new RuntimeException("Incorrect placement of card name: CELL_PARAMETERS");
			}
			if  (a == 0) {
				String[] split = cellParameters.get(a).trim().replaceAll("(", "").replaceAll(")", "").replaceAll("{", "").replaceAll("}", "").split("\\s+");
				if (split.length == 1) { //not specified
					m_LatticeVectorUnit = "bohr";
				} else if (split[1].trim().toLowerCase().equals("alat")) {
					m_LatticeVectorUnit = "angstrom";
				} else if (split[1].trim().toLowerCase().equals("bohr")) {
					m_LatticeVectorUnit = "bohr";
				} else if (split[1].trim().toLowerCase().equals("angstrom")) {
					m_LatticeVectorUnit = "angstrom";
				} else { //everything else
					m_LatticeVectorUnit = "bohr";
				}*/
			if  (a == 0) {
				String value = cellParameters.get(a);
				if  (value == null) {
					m_LatticeVectorUnit = "bohr";
				} else if (value.trim().toLowerCase().equals("alat")) {
					m_LatticeVectorUnit = "angstrom";
				} else if (value.trim().toLowerCase().equals("bohr")) {
					m_LatticeVectorUnit = "bohr";
				} else if (value.trim().toLowerCase().equals("angstrom")) {
					m_LatticeVectorUnit = "angstrom";
				} else { //everything else
					m_LatticeVectorUnit = "bohr";
				}
			} else {
				String[] split = cellParameters.get(a).trim().split("\\s+");
				if (split.length > 3) {
					throw new RuntimeException("There are more than 3 components in the vector coordinates.");
				}
				for (int b = 0; b < split.length; b++) {
					vectorArray[a-1][b] = Double.parseDouble(split[b].trim());
				}
			}
		}
		AbstractLinearBasis basis = null;
		if (m_LatticeVectorUnit.equals("bohr")) {
				basis = new CartesianScaleBasis(m_BohrScaling);
		} else {
			basis = CartesianBasis.getInstance();
		}
		m_LatticeVectors = new Vector[3];
		for (int c = 0; c < vectorArray.length; c++) {
			Coordinates vCoor = new Coordinates(vectorArray[c], basis);
			m_LatticeVectors[c] = new Vector(vCoor);
		}
	}
	//change with Basis
	public void scaleLatticeVectors(double scale, String unit) {
		double multiplier = scale;
		if (!unit.equals(m_LatticeVectorUnit)) {
			if (unit.equals("bohr")) { //not equal, bohr unit coming into angstrom
				multiplier = multiplier * m_BohrScaling;
			} else if (unit.equals("angstrom")) { //not equal, angstrom unit coming into bohr
				multiplier = multiplier/m_BohrScaling;
			} else {//other than that should not be possible
			throw new RuntimeException("An unknown unit is being used.");
			}
		}
		Vector[] newVectors = new Vector[m_LatticeVectors.length];
		for (int a = 0; a < m_LatticeVectors.length; a++) {
			double[] coordVals =  m_LatticeVectors[a].getCartesianDirection();
			Coordinates coords = new Coordinates(coordVals,new CartesianScaleBasis(multiplier));
			newVectors[a] = new Vector(coords);
		}
		m_LatticeVectors = newVectors;
	}
	
	public String getCoordsAtomicUnit() {
		return m_CoordsAtomicUnit;
	}
	public String getLatticeVectorUnit() {
		return m_LatticeVectorUnit;
	}
	public Vector[] getLatticeVectors() {
		return m_LatticeVectors;
	}
	public Species getSiteSpecies(int index) {
		return m_SiteSpecies[index];
	}
	public Species[] getSpecies() {
		return m_SiteSpecies;
	}
	public Species[] getUniqueSpecies() {
		return m_UniqueSpecies;
	}
	public int numAtoms() {
		return m_NumAtoms;
	}
	public double[] getSiteCoordArray(int index) {
		return m_AtomicCoordinates[index];
	}
	public double[][] getSiteCoordArray() {
		return m_AtomicCoordinates;
	}
	public boolean[][] useSelectiveDynamics() {
		return m_UseSelectiveDynamics;
	}

	/*
	 * This method parse a mathematical expression into a double precision 
	 * float number, as QE specified in their input instruction.
	 * 
	 * Only +, -, *, /, ^ are supported. And expr cannot start with +.
	 * 
	 */
	private static double parseExpr(String expr) {
	    double value = 0.0;
	    //TODO: parse the expression and return the values.
	    return value;
	}
}
