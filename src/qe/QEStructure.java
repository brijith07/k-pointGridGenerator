package qe;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;

import matsci.Species;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.structure.IStructureData;

public class QEStructure implements IStructureData{
	
	private QENamelists m_InputData;
	private boolean[] m_IsVectorPeriodic = new boolean[] {true, true, true}; // Default value.
	
	public QEStructure (Reader reader) {
		m_InputData = new QENamelists(reader);
	}
	
	public QEStructure (QENamelists namelists) {
		m_InputData = namelists;
	}
	
	public QEStructure (String filename) throws FileNotFoundException {
		this(new FileReader(filename));
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Vector[] getCellVectors() {
		return m_InputData.getLatticeVectors();
	}

	@Override
	public boolean[] getVectorPeriodicity() {
		return m_IsVectorPeriodic;
	}

	@Override
	public int numDefiningSites() {
		return m_InputData.getSiteCoordinates().length;
	}

	@Override
	public Coordinates getSiteCoords(int index) {
		return m_InputData.getSiteCoordinates()[index];
	}

	@Override
	public Species getSiteSpecies(int index) {
		return m_InputData.getSiteSpecies()[index];
	}
	
	public QENamelists getNamelists() {
		return m_InputData;
	}

}
