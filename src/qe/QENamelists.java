package qe;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Scanner;

import matsci.Species;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.AbstractBasis;
import matsci.location.basis.AbstractLinearBasis;
import matsci.location.basis.CartesianBasis;
import matsci.location.basis.CartesianScaleBasis;
import matsci.location.basis.LinearBasis;
import matsci.structure.IStructureData;
import matsci.structure.reciprocal.KPointLattice;
import matsci.util.arrays.ArrayUtils;
//import qe.QEInputFile.QEKPoints;
//import utils.TextFileParser;

public class QENamelists implements IStructureData{
	
	private ArrayList<String> m_FileContent;
	private ArrayList<String> m_ReturnedContent;
	private LinkedHashMap<String, LinkedHashMap<String, Object>> m_ParamValuesByNamelist = new LinkedHashMap<String, LinkedHashMap<String, Object>>();
	//the following ArrayLists are QE inputs that are not in cards
	/*private ArrayList<String> m_AtomicSpecies;
	private ArrayList<String> m_AtomicPositions;
	private ArrayList<String> m_KPoints;
	private ArrayList<String> m_CellParameters;
	private ArrayList<String> m_Constraints;
	private ArrayList<String> m_Occupations;
	private ArrayList<String> m_AtomicForces;*/
	private LinkedHashMap<String, ArrayList<String>> m_CardData = new LinkedHashMap<String, ArrayList<String>>();
	private QECardReader m_Cards;
	private Vector[] m_LatticeVectors;
	private Coordinates[] m_Coordinates;
	private Species[] m_SiteSpecies;
	//private String m_StructureUnit;
	private double m_BohrScaling = 0.52917720859; //TODO: check what value used in QE
	private boolean[] m_IsVectorPeriodic = new boolean[] {true, true, true}; // Default value.
	private QEKPoints m_KPoints;
	private boolean m_UseSelectiveDynamics;
	
	public QENamelists(Reader reader) {
		m_FileContent = new ArrayList<String>();
		m_ReturnedContent = new ArrayList<String>();
		Scanner scanner = new Scanner(reader);
		while (scanner.hasNextLine()) {
			String currentLine = scanner.nextLine();
			m_FileContent.add(currentLine);
			m_ReturnedContent.add(currentLine);
		}
		scanner.close();
		
		parseNamelists();
		parseCards();
		
		int numAtoms = this.getIntegerValue("&system", "nat", -1);
		if (numAtoms <= 0) {
			throw new RuntimeException("Number of atoms is less than 0.");
		}
		int numCoordsRead = m_CardData.get("atomic_positions").size() - 1; //The first value is the unit
		if (numCoordsRead != numAtoms) {
			throw new RuntimeException("Number of atoms specified is not the same as the number of coordinates listed.");
		}
		int ibrav = this.getIntegerValue("&system", "ibrav", -256);
		if (ibrav == -256) {
			throw new RuntimeException("Invalid ibrav value");
		}
		
		m_Cards = new QECardReader(m_CardData, numAtoms, ibrav);
		double[][] siteCoordArray = m_Cards.getSiteCoordArray();
		m_SiteSpecies = m_Cards.getSpecies();
		if (ibrav == 0) {
			double celldm1 = this.getDoubleValue("&system", "celldm(1)", 0.0);
			double A = this.getDoubleValue("&system", "a", 0.0);
			String unit = "";
			//scale lattice constant if celldm(1) or A is specified
			double latticeConstant = 1.0;
			if (celldm1 > 0.0 && A > 0.0) {
				throw new RuntimeException("Both celldm(1) and A are specified.");
			} else if (celldm1 > 0.0) {
				latticeConstant = celldm1;
				unit = "bohr";
				m_Cards.scaleLatticeVectors(latticeConstant, unit);
			} else if (A > 0.0) {
				latticeConstant = A;
				unit = "angstrom";
				m_Cards.scaleLatticeVectors(latticeConstant, unit);
			}
			m_LatticeVectors = m_Cards.getLatticeVectors();
		} else {
			initializeLatticeVectors();
		}
		String coordinateUnit = m_Cards.getCoordsAtomicUnit();
		AbstractLinearBasis coordBasis = null;
		if (coordinateUnit.equals("angstrom")) {
			coordBasis = CartesianBasis.getInstance();
		} else if (coordinateUnit.equals("bohr")) {
			coordBasis = new CartesianScaleBasis(m_BohrScaling);
		} else if (coordinateUnit.equals("crystal")) {
			coordBasis = new LinearBasis(m_LatticeVectors);
		} else if (coordinateUnit.equals("crystal_sg")) {
			throw new RuntimeException("crystal_sg is currently unimplemented.");
		} else {
			coordBasis = new CartesianScaleBasis(m_BohrScaling);
		}
		m_Coordinates = new Coordinates[siteCoordArray.length];
		for (int coordNum = 0; coordNum < siteCoordArray.length; coordNum++) {
			m_Coordinates[coordNum] = new Coordinates(siteCoordArray[coordNum], coordBasis);
		}
	}
	
	public QENamelists(String filename) throws FileNotFoundException {
		this(new FileReader(filename));
	}
	private void parseNamelists() {
		String key = null;
		LinkedHashMap<String, Object> current = null;
		for (int a = 0; a < m_FileContent.size(); a++) {
			if (m_FileContent.get(a).toLowerCase().contains("&control") 
					|| m_FileContent.get(a).toLowerCase().contains("&system")
					|| m_FileContent.get(a).toLowerCase().contains("&electrons")
					|| m_FileContent.get(a).toLowerCase().contains("&ions")
					|| m_FileContent.get(a).toLowerCase().contains("&cell")) {
				key = stripComments(m_FileContent.get(a).toLowerCase().trim());
				current = new LinkedHashMap<String,Object>();
			} else if (m_FileContent.get(a).trim().equals("/")) {
				m_ParamValuesByNamelist.put(key, current);
				key = null;
				current = null;
			} else if (key == null) {
				continue;
			} else {
				String line = stripComments(m_FileContent.get(a)).replaceAll("=", " ").trim();
				if (line.isEmpty() || line.length() == 0) { //line is empty or all whitespaces
					continue;
				}
				//Aasjdhasjdjha, not only whitespaces, QE also can use commas in the same line as parameter delimiter
				String[] multi = line.split(",");
				for (int b = 0; b < multi.length; b++) {
					String part = multi[b].trim();
					String[] split = part.split("\\s+");
					//In case we have a parameter that has no value
					if (split.length < 2) {
						current.put(split[0].toLowerCase(), null);
					} else {
						//String[] split = TextFileParser.splitIgnoringSingleQuotations(part);
						current.put(split[0].toLowerCase(), split[1]);
					}
				}
			}
		}
	}
	
	private void parseCards() {
		String key = null;
		ArrayList<String> current = null;
		boolean isSecondLine = false; //apparently, the line after the card name can be empty and it's ok....
		for (int a = 0; a < m_FileContent.size(); a++) {
			boolean isEmptyLine = false;
			if (stripComments(m_FileContent.get(a)).trim().equals("") || stripComments(m_FileContent.get(a)).trim().length() == 0) {
				isEmptyLine = true;
			}
			if (m_FileContent.get(a).toLowerCase().contains("atomic_species") 
					|| m_FileContent.get(a).toLowerCase().contains("atomic_positions")
					|| m_FileContent.get(a).toLowerCase().contains("cell_parameters")
					|| m_FileContent.get(a).toLowerCase().contains("k_points")
					|| m_FileContent.get(a).toLowerCase().contains("contraints")
					|| m_FileContent.get(a).toLowerCase().contains("occupations")
					|| m_FileContent.get(a).toLowerCase().contains("atomic_forces")) {
				
				isSecondLine = true;
				
				if (m_FileContent.get(a).contains("=")) {
					continue;
				}
				
				if (current != null) { //if we already have an ArrayList that means we found a different domain, store the existing
					m_CardData.put(key, current);
					key = null;
					current = null;
				}
				String head = m_FileContent.get(a);
				String[] split = head.replaceAll("\\{", " ").replaceAll("\\}", " ").replaceAll("\\(", " ").replaceAll("\\)", " ").trim().split("\\s+");
				key = split[0].trim().toLowerCase();	
				current = new ArrayList<String>();
				String firstValue = null;
				if (split.length > 1) { //in this case, value is specified
					firstValue = split[1].trim().toLowerCase();
					current.add(stripComments(firstValue));
				} 
				if (firstValue == null) {
					current.add(null);
				}
			
			} else if (isSecondLine && isEmptyLine) {
				//if (stripComments(m_FileContent.get(a)).trim().equals("") || stripComments(m_FileContent.get(a)).trim().length() == 0) {
				isSecondLine = false;
				continue;	
				//}
			
			//} else if (stripComments(m_FileContent.get(a)).trim().equals("") || stripComments(m_FileContent.get(a)).trim().length() == 0) {
			} else if (isEmptyLine) {
				isSecondLine = false;
				if (current != null) { //if we already have an ArrayList that means we found a different domain, store the existing
					m_CardData.put(key, current);
					key = null;
					current = null;
				}
					
			} else if (key == null) {
				isSecondLine = false;
				continue;
			} else {
				isSecondLine = false;
				String line = stripComments(m_FileContent.get(a)).replaceAll("=", " ").trim();
				current.add(line);
			}
		}
		if (current != null && key != null) { //in case reading hit EOF, safe the last arrayList
			m_CardData.put(key, current);
		}
	}
	
	private void initializeLatticeVectors() {
		if (keyExists("&system", "celldm(1)") && keyExists("&system", "a")) {
			throw new RuntimeException("Both celldm and a are specified.");
		}
		int ibrav = getIntegerValue("&system", "ibrav", -256);
		if ( ibrav == 0) { //ibrav == 0 everything specified explicitly
			return;
		}
		double test = getDoubleValue("&system", "celldm(1)", 0.0) ;
		String unit = null;
		double celldm[] = new double[6];
		//All numbers are immediately scaled to Angstroms
		if (keyExists("&system", "celldm(1)")) {
			/*celldm[0] = getDoubleValue("&system", "celldm(1)", 0.0) *  m_BohrScaling;
			celldm[1] = getDoubleValue("&system", "celldm(2)", 0.0) *  m_BohrScaling;
			celldm[2] = getDoubleValue("&system", "celldm(3)", 0.0) *  m_BohrScaling;
			celldm[3] = getDoubleValue("&system", "celldm(4)", 0.0) *  m_BohrScaling;
			celldm[4] = getDoubleValue("&system", "celldm(5)", 0.0) *  m_BohrScaling;
			celldm[5] = getDoubleValue("&system", "celldm(6)", 0.0) *  m_BohrScaling;*/
			celldm[0] = getDoubleValue("&system", "celldm(1)", 0.0);
			celldm[1] = getDoubleValue("&system", "celldm(2)", 0.0);
			celldm[2] = getDoubleValue("&system", "celldm(3)", 0.0);
			celldm[3] = getDoubleValue("&system", "celldm(4)", 0.0);
			celldm[4] = getDoubleValue("&system", "celldm(5)", 0.0);
			celldm[5] = getDoubleValue("&system", "celldm(6)", 0.0);
			unit = "bohr";
		} else if (keyExists("&system", "a")) {
			unit = "angstrom";
			double a = getDoubleValue("&system", "a", 0.0);
			double b = getDoubleValue("&system", "b", 0.0);
			double c = getDoubleValue("&system", "c", 0.0);
			double cosab = getDoubleValue("&system", "cosab", 0.0);
			double cosac = getDoubleValue("&system", "cosac", 0.0);
			double cosbc = getDoubleValue("&system", "cosbc", 0.0);
			celldm[0] = a/m_BohrScaling;
			celldm[1] = b/a;
			celldm[2] = c/a;
			if (ibrav == 14) {
				celldm[3] = cosbc;
				celldm[4] = cosac;
				celldm[5] = cosab;
			} else if (ibrav == -12) {
				celldm[4] = cosac;
			} else {
				celldm[3] = cosab;
			}
		} else {
			throw new RuntimeException("Lattice information from celldm(1) or a was not provided.");
		}
		QELatticeGenerator latticeGenerator = new QELatticeGenerator(ibrav, celldm);
		//m_LatticeVectors = latticeGenerator.getLatticeVectors(CartesianBasis.getInstance());
		m_LatticeVectors = latticeGenerator.getLatticeVectors(new CartesianScaleBasis(m_BohrScaling));
	}
	
		public int getIntegerValue(String card, String key, int defaultValue) {
			LinkedHashMap<String, Object> paramValue = m_ParamValuesByNamelist.get(card.toLowerCase());
			if (!paramValue.containsKey(key)) {
				return defaultValue; //default
			}
			if (paramValue.get(key) instanceof Integer) {
				return (Integer) paramValue.get(key);
			}
			try {
				String val = (String) paramValue.get(key);
				Integer value = getIntValue(val);
				paramValue.put(key, value);
				return value;
			} catch (NumberFormatException e) {
				return defaultValue;
			}
		}
		
		public double getDoubleValue(String card, String key, double defaultValue) {
			LinkedHashMap<String, Object> paramValue = m_ParamValuesByNamelist.get(card.toLowerCase());
			if (!paramValue.containsKey(key)) {
				return defaultValue; //default
			}
			if (paramValue.get(key) instanceof Double) {
				return (Double) paramValue.get(key);
			}
			try {
				Double value = QENamelists.this.getDoubleValue((String) paramValue.get(key));
				paramValue.put(key, value);
				return value;
			} catch (NumberFormatException e) {
				return defaultValue;
			}
		}
		
		public boolean getBooleanValue(String card, String key, Boolean defaultValue) {
			LinkedHashMap<String, Object> paramValue = m_ParamValuesByNamelist.get(card.toLowerCase());
			if (!paramValue.containsKey(key)){
				return defaultValue;
			}
			if (paramValue.get(key) instanceof Boolean) {
				return (Boolean) paramValue.get(key);
			}
			Boolean value = QENamelists.this.getBooleanValue((String) paramValue.get(key));
			if (value == null) {
				return defaultValue;
			}
			paramValue.put(key, value);
			return value;
		}
		
		public String getStringValue(String card, String key, String defaultValue) {
			LinkedHashMap<String, Object> paramValue = m_ParamValuesByNamelist.get(card.toLowerCase());
			if (!paramValue.containsKey(key)){
				return defaultValue;
			}
			String value = QENamelists.this.getStringValue((String) paramValue.get(key));
			if (value == null) {
				return defaultValue;
			}
			paramValue.put(key, value);
			return value;
		}
		
		public boolean keyExists(String card, String key) {
			if (m_ParamValuesByNamelist.get(card).containsKey(key)) {
				return true;
			}
			return false;
		}
		
		public Vector[] getLatticeVectors() {
			return m_LatticeVectors;
		}
		public Coordinates[] getSiteCoordinates() {
			return m_Coordinates;
		}
		public Species[] getSiteSpecies() {
			return m_SiteSpecies;
		}
		public boolean useSelectiveDynamics() {
			boolean[][] array = this.selectiveDynamicsArray();
			for (int a = 0; a < array.length; a++) {
				for (int b = 0; b < array[a].length; b++) {
					if (array[a][b]) {
						m_UseSelectiveDynamics = array[a][b];
					}
				}
			}
			return m_UseSelectiveDynamics;
		}
		
		public boolean[][] selectiveDynamicsArray() {
			return m_Cards.useSelectiveDynamics();
		}
		public boolean getSelectiveDynamics(int siteNum, int dimNum) {
			return m_Cards.useSelectiveDynamics()[siteNum][dimNum];
		}

	
	protected Integer getIntValue(String value) {
		//VASP can use whitespace as comment delimiter
		String[] split = value.replaceAll(",", "").split("\\s+");
		if (split.length == 0) {
			throw new NumberFormatException();
		}
		Integer val = Integer.parseInt(split[0].trim());
		return val;
	}
	protected Double getDoubleValue(String value) {
		//VASP can use whitespace as comment delimiter
		String[] split = value.replaceAll(",","").split("\\s+");
		if (split.length == 0) {
			throw new NumberFormatException();
		}
		//this is because Quantum Espresso accepts Fortran-style exponential symbol (i.e. D)
		String va = split[0].trim();
		if (split[0].trim().contains("D")) {
			va = va.replaceAll("D", "E");
		}
		Double val = Double.parseDouble(va);
		return val;
	}
	protected Boolean getBooleanValue(String value) {
		//VASP can use whitespace as comment delimiter
		//remove periods (e.g. .TRUE.)
		value = value.replaceAll("\\.", "").replaceAll(",", "");
		String[] split = value.split("\\s+");
		if (split.length == 0) {
			return null;
		}
		Boolean val = Boolean.parseBoolean(split[0].trim());
		return val;
	}
	protected String getStringValue(String value) {
		//VASP can use whitespace as comment delimiter
		String[] split = value.replaceAll(",","").split("\\s+");
		if (split.length == 0) {
			return null;
		}
		String val = split[0].trim();
		return val;
	}
	
	private String stripComments(String line) {
		int index = line.indexOf("#");
	    if (index >= 0) {
	      line = line.substring(0, index);
	    }
	    int index2 = line.indexOf("!");
	    if (index2 >= 0) {
	      line = line.substring(0, index2);
	    }
	    return line;
	}
	
	private void commentKPoints() {
		boolean kpointLine = false;
		m_ReturnedContent = new ArrayList<String>();
		String append = "";
		for (int a = 0; a < m_FileContent.size(); a++) {
			String current = m_FileContent.get(a);
			if (current == null) {
				kpointLine = false;
				continue;
				}
			if (stripComments(current).toLowerCase().contains("k_points")) {
				kpointLine = true;
				append = "#";
			}
			if (kpointLine) {
				if (stripComments(current).trim().isEmpty()) {
					kpointLine = false;
					append = "";
				} else if (stripComments(current).matches(".*[a-zA-Z].*") && !stripComments(current).toLowerCase().contains("k_points") ) {
					kpointLine = false;
					append = "";
				}
			}
			m_ReturnedContent.add(append + m_FileContent.get(a));
			
		}
	}
	
	public void updateWithKPoints(KPointLattice kpointLattice, LinearBasis basis, String header, String comment) {
		commentKPoints();
		m_KPoints = new QEKPoints(kpointLattice, basis);
		m_KPoints.setDescription(comment);
		ArrayList<String> appender = m_KPoints.toArrayList();
		ArrayList<String> commented = new ArrayList<String>();
		commented.add(header); //Warning: Will cause an empty line up to without the script to tidy this up
		commented.addAll(m_ReturnedContent); 
		commented.addAll(appender);
		m_ReturnedContent = commented;
	}
	
/*	public void appendWithKPoints(ArrayList<String> kpointLines) {
		commentKPoints();
		m_ReturnedContent.addAll(kpointLines);
	}*/
	
	public void writeReturnedContent(String fileName) { 
	    try {
	    	FileWriter fileWriter = new FileWriter(fileName);
	    	BufferedWriter writer = new BufferedWriter(fileWriter);
	    	this.writeReturnedContent(writer);
	    	writer.flush();
	    	writer.close();
	    } catch (IOException e) {
	    	throw new RuntimeException("Failed to write file " + fileName, e);
	    } 
	  }
	  //same case as this
	  public void writeReturnedContent(Writer writer) {
	    try {
	      //writeManual(writer);
	      writeFromList(writer);
	    } catch (IOException e) {
	      throw new RuntimeException("Failed to write kpoints.", e);
	    } 
	  }
	  private void writeFromList(Writer writer) throws IOException {
		  for (int a = 0; a < m_ReturnedContent.size(); a++) {
			  writer.write(m_ReturnedContent.get(a));
			  writer.write(System.lineSeparator());
		  }
	  }
		@Override
		public String getDescription() {
			// TODO Auto-generated method stub
			return null;
		}
	
		@Override
		public Vector[] getCellVectors() {
			return this.getLatticeVectors();
		}
	
		@Override
		public boolean[] getVectorPeriodicity() {
			return m_IsVectorPeriodic;
		}
	
		@Override
		public int numDefiningSites() {
			return this.getSiteCoordinates().length;
		}
	
		@Override
		public Coordinates getSiteCoords(int index) {
			return this.getSiteCoordinates()[index];
		}
	
		@Override
		public Species getSiteSpecies(int index) {
			return this.getSiteSpecies()[index];
		}
		
		public class QEKPoints {
			  
		  private String m_Comment = "Quantum Espresso k-points";
		  private String m_Header = "";
		  private Coordinates[] m_Coordinates;
		  private AbstractBasis m_Basis = CartesianBasis.getInstance(); //For simplicity use Cartesian basis (our default)
		  private double[] m_Weights;
		  private int[][] m_SuperToDirect;
		  private double[] m_Shifts;
		  
		  
		  public QEKPoints(Coordinates[] coords, double[] weights) {
		    m_Coordinates = (Coordinates[]) ArrayUtils.copyArray(coords);
		    m_Weights = ArrayUtils.copyArray(weights);
		  }
		  
		  
		 /* public QEKPoints(int[][] superToDirect, double[] shifts) {
			  m_SuperToDirect = ArrayUtils.copyArray(superToDirect);
			  m_Shifts = ArrayUtils.copyArray(shifts);
		  }*/ //no such function in QE
		  
		  public QEKPoints(KPointLattice lattice, LinearBasis basis) {
		    m_Coordinates = lattice.getDistinctKPointCoords();
		    m_Weights = lattice.getDistinctKPointWeights();
		    m_Basis = basis; //new LinearBasis(CartesianBasis.getInstance().getOrigin(), basis.getBasisToCartesian());
		  }
		  
		  /*public QEKPoints(KPointLattice lattice, BravaisLattice definingLattice) {
			  m_Coordinates = lattice.getDistinctKPointCoords();
			  m_Weights = lattice.getDistinctKPointWeights();
			  //m_Basis = definingLattice.getReciprocalLattice().getLatticeBasis();
			  m_Shifts = lattice.getShiftArray(definingLattice);
			  m_SuperToDirect = lattice.getSuperToDirect(definingLattice);
		  }*/
		  
		  public QEKPoints(KPointLattice lattice) {
		    this(lattice, lattice.getReciprocalSpaceLattice().getLatticeBasis());
		  }
		  
		  public void setHeader (String header) {
			  m_Header = header;
		  }
		  
		  public String getHeader() {
			  return m_Header;
		  }
		  
		  public void setDescription(String comment) {
		    m_Comment = comment;
		  }
		  
		  /*public String setComment() {
		    return m_Comment;
		  }*/
		  
		  public ArrayList<String> toArrayList() {
			  ArrayList<String> list = new ArrayList<String>();
			  list = toStringList();
			  return list;
		  }
		  
		  //this should not be used, but here for debugging purposes
		  public void writeFile(String fileName) {
		    
		    try {
		      FileWriter fileWriter = new FileWriter(fileName);
		      BufferedWriter writer = new BufferedWriter(fileWriter);
		      this.writeCoordinates(writer);
		      writer.flush();
		      writer.close();
		    } catch (IOException e) {
		      throw new RuntimeException("Failed to write file " + fileName, e);
		    } 
		  }
		  //same case as this
		  public void writeCoordinates(Writer writer) {
		    try {
		      writeFromList(writer);
		    } catch (IOException e) {
		      throw new RuntimeException("Failed to write kpoints.", e);
		    } 
		  }
		  
		  private void writeFromList(Writer writer) throws IOException {
			  ArrayList<String> list = new ArrayList<String>();
			  list = toStringList();
			  for (int a = 0; a < list.size(); a++) {
				  writer.write(list.get(a));
				  writer.write(System.lineSeparator());
			  }
		  }
		  
		  private ArrayList<String> toStringList() {
			  ArrayList<String> list = new ArrayList<String>();
			  list.add("# " + m_Comment);
			  
			  list.add("K_POINTS crystal");
			  list.add(m_Coordinates.length + "");
			  String formatString = "0.00000000000000";
			  DecimalFormat formatter = new DecimalFormat(formatString);
			    
			  for (int pointNum = 0; pointNum < m_Coordinates.length; pointNum++) {
			    double[] coordArray = m_Coordinates[pointNum].getCoordArray(m_Basis);
			    for (int dimNum = 0; dimNum < coordArray.length; dimNum++) {
			      coordArray[dimNum] -= Math.floor(coordArray[dimNum]);
			    }
			    double[] test = m_Coordinates[pointNum].getCoordArray(m_Basis);  
			    list.add(formatter.format(coordArray[0]) + " " + formatter.format(coordArray[1]) + " "
			    		+ formatter.format(coordArray[2]) + " " +  m_Weights[pointNum]);// + "#" + " " + test[0] + " " + test[1] + " " + test[2]);
			  }
			  return list;
		  }	  

		  private void newLine(Writer writer) throws IOException {
		    writer.write("\n");
		  }

		}

}
