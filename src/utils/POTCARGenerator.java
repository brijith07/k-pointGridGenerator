package utils;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import matsci.Element;
import matsci.io.structure.CIF;
import matsci.io.vasp.POSCAR;
import matsci.structure.IStructureData;
import matsci.structure.Structure;
import matsci.structure.superstructure.SuperStructure;

/**
 * Automatically generates a POSCAR and POTCAR given an input structure file (e.g. .cif, POSCAR)<br>
 * The output will be named POSCAR.out and POTCAR.out to avoid accidental overwriting.<br>
 * @author pwisesa <br> 
 * @date Jul10,2014
 */

public class POTCARGenerator {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		if (args[0].equals("-generate")) {
			generate(args[1], args[2], args[3]);
		}
		
		if (args[0].equals("-v5PSCR")) {
			createV5POSCAR(args[1], args[2]);
		}
		
		if (args[0].equals("-generatePrim")) {
			generatePRIM(args[1], args[2], args[3]);
		}
		
		if (args[0].equals("-generatePerturb"))
			generateAndPerturb(args[1], args[2], args[3]);
		
		if (args[0].equals("-generateSuperPerturb"))
			generateSuperAndPerturb(args[1], args[2], args[3], Double.parseDouble(args[4]), Double.parseDouble(args[5]));

	}
	
	private static void createV5POSCAR(String type, String structureFile) {
		Structure givenStruct = null;
		if(type.equals(".cif") || type.equals("cif") || type.equals("CIF") || type.equals(".CIF")) {
			CIF cifFile = new CIF (structureFile);
			givenStruct = new Structure (cifFile).findPrimStructure();
		} else if(type.equals(".vasp") || type.equals("vasp") || type.equals("POSCAR") || type.equals("poscar") || type.equals("VASP")) {
			POSCAR pos = new POSCAR(structureFile);
			if (!pos.usesVASP5Format()) {
				throw new RuntimeException("POSCAR does not use VASP5 format, cannot recognize elements, add the elements individually or use a different format.");
			}
			givenStruct = new Structure (pos);
		} else {
			throw new RuntimeException("Input file type is not recognized!");
		}
		String delim = "[\\[\\]\\s+]";
		String[] breakDown = structureFile.split(delim);
		String fileName = breakDown[breakDown.length-1];
		
		String currentDir = System.getProperty("user.dir");
		POSCAR outPOSCAR = new POSCAR(givenStruct, true);
		outPOSCAR.useVASP5Format(true);
		outPOSCAR.writeElementSymbolsOnly(true);
		outPOSCAR.writeFile(currentDir + "/" + fileName + ".V5");
	}

	private static void generate (String POTCARDir, String type, String structureFile) throws IOException {
		System.out.println("STARTING");
		long start = System.currentTimeMillis();
		Structure givenStruct = null;
		
		if(type.equals(".cif") || type.equals("cif") || type.equals("CIF") || type.equals(".CIF")) {
			CIF cifFile = new CIF (structureFile, 1.0/720);
			givenStruct = new Structure (cifFile);
		} else if(type.equals(".vasp") || type.equals("vasp") || type.equals("POSCAR") || type.equals("poscar") || type.equals("VASP")) {
			POSCAR pos = new POSCAR(structureFile);
			//boolean checkVer = pos.usesVASP5Format();
		/*	if (!pos.usesVASP5Format()) {
				throw new RuntimeException("POSCAR does not use VASP5 format, cannot recognize elements, add the elements individually or use a different format.");
			}*/
			givenStruct = new Structure (pos);
		} else {
			throw new RuntimeException("Input file type is not recognized!");
		}
		System.out.println("Processing: " + givenStruct.getDescription());
		POSCAR outPOSCAR = new POSCAR(givenStruct, true);
		String[] elementNames = getElementNames(outPOSCAR);
		String currentDir = System.getProperty("user.dir");
		//String[] availablePOTCARs = listFilesForFolder(POTCARDir);
		
		List<String> allLines = new ArrayList<String>();
		
		//As of today POTCARs are ~3000 lines in length each, so no problem. This might need changing if they do get significantly longer though.
		for (int a = 0; a < elementNames.length; a++) {
			List<String> currentLines = Files.readAllLines(Paths.get(POTCARDir + "/" + elementNames[a]), Charset.defaultCharset());
			allLines.addAll(currentLines);
		}
		//FileWriter writer = new FileWriter(currentDir + "/POTCAR.out");
		PrintStream writer = new PrintStream(new File(currentDir + "/POTCAR.vasp"));
		for (int a = 0; a < allLines.size(); a++) {
		//	writer.write(allLines.get(a));
			writer.println(allLines.get(a));
		}
		writer.close();
		outPOSCAR.useVASP5Format(true);
		outPOSCAR.writeElementSymbolsOnly(true);
		outPOSCAR.writeFile(currentDir + "/POSCAR.vasp");
		long end = System.currentTimeMillis();
		long total = end - start;
		System.out.println("FINISHED");
		System.out.println("Time spent: " + total + " ms");
	}
	
	private static void generateAndPerturb (String POTCARDir, String type, String structureFile) throws IOException {
		System.out.println("STARTING");
		long start = System.currentTimeMillis();
		Structure givenStruct = null;
		
		if(type.equals(".cif") || type.equals("cif") || type.equals("CIF") || type.equals(".CIF")) {
			CIF cifFile = new CIF (structureFile, 1.0/720);
			givenStruct = new Structure (cifFile);
		} else if(type.equals(".vasp") || type.equals("vasp") || type.equals("POSCAR") || type.equals("poscar") || type.equals("VASP")) {
			POSCAR pos = new POSCAR(structureFile);
			//boolean checkVer = pos.usesVASP5Format();
		/*	if (!pos.usesVASP5Format()) {
				throw new RuntimeException("POSCAR does not use VASP5 format, cannot recognize elements, add the elements individually or use a different format.");
			}*/
			givenStruct = new Structure (pos);
		} else {
			throw new RuntimeException("Input file type is not recognized!");
		}
		givenStruct = givenStruct.perturbSlightly(0.05);
		System.out.println("Processing: " + givenStruct.getDescription());
		POSCAR outPOSCAR = new POSCAR(givenStruct, true);
		String[] elementNames = getElementNames(outPOSCAR);
		String currentDir = System.getProperty("user.dir");
		//String[] availablePOTCARs = listFilesForFolder(POTCARDir);
		
		List<String> allLines = new ArrayList<String>();
		
		//As of today POTCARs are ~3000 lines in length each, so no problem. This might need changing if they do get significantly longer though.
		for (int a = 0; a < elementNames.length; a++) {
			List<String> currentLines = Files.readAllLines(Paths.get(POTCARDir + "/" + elementNames[a]), Charset.defaultCharset());
			allLines.addAll(currentLines);
		}
		//FileWriter writer = new FileWriter(currentDir + "/POTCAR.out");
		PrintStream writer = new PrintStream(new File(currentDir + "/POTCAR.vasp"));
		for (int a = 0; a < allLines.size(); a++) {
		//	writer.write(allLines.get(a));
			writer.println(allLines.get(a));
		}
		writer.close();
		outPOSCAR.useVASP5Format(true);
		outPOSCAR.writeElementSymbolsOnly(true);
		outPOSCAR.writeFile(currentDir + "/POSCAR.vasp");
		long end = System.currentTimeMillis();
		long total = end - start;
		System.out.println("FINISHED");
		System.out.println("Time spent: " + total + " ms");
	}
	private static void generateSuperAndPerturb (String POTCARDir, String type, String structureFile, Double periodic, Double perturb) throws IOException {
		System.out.println("STARTING");
		long start = System.currentTimeMillis();
		Structure givenStruct = null;
		
		if(type.equals(".cif") || type.equals("cif") || type.equals("CIF") || type.equals(".CIF")) {
			CIF cifFile = new CIF (structureFile, 1.0/720);
			givenStruct = new Structure (cifFile);
		} else if(type.equals(".vasp") || type.equals("vasp") || type.equals("POSCAR") || type.equals("poscar") || type.equals("VASP")) {
			POSCAR pos = new POSCAR(structureFile);
			//boolean checkVer = pos.usesVASP5Format();
		/*	if (!pos.usesVASP5Format()) {
				throw new RuntimeException("POSCAR does not use VASP5 format, cannot recognize elements, add the elements individually or use a different format.");
			}*/
			givenStruct = new Structure (pos);
		} else {
			throw new RuntimeException("Input file type is not recognized!");
		}
		Structure sup = new SuperStructure(givenStruct, periodic);
		sup = sup.perturbSlightly(perturb);
		System.out.println("Processing: " + sup.getDescription());
		POSCAR outPOSCAR = new POSCAR(sup, true);
		String[] elementNames = getElementNames(outPOSCAR);
		String currentDir = System.getProperty("user.dir");
		//String[] availablePOTCARs = listFilesForFolder(POTCARDir);
		
		List<String> allLines = new ArrayList<String>();
		
		//As of today POTCARs are ~3000 lines in length each, so no problem. This might need changing if they do get significantly longer though.
		for (int a = 0; a < elementNames.length; a++) {
			List<String> currentLines = Files.readAllLines(Paths.get(POTCARDir + "/" + elementNames[a]), Charset.defaultCharset());
			allLines.addAll(currentLines);
		}
		//FileWriter writer = new FileWriter(currentDir + "/POTCAR.out");
		PrintStream writer = new PrintStream(new File(currentDir + "/POTCAR.vasp"));
		for (int a = 0; a < allLines.size(); a++) {
		//	writer.write(allLines.get(a));
			writer.println(allLines.get(a));
		}
		writer.close();
		outPOSCAR.useVASP5Format(true);
		outPOSCAR.writeElementSymbolsOnly(true);
		outPOSCAR.writeFile(currentDir + "/POSCAR.vasp");
		long end = System.currentTimeMillis();
		long total = end - start;
		System.out.println("FINISHED");
		System.out.println("Time spent: " + total + " ms");
	}
	
	private static void generatePRIM (String POTCARDir, String type, String structureFile) throws IOException {
		System.out.println("STARTING");
		long start = System.currentTimeMillis();
		Structure givenStruct = null;
		
		if(type.equals(".cif") || type.equals("cif") || type.equals("CIF") || type.equals(".CIF")) {
			CIF cifFile = new CIF (structureFile, 1.0/720);
			
			for (int siteCount = 0; siteCount < cifFile.numDefiningSites(); siteCount++) {
				double siteOccupancy = cifFile.getSiteOccupancy(siteCount, 0);
				if (siteOccupancy < 1) {
					throw new RuntimeException("This is a partially occupied structure!");
				}
			}
			givenStruct = new Structure (cifFile).findPrimStructure().getCompactStructure();
		} else if(type.equals(".vasp") || type.equals("vasp") || type.equals("POSCAR") || type.equals("poscar") || type.equals("VASP")) {
			POSCAR pos = new POSCAR(structureFile);
			//boolean checkVer = pos.usesVASP5Format();
		/*	if (!pos.usesVASP5Format()) {
				throw new RuntimeException("POSCAR does not use VASP5 format, cannot recognize elements, add the elements individually or use a different format.");
			}*/
			givenStruct = new Structure (pos).findPrimStructure();
		} else {
			throw new RuntimeException("Input file type is not recognized!");
		}
		POSCAR outPOSCAR = new POSCAR(givenStruct, true);
		System.out.println("Processing: " + givenStruct.getDescription());
		String[] elementNames = getElementNames(outPOSCAR);
		String currentDir = System.getProperty("user.dir");
		//String[] availablePOTCARs = listFilesForFolder(POTCARDir);
		
		List<String> allLines = new ArrayList<String>();
		
		//As of today POTCARs are ~3000 lines in length each, so no problem. This might need changing if they do get significantly longer though.
		for (int a = 0; a < elementNames.length; a++) {
			List<String> currentLines = Files.readAllLines(Paths.get(POTCARDir + "/" + elementNames[a]), Charset.defaultCharset());
			allLines.addAll(currentLines);
		}
		//FileWriter writer = new FileWriter(currentDir + "/POTCAR.out");
		PrintStream writer = new PrintStream(new File(currentDir + "/POTCAR.vasp"));
		for (int a = 0; a < allLines.size(); a++) {
		//	writer.write(allLines.get(a));
			writer.println(allLines.get(a));
		}
		writer.close();
		outPOSCAR.useVASP5Format(true);
		outPOSCAR.writeElementSymbolsOnly(true);
		outPOSCAR.writeFile(currentDir + "/POSCAR.vasp");
		long end = System.currentTimeMillis();
		long total = end - start;
		System.out.println("FINISHED");
		System.out.println("Time spent: " + total + " ms");
	}
	
	
	
	public static String[] getElementNames(IStructureData structureFile) {
		Structure currentStructure = new Structure(structureFile);
		Element[] elements = currentStructure.getDistinctElements();
		String[] elementNames = new String[elements.length];
		for (int a = 0; a < elements.length; a++) {
			elementNames[a] = elements[a].getSymbol();
		}
		return elementNames;
	}
	
	public static String[] listFilesForFolder(String dir) {
		File dirName = new File(dir);
		String[] fileFilter = new String[dirName.listFiles().length];
		int counter = 0;
	    for (final File fileEntry : dirName.listFiles()) {
	        if (fileEntry.isFile()) {
	        	fileFilter[counter] = fileEntry.getName();
	        	counter++;
	        } 
	    }
	    
	    //now remove all the nulls (i.e. the names of all the directories.
		List<String> list = new ArrayList<String>();
		for (String s : fileFilter) {
			if(s != null && s.length() > 0) {
				list.add(s);
			}
		}
		fileFilter = list.toArray(new String[list.size()]);
		
		//System.out.println("dirList: " +Arrays.toString(fileFilter));
	    return fileFilter;
	}
	

}
