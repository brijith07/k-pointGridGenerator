package utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import matsci.io.structure.GULP;
import matsci.io.vasp.POSCAR;

public class InputFilesConverter {

	public static void main(String[] args) {
		if(args[0].equals("toGULP")) {
			String strucDir = args[1];
			String[] files = listFilesInFolder(strucDir);
			for (int a = 0; a < files.length; a++) {
				try {
					toGulp(files[a], strucDir);
				} catch (RuntimeException e) {
					System.out.println("Fail to read: " + files[a]);
				}
			}
		}

	}
	
	public static void toGulp(String fileName, String strucDir) {
		POSCAR poscar = new POSCAR(strucDir + "/" + fileName, true);
		GULP gulp = new GULP(poscar);
		String dest = strucDir + "/structure.GULP";
		File file = new File(dest);
		file.mkdir();
		gulp.writeFile(dest + "/" + fileName);
		
	}
	
	public static String[] listFilesInFolder(String dir) {
		File dirName = new File(dir);
		String[] fileFilter = new String[dirName.listFiles().length];
		int counter = 0;
	    for (final File fileEntry : dirName.listFiles()) {
	        if (fileEntry.isFile()) {
	        	fileFilter[counter] = fileEntry.getName();
	        	counter++;
	        } 
	    }
	    
	    //now remove all the nulls (i.e. the names of all the directories.)
		List<String> list = new ArrayList<String>();
		for (String s : fileFilter) {
			if(s != null && s.length() > 0) {
				list.add(s);
			}
		}
		fileFilter = list.toArray(new String[list.size()]);
		
		//System.out.println("dirList: " +Arrays.toString(fileFilter));
	    return fileFilter;
	}

}
