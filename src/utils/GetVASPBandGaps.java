package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class GetVASPBandGaps {

	public static void main(String[] args) throws FileNotFoundException {
		String OUTCARPath = args[0];
		ArrayList<Double[]> tips = new ArrayList<>();
		double directGap = Double.POSITIVE_INFINITY;
		double pastVal = Double.NaN;
		double pastOcc = Double.NaN;
		double currVal = Double.NaN;
		double currOcc = Double.NaN;
		boolean read = false;
		//boolean start = false;
		String delim = "[/,\\[\\]\\s]";
		File file = new File(OUTCARPath);
		Scanner scanner = new Scanner(file);
		while(scanner.hasNextLine()) {
			String currentLine = scanner.nextLine();
			if (currentLine.contains(" k-point")) { //safety check in case no bandgap was found
				read = false;
				continue;
			}
			if (currentLine.contains("  band No.  band energies     occupation")) {
				read = true;
				pastVal = Double.NaN; //reset checker
				currVal = Double.NaN;
				continue;
			}
			if (read) {
				pastVal = currVal; //safe history
				pastOcc = currOcc;
				ArrayList<Double> row = TextFileParser.getDoublesAmongString(currentLine, delim);
				currVal = row.get(1); //update currentValue
				currOcc = row.get(2);
				if (Double.isNaN(pastVal)) { //first step, so move on
					continue;
				}
				if (currOcc < pastOcc) { //we found a bandgap
					if (currOcc > 0) { //partial occupancy, it's a metal
						scanner.close();
						return;
						//return new double[] {0,0};
					}
					if (currOcc < 0) { //some negative number, treat it as 0
						currOcc = 0;
					}
					double val = currVal - pastVal;
					directGap = Math.min(directGap, val);
					Double[] valCon = new Double[] {pastVal, currVal}; //0 is valence tip, 1 is conduction tip
					tips.add(valCon);
					read = false;
				}
			}
			if(currentLine.contains("soft charge-density along")) { //done, no need to continue reading
				break;
			}
		}
		scanner.close();
		double valenceVal = Double.NEGATIVE_INFINITY; //valence
		double conductionVal = Double.POSITIVE_INFINITY; //conduction
		for (int a = 0; a < tips.size(); a++) {
			conductionVal = Math.min(conductionVal, tips.get(a)[1]);
			valenceVal = Math.max(valenceVal, tips.get(a)[0]);
		}
		double indirectGap = conductionVal - valenceVal;
		if (indirectGap < 0) {
			indirectGap = 0;
		}
		//double[] bandGaps = new double[] {directGap, indirectGap}; //0 is direct, 1 is indirect
		System.out.println("Direct Gap: " + directGap);
		System.out.println("Indirect Gap: " + indirectGap);

	}

}
