package generators;
/*
 * Created on Jun 17, 2013
 *
 */

import java.util.Arrays;

import generators.filters.LowSymmetryFilter;
import matsci.location.Vector;
import matsci.location.symmetry.operations.SpaceGroup;
import matsci.structure.BravaisLattice;
import matsci.structure.reciprocal.KPointLattice;
import matsci.structure.superstructure.SuperLattice;
import matsci.structure.superstructure.SuperLatticeGenerator;
import matsci.structure.superstructure.filters.ISuperLatticeFilter;
import matsci.structure.superstructure.filters.SymmetryPreservingFilter;
import matsci.util.MSMath;

/**
 * This is a class that dynamically generates k-point lattices, without looking 
 * them up in a table.  It should be used for low-symmetry lattices, such as
 * triclinic and monoclinic lattices.
 * 
 * @author Tim Mueller
 *
 */
public class LowSymmetryGenerator implements IKPointFactory {
  
  /**
   * Up to a certain size k-point grid (given by m_MaxKPoints), the library
   * of grids will be directly calculated.  Above this size, the grids will be 
   * scaled in each direction by a factor of N, where N is the scale factor.  The 
   * maximum scale factor that will be used is the given by M_MaxScaleFactor.  This
   * number can be set arbitrarily high -- it's just there to prevent the code from
   * trying to generate ridiculously large k-point grids.
   */
  private static int MAX_SCALE_FACTOR = 6;
  
  private final boolean m_IncludeGamma;  // True if the gamma point should be included, false if not.

  //If the gamma point is not included, the returned k-point grids will 
  // have an even number of total k-points (if time reversal symmetry is used)
  //private final int m_Increment;  This is now calculated dynamically by getIncrement();
  
  // The maximum size of the k-point grid we will try to generate.  Above this, use the scale factor
  private final int m_MaxKPoints;
  
  private boolean m_UseOrthogonalDistance = false;
  private boolean m_OnlyDiagonalMatrices = false;
  //private boolean m_AddInversionSymmetry = false;
  
  public LowSymmetryGenerator(int maxKPoints, boolean includeGamma, boolean useOrthogonalDistance, boolean forceDiagonalMatrix) {
    
    this(maxKPoints, includeGamma, useOrthogonalDistance);
    m_OnlyDiagonalMatrices = forceDiagonalMatrix;
    
  }

  public LowSymmetryGenerator(int maxKPoints, boolean includeGamma, boolean useOrthogonalDistance) {
    
    this(maxKPoints, includeGamma);
    m_UseOrthogonalDistance = useOrthogonalDistance;
        
  }
  
  /**
   * 
   * @param maxKPoints The maximum size of the k-point grid we will try to generate.  
   * Above this, use the scale factor
   * @param includeGamma  True if the gamma point should be included in the grid, false if not.
   */
  public LowSymmetryGenerator(int maxKPoints, boolean includeGamma) {
    
    m_MaxKPoints = maxKPoints;
    m_IncludeGamma = includeGamma;
    
  }
  
  public boolean usesOrthogonalDistance() {
    return m_UseOrthogonalDistance;
  }
  
  public boolean forcesDiagonalMatrices() {
    return m_OnlyDiagonalMatrices;
  }
  
  public int getIncrement(SpaceGroup kPointSpaceGroup) {

    if (m_IncludeGamma) {return 1;}
    for (int opNum = 0; opNum < kPointSpaceGroup.numOperations(); opNum++) {
      if (kPointSpaceGroup.getOperation(opNum).isInversion() != null) {
        return 2;
      }
    }
    return 1;
    
  }

  @Override
  public KPointLattice getKPointLattice(SpaceGroup spaceGroup, double minAllowedDistance, int minTotalKPoints, int maxDistinctKPoints) {

    //SpaceGroup kPointSpaceGroup = spaceGroup.getSymmorphicGroup(m_AddInversionSymmetry);
    SpaceGroup kPointSpaceGroup = spaceGroup.getSymmorphicGroup();
    
    int baseIncrement = this.getIncrement(kPointSpaceGroup);
    
    minTotalKPoints = Math.max(minTotalKPoints, 1);
    
    BravaisLattice primLattice = spaceGroup.getLattice();
    int numDimensions = primLattice.numPeriodicVectors();
    
    // First get the minimum scale factor based upon the total number of points
    double roundedMinKPoints = baseIncrement * Math.ceil(1.0 * minTotalKPoints / baseIncrement);
    int minScaleFactor = (int) Math.ceil(Math.pow(roundedMinKPoints / m_MaxKPoints, 1.0 / numDimensions));

    KPointLattice bestLattice = null;
    for (int scaleFactor = minScaleFactor; scaleFactor <= MAX_SCALE_FACTOR; scaleFactor++) {
      
      int increment = baseIncrement / MSMath.GCF(new int[] {scaleFactor, baseIncrement});
      
      double minAllowedDistanceScaled = minAllowedDistance / scaleFactor;
      double minScaledKPointsByDistance = minAllowedDistanceScaled * minAllowedDistanceScaled * minAllowedDistanceScaled / (primLattice.getCellSize() * Math.sqrt(2));
      int roundedMinScaledKPoints = (int) Math.ceil(minScaledKPointsByDistance / increment) * increment; // Should be an even number for shifted
   
      // CHeck to see if the minTotalKPoints is the limiting factor for how low we start.
      double minScaledTotalKPoints = minTotalKPoints / Math.pow(scaleFactor, numDimensions);   
      if (minScaledTotalKPoints > roundedMinScaledKPoints) {
        roundedMinScaledKPoints = (int) Math.ceil(minScaledTotalKPoints / increment) * increment; // Should be an even number for shifted
      }
      
      int maxTotalScaledKPoints = m_MaxKPoints;
      for (int numScaledKPoints = roundedMinScaledKPoints; numScaledKPoints <= maxTotalScaledKPoints; numScaledKPoints += increment) {
        
        // We don't have to recast this in terms of the original structure -- that's done by KPointLibrary
        KPointLattice lattice = this.findKPointLattice(kPointSpaceGroup, minAllowedDistance, numScaledKPoints, scaleFactor);
        
        // TODO make this more exhaustive
        /*if ((lattice != null) && (lattice.numDistinctKPoints() < maxDistinctKPoints)) {
          return lattice;
        }*/
        
        if ((lattice != null) && (lattice.numDistinctKPoints() <= maxDistinctKPoints)) {
          if (lattice.numDistinctKPoints() < maxDistinctKPoints) {
            maxDistinctKPoints = lattice.numDistinctKPoints();

            // This makes sure we do at last as well as using a 2x2x2 scale factor
            maxTotalScaledKPoints = ((numScaledKPoints - 1) / 8) * 8 + 8; // Integer division.
            
            bestLattice = lattice;
          } else if (bestLattice != null && lattice.getRealSpaceMinPeriodicDistance() > bestLattice.getRealSpaceMinPeriodicDistance()) {
            bestLattice = lattice;
          }
          
        }

      }
      
      if (bestLattice != null) {
        return bestLattice;
      }
      
    }
    
    return bestLattice;
    
  }
  
  /**
   * This method calculates the k-point lattice of a given size that has the fewest 
   * distinct k-points.  If there are multiple lattices with the same number of distinct 
   * k-points, it returns the one with the largest distance between real-space lattice points.
   * 
   * @param spaceGroup The space group for the structure for which we are generating k-points
   * @param minAllowedDistance  Every k-point grid corresponds to a real-space supercell.
   * The min periodic distance is the shortest distance between two lattice points of that 
   * supercell.
   * @param numKPoints The total number of desired k-points per reciprocal space unit cell.  
   * The value for this number can be calculated using getNumTotalKPoints
   * @param scaleFactor The factor by which to scale the k-point lattice.
   * @return A reciprocal-space superlattice containing all of the k-points.  The
   * returned lattice will represent the reciprocal lattice of the given space group,
   * and the underlying primitive lattice (accessed by getPrimLattice()) will return 
   * the k-point lattice.
   */
  private KPointLattice findKPointLattice(SpaceGroup symmorphicGroup, double minAllowedDistance, int numScaledKPoints, int scaleFactor) {

    //System.out.println(numScaledKPoints);
    //BravaisLattice primLattice = symmorphicGroup.getLattice();
    
    BravaisLattice primLattice = symmorphicGroup.getLattice().getCompactLattice();
    symmorphicGroup = symmorphicGroup.getFactoredSpaceGroup(primLattice);
    
    int numDimensions = symmorphicGroup.getLattice().numPeriodicVectors();
    
    LowSymmetryFilter filter = this.fastDiagonalFilter(symmorphicGroup, minAllowedDistance, numScaledKPoints, scaleFactor);
    
    SuperLatticeGenerator generator = new SuperLatticeGenerator(numScaledKPoints, numDimensions);
    
    ISuperLatticeFilter[] filters = new ISuperLatticeFilter[] {
       new SymmetryPreservingFilter(symmorphicGroup),
       filter,
    };
    
    int[][][] superLattices = m_OnlyDiagonalMatrices ? 
        generator.generateDiagonalSuperLattices(filters) :
        generator.generateSuperLattices(filters);

    SuperLattice returnLattice = null;

    if (!filter.foundLattice()) {return null;}
    
    int[][] superToDirect = filter.getBestKnownSuperToDirect();
    SuperLattice superLattice = new SuperLattice(filter.getLattice(), superToDirect);
    
    BravaisLattice kPointLattice = superLattice.getInverseLattice();
    
    if (!m_IncludeGamma) {
      Vector shift = new Vector(filter.getBestKnownShift(), kPointLattice.getLatticeBasis()); 
      kPointLattice = kPointLattice.translateBy(shift);
    }
    
    SuperLattice reciprocalLattice = new SuperLattice(kPointLattice, MSMath.transpose(superToDirect));
    /*
    double[] weights = new double[numScaledKPoints * (int) Math.pow(scaleFactor, numDimensions)];
    int numDistinctKPoints = 0;
    for (int pointNum = 0; pointNum < reciprocalLattice.numPrimCells(); pointNum++) {
      Coordinates coords = reciprocalLattice.getPrimCellOriginCoords(pointNum);
      int minMappedKPoint = Integer.MAX_VALUE;
      for (int opNum = 0; opNum < symmorphicGroup.numOperations(); opNum++) {
        SymmetryOperation op = symmorphicGroup.getOperation(opNum);
        Coordinates oppedPoint = op.operate(coords);
        if (!kPointLattice.isLatticePoint(oppedPoint)) {
          System.out.println("Failed map!");
        }
        int oppedKPointNumber = reciprocalLattice.getNearbyPrimCellOriginNumber(oppedPoint);
        minMappedKPoint = Math.min(minMappedKPoint, oppedKPointNumber);
      }
      if (weights[minMappedKPoint] == 0) {
        numDistinctKPoints++;
      }
      weights[minMappedKPoint]++;
    }
      
      double distance = m_UseOrthogonalDistance ? 
          superLattice.getMinOrthogonalDistance() :
          superLattice.getMinPeriodicDistance();
          
      //System.out.println(kPointLattice.translateTo(CartesianBasis.getInstance().getOrigin()).getCrystalFamily());

      //System.out.println(numDistinctKPoints + ", " + filter.getBestKnownDistinctKPoints() + ", " + distance + ", " + filter.getBestKnownDistance());
*/
      returnLattice = reciprocalLattice;//System.out.println(returnLattice.getMinPeriodicDistance()  + ", " + minNumDistinctKPoints + ", " + numKPoints + ", " + numKPoints * scaleFactor * scaleFactor * scaleFactor);      

    return new KPointLattice(returnLattice, symmorphicGroup.getOperations());
     
  }
  
  private LowSymmetryFilter fastDiagonalFilter(SpaceGroup spaceGroup, double scaledMinAllowedDistance, int numScaledKPoints, int scaleFactor) {

    // We do a fast search through all previous diagonal matrices to make sure we do at least 
    // as well as those with the minPeriodicDistance.
    
    SuperLatticeGenerator generator = new SuperLatticeGenerator(numScaledKPoints, spaceGroup.getLattice().numPeriodicVectors());

    LowSymmetryFilter returnFilter = new LowSymmetryFilter(spaceGroup, scaledMinAllowedDistance, Integer.MAX_VALUE, 0, m_IncludeGamma, scaleFactor);
    
    ISuperLatticeFilter[] filters = new ISuperLatticeFilter[] {
        new SymmetryPreservingFilter(spaceGroup),
        returnFilter,
    };
    
    generator.generateDiagonalSuperLattices(filters);
    
    return returnFilter;

  }
    
}
