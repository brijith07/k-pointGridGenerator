/*
 * Created on Feb 26, 2016
 *
 */
package generators;

import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.AbstractBasis;
import matsci.location.basis.CartesianBasis;
import matsci.location.basis.LinearBasis;
import matsci.location.symmetry.operations.SpaceGroup;
import matsci.structure.BravaisLattice;
import matsci.structure.Structure;
import matsci.structure.StructureBuilder;
import matsci.util.arrays.ArrayUtils;

public class GapStretcher {
    
    private Structure m_OriginalStructure;              // User input structure
    private double m_GapDistance;                       // GAPDISTANCE, default = 7.0 Angstroms
    private double m_MinPeriodicDistance;               // MINDINSTACE
    private int m_MinTotalKPoints;                      // MINTOTALKPOINTS, default = 1
    private BravaisLattice m_ContiguousVectorLattice;   // Actual periodic lattices
    private Structure m_StretchedStructure;
    private int m_NumContiguousDimensions;              // Actual periodic dimensions
    private static double STRETCHER_PROJECTION_TOLERANCE = 
            Math.sin(CartesianBasis.getAngleToleranceRadians()) * 1E-6;

    public GapStretcher(Structure structure, double gapDistance, double minPeriodicDistance, 
            int minTotalKPoints) {
        m_OriginalStructure = structure;             
        m_GapDistance = gapDistance;
        // If user uses MINTOTALKPOINTS or KPPRA, the minPeriodicDistance will be 28.1.
        m_MinPeriodicDistance = minPeriodicDistance; 
        m_MinTotalKPoints = minTotalKPoints;         
        m_ContiguousVectorLattice = this.getContiguousVectors();
        m_NumContiguousDimensions = m_ContiguousVectorLattice.numPeriodicVectors();
        m_StretchedStructure = this.scaleGaps();
    }
    
    public Structure getOriginalStructure() {
        return m_OriginalStructure;
    }
    
    public double getGapDistance() {
        return m_GapDistance;
    }
    
    public int getMinTotalKPoints() {
        return m_MinTotalKPoints;
    }
    
    public Structure getStretchedStructure() {
        return m_StretchedStructure;
    }
    
    /**
     * 
     * @return 3 for bulk crystal
     *                 2 for slab 
     *                 1 for nanowires
     *                 0 for isolated atom or nanoparticles
     */
    public int numContiguousDimensions() {
        return m_ContiguousVectorLattice.numPeriodicVectors();
    }
    
    /**
     * Stretch the vacuum to make sure that along the vacuum directions, the stretched lattice 
     * has at least a length of the MINDISTANCE for clusters, and 2 * MINDISTANCE for nanowires 
     * and slabs. This ensure the vector along the vacuum direction being not the shortest one, and
     * the actual minimum periodic distance is achieved by that of the periodic low-dimensional 
     * lattice.
     * 
     * Note, the stretch direction is orthogonal to the lattice defined by contiguous vectors, not
     * necessarily one of the three lattice vectors.
     * 
     * @return the stretched structure if the user input is a not a bulk one.
     */
    private Structure scaleGaps() {
        Structure structure = m_OriginalStructure; // Just to keep notation simple.
        double minSuperCellVolume = structure.getDefiningVolume() * m_MinTotalKPoints;
        double stretchDistance = m_MinPeriodicDistance;
        
        if (m_NumContiguousDimensions == 0) {
            // Cluster.
            // The maximum lattice constant for a fixed volume is that of the FCC, for which 
            // V = sqrt(2)/2 * a ^ 3.
            stretchDistance = 
                    Math.max(stretchDistance, Math.cbrt(minSuperCellVolume / Math.sqrt(2))); 
        } else if (m_NumContiguousDimensions == 1) {
            // Nanowire.
            double minPeriodicLength = 
                    Math.max(m_MinPeriodicDistance, m_ContiguousVectorLattice.getCellSize());
            // Maximum side length for a parallelogram with a fixed area is that of a 
            // hexagonal unit cell. 
            stretchDistance = Math.max(stretchDistance, 
                    Math.sqrt(2 * (minSuperCellVolume / minPeriodicLength) / Math.sqrt(3)));
            // Use an approximated upper bound of the actual minPeriodic distance. 
            // This should prevent the orthogonal vector from being the shortest in superlattices. 
            stretchDistance *= 2;
        } else if (m_NumContiguousDimensions == 2) {
            // Slab.
            // The maximum area of a parallelogram with a fixed side legnth, m_MinPeriodicDistance,
            // is that of a hexagonal unit cell. 
            double minPeriodicArea = 
                    Math.max(m_MinPeriodicDistance * m_MinPeriodicDistance * Math.sqrt(3) / 2, 
                             m_ContiguousVectorLattice.getCellSize());
            stretchDistance = Math.max(stretchDistance, minSuperCellVolume / minPeriodicArea);
            // Use an approximated upper bound of the actual minPeriodic distance. 
            // This should prevent the orthogonal vector from being the shortest in superlattices. 
            stretchDistance *= 2;
        }
        
        if (m_ContiguousVectorLattice.numPeriodicVectors() 
                == m_ContiguousVectorLattice.numTotalVectors()) {
            return structure;   // User input structure is a bulk structure. Don't modify.
        }
        
        // Get the Minkowski-reduced basis of the original input lattice, which is the basis
        // that defines the smallest unit cell in a lattice. We could also use input lattice 
        // vectors here, but compact lattice vectors give more numerical stability.
        // Note: the Minkwski-reduced basis might not be mutually orthogonal.
        Vector[] compactVectors = structure.getDefiningLattice()
                                           .getCompactLattice()
                                           .getCellVectors();
        double scaleRatio = 0;
        
        stretchDistance = stretchDistance + CartesianBasis.getPrecision(); 
        
        for (int vecNum = 0; vecNum < compactVectors.length; vecNum++) {
            
            Vector compactVector = compactVectors[vecNum];
            if (m_ContiguousVectorLattice.isOnLatticePlane(compactVector)) { continue; }
            // Get the projection along the vacuum directions. 
            Vector normalVector = m_ContiguousVectorLattice.removeLattice(compactVector);
            double newScaleRatio = stretchDistance / normalVector.length();
            scaleRatio = Math.max(scaleRatio, newScaleRatio);
        }
        
        // The stretchDistance is already smaller than the compact vector of original lattice that
        // is not in the periodic sublattice.
        if (scaleRatio <= 1) { return structure; } 
        
        // Stretch the non-periodic directions.
        Vector[] stretchVectors = m_ContiguousVectorLattice.getNonPeriodicVectors(); 
        Vector[] cellVectors = structure.getCellVectors();
        for (int vecNum = 0; vecNum < cellVectors.length; vecNum++) {
            // Note: this judgment is on the original lattice, not the contiguousLattice.
            if (!structure.getDefiningLattice().isDimensionPeriodic(vecNum)) { continue; }
            Vector cellVector = cellVectors[vecNum];
            for (int stretchNum = 0; stretchNum < stretchVectors.length; stretchNum++) {
                Vector stretchVector = stretchVectors[stretchNum];
                Vector unitVector = stretchVector.unitVectorCartesian();
                double projection = cellVector.innerProductCartesian(unitVector);
                // Deals with some numerical issues 
                // TODO get rid of this -- it's to compensate for an EJML bug
                if (Math.abs(projection) > STRETCHER_PROJECTION_TOLERANCE) {
                    cellVector = cellVector.add(unitVector.resize(projection * (scaleRatio - 1)));
                }
            }
            cellVectors[vecNum] = cellVector;
        }
        
        // Generate the stretched structure:
        StructureBuilder builder = new StructureBuilder(structure);
        builder.setCellVectors(cellVectors);
        
        // Transform the atomic positions from input structure to stretched structure.
        AbstractBasis origBasis = structure.getDirectBasis();
        AbstractBasis newBasis = new LinearBasis(cellVectors);
        for (int siteNum = 0; siteNum < builder.numDefiningSites(); siteNum++) {
            // Get fractional coordinates in the old basis first, and then apply to new basis.
            // We are not using it in a DFT calculation, so even if the atomic spacing is enlarged, 
            // it's fine, as long as the symmetries are intact.
            double[] coordArray = builder.getSiteCoords(siteNum).getCoordArray(origBasis);
            Coordinates newCoords = new Coordinates(coordArray, newBasis);
            builder.setSiteCoordinates(siteNum, newCoords);
        }
        
        return new Structure(builder);
    }
    
    /**
     * Find the actual periodic directions (sublattice) in a structure. 
     * 
     * For slabs, the periodic directions will be the ones parallel to the surface, not necessarily
     * parallel to any of the three planes defined by pairs of lattice vector. For nanowires, the 
     * periodic direction will be the direction along the wire. For nanoparticles or isolated atoms, 
     * there is no periodic direction.
     *                    
     * Whether a direction is determined as containing vacuum is decided by the GAPDISTANCE in 
     * PRECALC file. 
     * 
     * @return The BravaisLattice (sublattice) formed by finite lattice vectors for periodic 
     *         dimensions and infinite vectors for non-periodic dimensions. 
     */
    private BravaisLattice getContiguousVectors() {
        
        Vector[] knownVectors = new Vector[0];
        BravaisLattice knownLattice = new BravaisLattice(knownVectors);
        Coordinates[] seenSites = new Coordinates[m_OriginalStructure.numDefiningSites()];

        for (int siteNum = 0; siteNum < m_OriginalStructure.numDefiningSites(); siteNum++) {
            if (seenSites[siteNum] != null) { continue; }
            Structure.Site site = m_OriginalStructure.getDefiningSite(siteNum);
            seenSites[siteNum] = site.getCoords();
            knownLattice = findContiguousVectors(seenSites, siteNum, knownLattice);
        }
        
        return knownLattice;
    }
    
    /**
     * This function is iteratively called to find the periodic directions and the corresponding 
     * lattice vectors. The direction with vacuum will be determined as non-periodic, and the 
     * lattice vector(s) parallel to this direction will be marked as "infinite vector".
     * 
     * @param seenSites The sites that has been traversed.
     * @param siteIndex The index of the defining site used to get all its neighbors.
     * @param knownLattice The already found lattice up to now.
     * @return A BravaisLattice object formed by the knowLattice. 
     */
    private BravaisLattice findContiguousVectors(Coordinates[] seenSites, int siteIndex, 
            BravaisLattice knownLattice) {
    
        Coordinates siteCoords = seenSites[siteIndex];
        Structure.Site[] neighbors = m_OriginalStructure.getNearbySites(siteCoords, 
                m_GapDistance, false);

        for (int neighborNum = 0; neighborNum < neighbors.length; neighborNum++) {
            Structure.Site neighbor = neighbors[neighborNum];
            int neighborIndex = neighbor.getIndex();
            if (seenSites[neighborIndex] == null) {
                seenSites[neighborIndex] = neighbor.getCoords();
                knownLattice = findContiguousVectors(seenSites, neighborIndex, knownLattice);
            } else {
                Vector newVector = new Vector(seenSites[neighborIndex], neighbor.getCoords());
                if (newVector.length() < CartesianBasis.getPrecision()) { continue; }
                if (!knownLattice.isParallelToLatticePlane(newVector)) {
                    Vector[] periodicVectors = knownLattice.getPeriodicVectors();
                    periodicVectors = (Vector[]) ArrayUtils.appendElement(periodicVectors, 
                                        newVector);
                    knownLattice = new BravaisLattice(periodicVectors);
                }
            }
            if (knownLattice.numPeriodicVectors() == knownLattice.numTotalVectors()) {
                return knownLattice;
            }
        }
        return knownLattice;
    }
}
