/*
 * Created on Mar 16, 2019
 *
 */
package generators;

import java.util.Arrays;

import generators.KPointLatticeGenerator.KPointLattice;
import matsci.location.basis.CartesianBasis;
import matsci.location.symmetry.operations.SpaceGroup;
import matsci.structure.BravaisLattice;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;

/**
 * This can be used with any crystal class other than triclinic.
 * 
 * @author Tim Mueller
 *
 */
public class KPointLatticeGenerator {

  private final double m_MaxZLength;
  private final double[][] m_PrimVectors;
  private final double[][] m_ConventionalVectors;
  private final double[][] m_CartesianToPrim;
  
  private final double m_NumConventionalPrimCells; // Keep this as a double to avoid int division issues.
  private final double m_PrimCellSize;

  private final int[][][] m_PointOperators2D;
  private final int[][][] m_PointOperators3D;
  
  private final int[][][] m_KPointOperators;  // This is global just so we don't need to reallocate this array each time.
  
  private final double[][] m_Shifts2D;
  private double[][] m_KPointShifts;
  
  public KPointLatticeGenerator(SpaceGroup spaceGroup) {
    
    this(spaceGroup.getLattice().getLatticeBasis().getBasisToCartesian(), spaceGroup.getConventionalLattice().getLatticeBasis().getBasisToCartesian(), spaceGroup.getLatticePointOperators(true), (spaceGroup.getCrystalFamily() == BravaisLattice.CrystalFamily.HEXAGONAL_3D));
    
  }
  
  /**
   * You can get all of the arguments from a SpaceGroup object, but this is more general.
   * 
   * @param primVectors
   * @param conventionalVectors The third vector should be orthogonal to the first two.
   * @param latticePointOperators
   * @param isHexagonal
   */
  public KPointLatticeGenerator(double[][] primVectors, double[][] conventionalVectors, int[][][] latticePointOperators, boolean isHexagonal) {
    
    m_PointOperators3D = ArrayUtils.copyArray(latticePointOperators);
    m_KPointOperators = new int[m_PointOperators3D.length][][];

    m_PrimVectors = ArrayUtils.copyArray(primVectors);
    m_CartesianToPrim = MSMath.simpleInverse(primVectors);
    m_PrimCellSize = Math.abs(MSMath.determinant(primVectors));
    
    // Check to see if we have a face-centered 2D lattice.  We need to work with the primitive 2D lattice.
    double[] faceCenter = MSMath.arrayAdd(conventionalVectors[0], conventionalVectors[1]);
    double[] directFaceCenter = MSMath.vectorTimesMatrix(faceCenter, m_CartesianToPrim);
    boolean isLatticePoint = true;
    for (int dimNum = 0; dimNum < directFaceCenter.length; dimNum++) {
      int coord = (int) Math.round(directFaceCenter[dimNum]);
      isLatticePoint &= (coord %2 == 0);
    }
    if (isLatticePoint) {
      conventionalVectors[0] = MSMath.arrayDivide(faceCenter, 2);
      conventionalVectors[1] = MSMath.arraySubtract(conventionalVectors[0], conventionalVectors[1]);
    }
    m_ConventionalVectors = ArrayUtils.copyArray(conventionalVectors);
    
    // TODO make this more sophisticated to minimize the number of shifts that need to be evaluated.
    if (isHexagonal) {
      m_Shifts2D = m_Hexagonal2DShifts;
    } else {
      m_Shifts2D = m_Other2DShifts;
    }
    
    double[][] conventionalToPrim = MSMath.matrixMultiply(conventionalVectors, m_CartesianToPrim);
    double[][] primToConventional = MSMath.simpleInverse(conventionalToPrim);
    
    int[][][] ops2D = new int[m_PointOperators3D.length][][];
    
    // First we get all of the lattice operations in the coordinates of the conventional vectors
    int keeperIndex = 0;
    for (int opNum = 0; opNum < m_PointOperators3D.length; opNum++) {
      int[][] op = m_PointOperators3D[opNum];
      double[][] conventionalOp = MSMath.matrixMultiply(conventionalToPrim, op);
      conventionalOp = MSMath.matrixMultiply(conventionalOp, primToConventional);
      if (Math.round(conventionalOp[0][2]) != 0) {continue;}
      if (Math.round(conventionalOp[1][2]) != 0) {continue;}
      if (Math.round(conventionalOp[2][0]) != 0) {continue;}
      if (Math.round(conventionalOp[2][1]) != 0) {continue;}
      int[][] op2D = new int[][] {
        {(int) Math.round(conventionalOp[0][0]), (int) Math.round(conventionalOp[0][1])},
        {(int) Math.round(conventionalOp[1][0]), (int) Math.round(conventionalOp[1][1])},
      };
      
      boolean match = false;
      for (int prevOpNum = 0; prevOpNum < keeperIndex; prevOpNum++) {
        int[][] prevOp = ops2D[prevOpNum];
        match |= ArrayUtils.equals(prevOp, op2D);
        if (match) {break;}
      }
      if (match) {continue;}
      ops2D[keeperIndex++] = op2D;
    }
    
    m_NumConventionalPrimCells = Math.abs(MSMath.determinant(conventionalToPrim));
    m_MaxZLength = MSMath.magnitude(conventionalVectors[2]) * (isHexagonal ? 3 : 2);  // Factor of 3 is for rhombohedral.
    m_PointOperators2D = ArrayUtils.truncateArray(ops2D, keeperIndex);
    
  }
  
  public KPointLattice getKPointLattice(double minDistance, int minSize, int scaleFactor) {
    
    return this.getKPointLattice(minDistance, minSize, Integer.MAX_VALUE, scaleFactor);
    
  }
  
  public int numPointOperators3D() {
    return m_PointOperators3D.length;
  }
  
  /**
   * This should be pretty self-explanatory.
   * 
   * @param minDistance
   * @param scaledMinSize
   * @return
   */
  public KPointLattice getKPointLattice(double minScaledDistance, int scaledMinSize, int scaledMaxSize, int scaleFactor) {
    
    KPointLattice bestKnownLattice = new KPointLattice(null, null, Integer.MAX_VALUE, 0);
    
    // We can't do better than fcc packing.
    int minSizeByDistance = (int) Math.floor(minScaledDistance * minScaledDistance * minScaledDistance / (m_PrimCellSize * Math.sqrt(2)));
    scaledMinSize = Math.max(scaledMinSize, minSizeByDistance);
    
    for (int scaledSize = scaledMinSize; scaledSize <= scaledMaxSize; scaledSize++) {
      if (isTriclinic()) {
        bestKnownLattice = this.getKPointLatticeTriclinic(scaledSize, minScaledDistance, bestKnownLattice, scaleFactor);
      } else {
        bestKnownLattice = this.getKPointLatticeOrthogonal(scaledSize, minScaledDistance, bestKnownLattice, scaleFactor);
      }
      if (bestKnownLattice.m_NumDistinctKPoints == Integer.MAX_VALUE) {continue;}
      scaledMaxSize = bestKnownLattice.m_NumDistinctKPoints * m_PointOperators3D.length;
      // The bestKnownLattice returned has already counted in the scaleFactor.
      scaledMaxSize = (int) Math.floor(1.0 * scaledMaxSize / Math.pow(scaleFactor, 3));
    }
    return bestKnownLattice;
  }
  
  public boolean isTriclinic() {
    if (m_PointOperators3D.length > 2) {return false;}
    for (int opNum = 0; opNum < m_PointOperators3D.length; opNum++) {
      int[][] op = m_PointOperators3D[opNum];
      if (!(isInverse(op) || isIdentity(op))) {return false;}
    }
    return true;
  }
  
  // This could probably be moved to utilities
  public static boolean isInverse(int[][] operation) {
    if (!Arrays.equals(operation[0], new int[] {-1, 0, 0})) {return false;}
    if (!Arrays.equals(operation[1], new int[] {0, -1, 0})) {return false;}
    if (!Arrays.equals(operation[2], new int[] {0, 0, -1})) {return false;}
    return true;
  }
  
  // This could probably be moved to utilities
  public static boolean isIdentity(int[][] operation) {
    if (!Arrays.equals(operation[0], new int[] {1, 0, 0})) {return false;}
    if (!Arrays.equals(operation[1], new int[] {0, 1, 0})) {return false;}
    if (!Arrays.equals(operation[2], new int[] {0, 0, 1})) {return false;}
    return true;
  }
  
  public void includeGamma(KPointLibrary.INCLUDE_GAMMA includeGamma) {

    if (includeGamma == KPointLibrary.INCLUDE_GAMMA.TRUE) {
      m_KPointShifts = new double[][] {
        {0, 0, 0}
      };
    } else if (includeGamma == KPointLibrary.INCLUDE_GAMMA.AUTO) {
      m_KPointShifts = new double[][] {
        {0, 0, 0},
        {0, 0, 0.5},
        {0, 0.5, 0},
        {0.5, 0, 0},
        {0.5, 0.5, 0},
        {0.5, 0, 0.5},
        {0, 0.5, 0.5},
        {0.5, 0.5, 0.5},
      };
    } else if (includeGamma == KPointLibrary.INCLUDE_GAMMA.FALSE) {
      m_KPointShifts = new double[][] {
        {0, 0, 0.5},
        {0, 0.5, 0},
        {0.5, 0, 0},
        {0.5, 0.5, 0},
        {0.5, 0, 0.5},
        {0, 0.5, 0.5},
        {0.5, 0.5, 0.5},
      };
    }
  }
  
  private KPointLattice getKPointLatticeTriclinic(int scaledSize, double minAllowedScaledDistance, KPointLattice bestKnownLattice, int scaleFactor) {

      boolean includeGamma = (m_KPointShifts.length == 1);
      if (!includeGamma && ((scaledSize * scaleFactor * scaleFactor * scaleFactor) % m_PointOperators3D.length != 0)) { // This can cause problems with scale factors.
        return bestKnownLattice;
      }
      double minAllowedDistance = minAllowedScaledDistance * scaleFactor;
      
      // Figure out what the minimum possible number of k-points is.
      int gammaFactor = includeGamma ? 1 : 0;
      int minPossibleKPoints  = (int) Math.ceil((scaledSize * scaleFactor * scaleFactor * scaleFactor - gammaFactor) / (1.0 + m_PointOperators3D.length)) + gammaFactor;
      if (minPossibleKPoints > bestKnownLattice.getNumDistinctKPoints()) {return bestKnownLattice;}
      
      // Loop over all possible superlattices.
      int[][] factorSets = MSMath.getFactorSets(scaledSize, 3);
      double[][] superVectors = new double[3][3];;
      for (int setNum = 0; setNum < factorSets.length; setNum++) {
        int[] factors = factorSets[setNum];
        
        int[][] superToDirect = new int[][] {
          {factors[0] * scaleFactor, 0, 0},
          {0, factors[1] * scaleFactor, 0},
          {0, 0, factors[2] * scaleFactor}
        };
        
        superVectors[0] = MSMath.vectorTimesMatrix(superToDirect[0], m_PrimVectors, superVectors[0]);
        if (MSMath.magnitude(superVectors[0]) < minAllowedDistance) {continue;}
        for (int ba = 0; ba < factors[0]; ba++) {
          superToDirect[1][0] = ba * scaleFactor;
          superVectors[1] = MSMath.vectorTimesMatrix(superToDirect[1], m_PrimVectors, superVectors[1]);
          if (getMinDistance(ArrayUtils.copyArray(superVectors), 2) < minAllowedDistance) {continue;}
          for (int ca = 0; ca < factors[0]; ca++) {
            superToDirect[2][0] = ca * scaleFactor;
            for (int cb = 0; cb < factors[1]; cb++) {
              superToDirect[2][1] = cb * scaleFactor;
              superVectors[2] = MSMath.vectorTimesMatrix(superToDirect[2], m_PrimVectors, superVectors[2]);
              if (MSMath.magnitude(superVectors[2]) < minAllowedDistance) {continue;}
              double minDistance = getMinDistance(ArrayUtils.copyArray(superVectors), 3);
              if (minDistance < minAllowedDistance) {continue;}
              if ((bestKnownLattice.getNumDistinctKPoints() == minPossibleKPoints) && (minDistance < bestKnownLattice.getMinPeriodicDistance())) {continue;}
              int numTotalKPoints = superToDirect[0][0] * superToDirect[1][1] * superToDirect[2][2];
              for (int shiftNum = 0; shiftNum < m_KPointShifts.length; shiftNum++) {
                double[] shift = m_KPointShifts[shiftNum];
                int numDistinctKPoints = (m_PointOperators3D.length == 1) ? numTotalKPoints : this.numDistinctKPoints(superToDirect, shift);
                if (numDistinctKPoints < bestKnownLattice.getNumDistinctKPoints()) {
                  bestKnownLattice = new KPointLattice(superToDirect, shift, numDistinctKPoints, minDistance);
                } else if (numDistinctKPoints == bestKnownLattice.getNumDistinctKPoints() && minDistance > bestKnownLattice.getMinPeriodicDistance() + CartesianBasis.getPrecision()) {
                  bestKnownLattice = new KPointLattice(superToDirect, shift, numDistinctKPoints, minDistance);
                } else if (numDistinctKPoints == bestKnownLattice.getNumDistinctKPoints() && Math.abs(minDistance - bestKnownLattice.getMinPeriodicDistance()) <= CartesianBasis.getPrecision() && numTotalKPoints > bestKnownLattice.numTotalKPoints()) {
                  bestKnownLattice = new KPointLattice(superToDirect, shift, numDistinctKPoints, minDistance);
                }
                
                if (numDistinctKPoints == minPossibleKPoints) {break;}
              }
            }
          }
        }
      }
      
      return bestKnownLattice;
      
    }
  
  /**
   * The main routine.
   * 
   * @param scaledSize
   * @param minAllowedScaledDistance
   * @param bestKnownLattice
   * @return
   */
  private KPointLattice getKPointLatticeOrthogonal(int scaledSize, double minAllowedScaledDistance, KPointLattice bestKnownLattice, int scaleFactor) {

    /**
     * First we generate the 2D lattices
     */
    int[] factors = MSMath.factor(scaledSize);
    double[][] superVectors = new double[3][3];
    for (int factorNum = factors.length - 1; factorNum >= 0; factorNum--) {
      int factor = factors[factorNum];
      if ((factor * m_MaxZLength / m_NumConventionalPrimCells) < minAllowedScaledDistance) {continue;}
      int[][][] lattices2D = getSymPreservingLattices2D(scaledSize / factor);
      
      for (int latticeNum = 0; latticeNum < lattices2D.length; latticeNum++) {
        
        int[][] lattice2D = lattices2D[latticeNum];
        for (int dimNum = 0; dimNum < 3; dimNum++) {
          superVectors[0][dimNum] = m_ConventionalVectors[0][dimNum] * lattice2D[0][0] +  m_ConventionalVectors[1][dimNum] * lattice2D[0][1];
          superVectors[1][dimNum] = m_ConventionalVectors[0][dimNum] * lattice2D[1][0] +  m_ConventionalVectors[1][dimNum] * lattice2D[1][1];
        }
        if (getMinDistance(ArrayUtils.copyArray(superVectors), 2) < minAllowedScaledDistance) {continue;}
        
        /**
         * Now we shift the relative layers of the stacked 2D lattices and evaluate the resulting 3D lattice.
         */
        for (int shiftNum = 0; shiftNum < m_Shifts2D.length; shiftNum++) {

          double[] shift = m_Shifts2D[shiftNum];
          for (int dimNum = 0; dimNum < 3; dimNum++) {
            superVectors[2][dimNum] = superVectors[0][dimNum] * shift[0] + superVectors[1][dimNum] * shift[1] + m_ConventionalVectors[2][dimNum] * (factor / m_NumConventionalPrimCells);
          }
          double[][] superToDirectDouble = MSMath.matrixMultiply(superVectors, m_CartesianToPrim);
          
          // Important to create a new one each time, as it's stored without copying in KPointLattice.
          int[][] superToDirect = new int[3][3];
          boolean allInts = true;
          for (int rowNum = 0; rowNum < superToDirect.length; rowNum++) {
            double[] rowDouble = superToDirectDouble[rowNum];
            int[] row = superToDirect[rowNum];
            for (int colNum = 0; colNum < row.length; colNum++) {
              row[colNum] = (int) Math.round(rowDouble[colNum]);
              double delta = Math.abs(row[colNum] - rowDouble[colNum]);
              allInts &= (delta < 1E-2);
            }
          }
          if (!allInts) {
            continue;
          }

          double minDistance = getMinDistance(ArrayUtils.copyArray(superVectors), 3);
          if (minDistance < minAllowedScaledDistance) {continue;}

          // This is the simplest way to take care of the scale factor.
          for (int rowNum = 0; rowNum < superToDirect.length; rowNum++) {
            for (int colNum = 0; colNum < superToDirect[rowNum].length; colNum++) {
              superToDirect[rowNum][colNum] *= scaleFactor;
            }
          }
          minDistance *= scaleFactor;
          
          toHermiteNormalForm(superToDirect);
          
          if (!isSymmetryPreserving(superToDirect, m_PointOperators3D)) {continue;}
          for (int kPointShiftNum = 0; kPointShiftNum < m_KPointShifts.length; kPointShiftNum++) {
            double[] kPointShift = m_KPointShifts[kPointShiftNum];
            int numTotalKPoints = MSMath.determinant(superToDirect);
            int numDistinctKPoints = numDistinctKPoints(superToDirect, kPointShift);
            if (numDistinctKPoints > bestKnownLattice.m_NumDistinctKPoints) {
                continue;
            } else if (numDistinctKPoints == bestKnownLattice.m_NumDistinctKPoints) {
              if (minDistance < bestKnownLattice.m_MinPeriodicDistance - CartesianBasis.getPrecision()) {
                  continue;
              } else if (Math.abs(minDistance - bestKnownLattice.m_MinPeriodicDistance) <= CartesianBasis.getPrecision()
                      && numTotalKPoints <= bestKnownLattice.numTotalKPoints()) {
                  continue;
              }
              // TODO: dist kpoints, total kpoints, minDistance are all equal. --> compare compactness.
            }
            bestKnownLattice = new KPointLattice(superToDirect, kPointShift, numDistinctKPoints, minDistance);
          }
        }
      }
    }
    
    return bestKnownLattice;
    
  }
  
  /**
   * This is one of the most expensive operations.  I try to make it fast by pre-calculating the 
   * operators, but it still takes some time.
   * 
   * @param superToDirect
   * @param shiftArray This should be in the coordinates of the k-point lattice.
   * @return
   */
  private int numDistinctKPoints(int[][] superToDirect, double[] shiftArray) {
    
    // Apply the operation in k-space
    double[][] recipSTD = MSMath.simpleLowerTriangularInverse(superToDirect);
    for (int opNum = 0; opNum < m_KPointOperators.length; opNum++) {
      int[][] opInverse = MSMath.matrixMultiply(superToDirect, m_PointOperators3D[opNum]);
      m_KPointOperators[opNum] = MSMath.round(MSMath.matrixMultiply(opInverse, recipSTD));
    }
    
    int[][] kPointCanonical = MSMath.transpose(superToDirect);
    // kPointCanonical is not necessarily diagonal here. 
    // e.g. superToDirect = {{78, 54, 39}, {0, 1, 0}, {0, 0, 4}};
    toHermiteNormalForm(kPointCanonical);       

    int numDistinctKPoints = 0;
    int[] currKPoint = new int[3];
    double[] shiftedPoint = new double[currKPoint.length];
    int[] mappedPointIntArray = new int[currKPoint.length];
    int mappedIndex = -1;
    
    for (int i = 0; i < kPointCanonical[0][0]; i++) {
      currKPoint[0] = i;
      for (int j = 0; j < kPointCanonical[1][1]; j++) {
        currKPoint[1] = j;
        for (int k = 0; k < kPointCanonical[2][2]; k++) {
          currKPoint[2] = k;
          
          int currIndex = currKPoint[0] * kPointCanonical[1][1] * kPointCanonical[2][2] + currKPoint[1] * kPointCanonical[2][2] + currKPoint[2];

          for (int opNum = 0; opNum < m_KPointOperators.length; opNum++) {
            for (int dimNum = 0; dimNum < currKPoint.length; dimNum++) {
              shiftedPoint[dimNum] = currKPoint[dimNum] + shiftArray[dimNum];
            }
            
            // Putting the vector last is faster than taking the transpose of the op.
            double[] mappedPoint = MSMath.matrixTimesVector(m_KPointOperators[opNum], shiftedPoint); 
            for (int dimNum = 0; dimNum < currKPoint.length; dimNum++) {
              double newCoord = mappedPoint[dimNum] - shiftArray[dimNum];
              if (Math.abs(newCoord - Math.round(newCoord)) > 1E-2) { // Not symmetry preserving
                return Integer.MAX_VALUE;
              }
              mappedPointIntArray[dimNum] = (int) Math.round(newCoord);
            }
            getInnerPrimCell(mappedPointIntArray, kPointCanonical);
            mappedIndex = mappedPointIntArray[0] * kPointCanonical[1][1] * kPointCanonical[2][2] + mappedPointIntArray[1] * kPointCanonical[2][2] + mappedPointIntArray[2]; 
            if (mappedIndex < currIndex) {
              break;
            }
          }
          
          if (mappedIndex >= currIndex) {
            numDistinctKPoints++;
          }
        }
      }
    } 
    
    return numDistinctKPoints;
    
  }
  
  /**
   * A quick way to determine the rectangular coordinates of a lattice point relative to the location of 
   * corresponding lattice point in the SuperLattice.  Assumes superToDirect is in Hermite Normal Form.
   * 
   * @param primCellLocation
   * @param primCellIterator
   * @param superToDirect This should be in Hermite Normal Form
   */
  private static void getInnerPrimCell(int[] primCellLocation,  int[][] canonicalMatrix) {
    
    for (int coordNum = primCellLocation.length - 1; coordNum >= 0; coordNum--) {
      int innerCoord = primCellLocation[coordNum];
      for (int row = coordNum + 1; row < canonicalMatrix.length; row++) {
        
        int rowDimensionSize = canonicalMatrix[row][row];
        int shiftedCoord = primCellLocation[row];
        if (shiftedCoord < 0) {  // Account for how Java handles negatives in integer division
          innerCoord -= ((shiftedCoord - rowDimensionSize + 1) / rowDimensionSize) * canonicalMatrix[row][coordNum];
        } else {
          innerCoord -= (shiftedCoord / rowDimensionSize) * canonicalMatrix[row][coordNum];
        }
      }
      primCellLocation[coordNum] = innerCoord;
    }
    
    // Now take the modulo of the coordinates
    for (int coordNum = 0; coordNum < primCellLocation.length; coordNum++) {
      int innerCoord = primCellLocation[coordNum] % canonicalMatrix[coordNum][coordNum];
      primCellLocation[coordNum] = innerCoord < 0 ? innerCoord + canonicalMatrix[coordNum][coordNum] : innerCoord;
    }
    
  }
  
  // TODO replace this with a generic matrix multiply
  public static int[][] operateSuperToDirect(int[][] superToDirect, int[][] pointOperator, int[][] returnArray) {
    int dim = superToDirect.length;
    returnArray = (returnArray == null) ? new int[dim][dim] : returnArray;
    for (int rowNum = 0; rowNum < returnArray.length; rowNum++) {
      int[] row = superToDirect[rowNum];
      int[] oppedRow = returnArray[rowNum];
      for (int colNum = 0; colNum < row.length; colNum++) {
        int value = 0;
        for (int dimNum = 0; dimNum < row.length; dimNum++) {
          value += pointOperator[dimNum][colNum] * row[dimNum];
        }
        oppedRow[colNum] = value;
      }
    }
    return returnArray;
  }
  
  /**
   * Checks to see if a given superlattice preserves symmetry by applying symmetry operations to it and
   * comparing the Hermite normal form of the operated lattice.
   * 
   * @param canonicalSuperToDirect  This must be in Hermite normal form.  
   * @param pointOperators
   * @return
   */
  private static boolean isSymmetryPreserving(int[][] canonicalSuperToDirect, int[][][] pointOperators) {
    
    int[][] oppedArray = new int[canonicalSuperToDirect.length][canonicalSuperToDirect.length];
    for (int opNum = 0; opNum < pointOperators.length; opNum++) {
      int[][] pointOperator = pointOperators[opNum];
      operateSuperToDirect(canonicalSuperToDirect, pointOperator, oppedArray);
      toHermiteNormalForm(oppedArray);
      for (int row = 0; row < canonicalSuperToDirect.length; row++) {
        if (!Arrays.equals(canonicalSuperToDirect[row], oppedArray[row])) {
          return false;
        }
      }
    }
    return true;
    
  }
  
  /**
   * Simply checks to make sure lattices are symmetry preserving by applying symmetry operations
   * and comparing the Hermite normal form of the transformed lattice to the original.
   * 
   * @param size
   * @return
   */
  private int[][][] getSymPreservingLattices2D(int size) {
    
    int[] factors = MSMath.factor(size);
    int[][][] returnArray = new int[MSMath.arraySum(factors)][][];
    
    int[][] superToDirect = new int[2][2];
    
    int returnIndex = 0;
    for (int factorNum = 0; factorNum < factors.length; factorNum++) {
      int factor = factors[factorNum];
      
      // First we build the lattice in Hermite normal form.
      superToDirect[0][0] = factor;
      superToDirect[1][1] = size / factor;
      for (int diagElement = 0; diagElement < factor; diagElement++) {
        superToDirect[1][0] = diagElement;
        if (!isSymmetryPreserving(superToDirect, m_PointOperators2D)) {continue;}
        returnArray[returnIndex++] = ArrayUtils.copyArray(superToDirect);
      }
    }
    
    return ArrayUtils.truncateArray(returnArray, returnIndex);
    
  }
  
  /**
   * This reduces the matrix to lower-triangular Hermite Normal form.  The algorithm
   * is simple -- start from the last column and perform elementary row operations to 
   * get the necessary zeroes.  Then do this for the rest of the columns in reverse
   * order.  Once you have a lower triangular matrix, use modulo for the off-diagonal elements.
   */
  private static void toHermiteNormalForm(int[][] superToDirect) {
    
    //int[][] returnArray = ArrayUtils.copyArray(superToDirect);
    int[][] returnArray = superToDirect;
    if (returnArray.length == 0) {return;}
    
    // First we make it lower triangular using elementary row operations
    for (int colNum = returnArray[0].length - 1; colNum > 0; colNum--) {
      
      // Find the row with the minimum value in this column
      int minValue = Integer.MAX_VALUE;
      int minRowNum = -1;
      for (int rowNum = 0; rowNum <= colNum; rowNum++) {
        int rowValue = Math.abs(returnArray[rowNum][colNum]);
        if (rowValue < minValue && rowValue != 0) {
          minRowNum = rowNum;
          minValue = rowValue;
        }
      }
      int[] minRow = returnArray[minRowNum];
      
      // Swap the minRow so it is the new "last" row
      returnArray[minRowNum] = returnArray[colNum];
      returnArray[colNum] = minRow;
      
      // Make sure the diagonal element on the found row is positive
      if (minRow[colNum] < 0) {
        for (int prevColNum = 0; prevColNum <= colNum; prevColNum++) {
          minRow[prevColNum] *= -1;
        }
      }
      
      // Now keep getting the remainder between the minimum row and other rows 
      // and calling that new remainder the new minimum.  Do this until all remainders
      // are zero
      boolean allZeroes = true;
      do {
        allZeroes = true;
        for (int rowNum = 0; rowNum < colNum; rowNum++) {
          int[] row = returnArray[rowNum];
          int quotient = MSMath.divFloor(row[colNum], minValue); 
          int remainder = row[colNum] - (minValue * quotient);
          if (remainder != 0) {
            for (int prevColNum = 0; prevColNum <= colNum; prevColNum++) {
              row[prevColNum] -= minRow[prevColNum] * quotient;
            }
            returnArray[rowNum] = minRow;
            returnArray[colNum] = row;
            minRow = row;
            minValue = minRow[colNum];
            allZeroes = false;
            break;
          }
        }
      } while (!allZeroes);
      
      // Now we subtract multiples of the minimum row from all of the previous rows
      for (int rowNum = 0; rowNum < colNum; rowNum++) {
        int[] row = returnArray[rowNum];
        int quotient = MSMath.divFloor(row[colNum], minValue);
        for (int prevColNum = 0; prevColNum <= colNum; prevColNum++) {
          row[prevColNum] -= minRow[prevColNum] * quotient;
        }
      }
    }
    
    // We've made all of the diagonal elements positive except one...
    if (returnArray[0][0] < 0) {
      returnArray[0][0] *= -1;
    }
    
    // Now we take the modulo of the off-diagonal elements
    try {
      for (int rowNum = returnArray.length - 2; rowNum >= 0; rowNum--) {
        int[] row = returnArray[rowNum];
        int diagonal = row[rowNum];
        for (int nextRowNum = rowNum+1; nextRowNum < returnArray.length; nextRowNum++) {
          int[] nextRow = returnArray[nextRowNum];
          int quotient = MSMath.divFloor(nextRow[rowNum], diagonal);
          for (int prevCol = 0; prevCol <= rowNum; prevCol++) {
            nextRow[prevCol] -= row[prevCol] * quotient;
          }
        }
      }
    } catch (ArithmeticException e) {
      throw new RuntimeException("Error finding Hermite Normal Form:  Matrix is singular.", e);
    }
  }
  
  /**
   * Find the minimum periodic distance using Minkowski reduction.
   * 
   * @param superVectors
   * @return
   */
  private static double getMinDistance(double[][] superVectors, int numDimensions) {
    
	double minMagSq = Double.POSITIVE_INFINITY;
   
    boolean minimizing = true;
    double[] newVector = new double[superVectors[0].length];
    while (minimizing) {
      minimizing = false;
      for (int vecNum = 0; vecNum < numDimensions; vecNum++) {
        double[] vector = superVectors[vecNum];
        double magSq = MSMath.magnitudeSquared(vector);
        for (int increment = 1; increment < numDimensions; increment++) {
          int vecNum2 = (vecNum + increment) % numDimensions;
          double[] vector2 = superVectors[vecNum2];
          double innerProduct = MSMath.dotProduct(vector, vector2) / magSq;
          double shift = Math.round(innerProduct);
          double oldMagSq = MSMath.magnitudeSquared(vector2);
          minMagSq = Math.min(oldMagSq, minMagSq);
          if (shift == 0) {
        	  continue;
          }
          for (int dimNum = 0; dimNum < vector2.length; dimNum++) {
            newVector[dimNum] = vector2[dimNum] - vector[dimNum] * shift;
          }
          double newMagSq = MSMath.magnitudeSquared(newVector);
          if (newMagSq < oldMagSq) {
            minimizing = true;
            superVectors[vecNum2] = newVector;
            newVector = vector2;
            minMagSq = Math.min(newMagSq, minMagSq);
          } 
        }
      }
    }
   
    if (numDimensions == 3) {

      // An extra step that is necessary in three dimensions.
      int eps01 = (int) Math.signum(MSMath.dotProduct(superVectors[0], superVectors[1]));
      int eps02 = (int) Math.signum(MSMath.dotProduct(superVectors[0], superVectors[2]));
      int eps12 = (int) Math.signum(MSMath.dotProduct(superVectors[1], superVectors[2]));

      if (eps01 * eps02 * eps12 == -1) {
        for (int dimNum = 0; dimNum < superVectors[0].length; dimNum++) {
          superVectors[0][dimNum] -= (superVectors[1][dimNum] * eps01 + superVectors[2][dimNum] * eps02);
        }
        minMagSq = Math.min(MSMath.magnitudeSquared(superVectors[0]), minMagSq);
      }
     
    }
   
    return Math.sqrt(minMagSq);
  }
  
  /**
   * These are the possible layer-by-layer shifts for different lattice types.
   */
  private static double[][] m_Other2DShifts = new double[][] {
      {0, 0},
      {0, 0.5},
      {0.5, 0},
      {0.5, 0.5},
  };
  
  private static double[][] m_Hexagonal2DShifts = new double[][] {
    {0, 0},
    {1.0/3, 1.0/3},
    {2.0/3, 2.0/3},
    {1.0/3, 2.0/3},
    {2.0/3, 1.0/3},
    {0, 1.0/3},
    {1.0/3, 0},
    {0, 2.0/3},
    {2.0/3, 0},
  };
  
  /**
   * This is just a structure for holding data about the best lattice we've found so far.
   * 
   * @author Tim Mueller
   *
   */
  public class KPointLattice {
    
    private int[][] m_SuperToDirect;
    private int m_NumDistinctKPoints;
    private double m_MinPeriodicDistance;
    private double[] m_Shift;
    
    /**
     * 
     * @param superToDirect You can get the total number of k-points from the determinant of this.
     * @param shift The shift is given in the coordinates of the k-point lattice basis.
     * @param numDistinctKPoints
     * @param minDistance
     */
    public KPointLattice(int[][] superToDirect, double[] shift, int numDistinctKPoints, double minDistance) {
      m_SuperToDirect = (superToDirect == null) ? null : ArrayUtils.copyArray(superToDirect);
      m_Shift = (shift == null) ? null : ArrayUtils.copyArray(shift);
      m_NumDistinctKPoints = numDistinctKPoints;
      m_MinPeriodicDistance = minDistance;
    }
    
    public int[][] getSuperToDirect() {
      return m_SuperToDirect == null ? null : ArrayUtils.copyArray(m_SuperToDirect);
    }
    
    public double[] getShift() {
      return m_Shift == null ? null : ArrayUtils.copyArray(m_Shift);
    }
    
    public double getMinPeriodicDistance() {
      return m_MinPeriodicDistance;
    }
    
    public int getNumDistinctKPoints() {
      return m_NumDistinctKPoints;
    }
    

    public int numTotalKPoints() {
      return (m_SuperToDirect == null) ? -1 : MSMath.determinant(m_SuperToDirect);
    }
    
    // These are in the direct coords for the k-point lattice.
    // The k-point lattice vectors are determined by taking the inverse of the real-space superlattice vectors as 
    // given by superToDirect..
    public double[][] getKPointCoordinates() {
      if (m_SuperToDirect == null) {return null;}
      
      double[][] returnArray = new double[this.numTotalKPoints()][];
      int[][] kPointCanonical = MSMath.transpose(m_SuperToDirect);
      toHermiteNormalForm(kPointCanonical);
      int returnIndex = 0;
      for (int i = 0; i < kPointCanonical[0][0]; i++) {
        for (int j = 0; j < kPointCanonical[1][1]; j++) {
          for (int k = 0; k < kPointCanonical[2][2]; k++) {
            returnArray[returnIndex] = new double[] {i, j, k};
          }
        }
      }
      
      return returnArray;
      
    }
    
    public int[] getKPointWeights() {
      if (m_SuperToDirect == null) {return null;}
      
      // Apply the operation in k-space
      double[][] recipSTD = MSMath.simpleLowerTriangularInverse(m_SuperToDirect);
      for (int opNum = 0; opNum < m_KPointOperators.length; opNum++) {
        int[][] opInverse = MSMath.matrixMultiply(m_SuperToDirect, m_PointOperators3D[opNum]);
        m_KPointOperators[opNum] = MSMath.round(MSMath.matrixMultiply(opInverse, recipSTD));
      }
      
      int[][] kPointCanonical = MSMath.transpose(m_SuperToDirect);
      toHermiteNormalForm(kPointCanonical);

      double[] shiftedPoint = new double[3];
      int[] mappedPointIntArray = new int[3];
      
      int[] weights = new int[this.numTotalKPoints()];
      
      for (int i = 0; i < kPointCanonical[0][0]; i++) {
        for (int j = 0; j < kPointCanonical[1][1]; j++) {
          for (int k = 0; k < kPointCanonical[2][2]; k++) {
            int[] currKPoint = new int[] {i, j, k};
            
            int minMappedIndex = Integer.MAX_VALUE;
            for (int opNum = 0; opNum < m_KPointOperators.length; opNum++) {
              for (int dimNum = 0; dimNum < currKPoint.length; dimNum++) {
                shiftedPoint[dimNum] = currKPoint[dimNum] + m_Shift[dimNum];
              }
              
              // Putting the vector last is faster than taking the transpose of the op.
              double[] mappedPoint = MSMath.matrixTimesVector(m_KPointOperators[opNum], shiftedPoint); 
              for (int dimNum = 0; dimNum < currKPoint.length; dimNum++) {
                double newCoord = mappedPoint[dimNum] - m_Shift[dimNum];
                if (Math.abs(newCoord - Math.round(newCoord)) > 1E-2) { // Not symmetry preserving
                  throw new RuntimeException("Invalid KPointLattice was created.");
                }
                mappedPointIntArray[dimNum] = (int) Math.round(newCoord);
              }
              getInnerPrimCell(mappedPointIntArray, kPointCanonical);
              int mappedIndex = mappedPointIntArray[0] * kPointCanonical[1][1] * kPointCanonical[2][2] + mappedPointIntArray[1] * kPointCanonical[2][2] + mappedPointIntArray[2]; 
              minMappedIndex = Math.min(minMappedIndex, mappedIndex);
            }
            weights[minMappedIndex]++;
          }
        }
      } 
      
      return weights;
    }
  }

}
