package generators;
/*
 * Created on Jun 17, 2013
 *
 */

//import generators.ParetoFrontierGenerator.ParetoFrontier;
import matsci.Species;
import matsci.io.app.log.Status;
import matsci.io.vasp.KPOINTS;
import matsci.io.vasp.POSCAR;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;

//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//import org.json.JSONTokener;

import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.CartesianBasis;
import matsci.location.symmetry.operations.SpaceGroup;
import matsci.location.symmetry.operations.SymmetryOperation;
import matsci.structure.BravaisLattice;
import matsci.structure.Structure;
import matsci.structure.Structure.Site;
import matsci.structure.StructureBuilder;
import matsci.structure.reciprocal.KPointLattice;
import matsci.structure.superstructure.SuperLattice;
import matsci.structure.superstructure.SuperStructure;

/**
 * This is the main class the represents a library of k-Point lattices (and lattice generators)
 * 
 * @author Tim Mueller
 *
 */
public class KPointLibrary {
  
  private IKPointFactory[][] m_GammaFactories = new IKPointFactory[4][74];
  private IKPointFactory[][] m_ShiftedFactories = new IKPointFactory[4][74];
  
  public static enum INCLUDE_GAMMA {TRUE, FALSE, AUTO};
  
  /**
   * The symmetries to be used when finding the symmetrically irreducible k-points.
   * ALL: structural symmetries + time-reversal symmetry.
   * STRUCTURE_ONLY: structural symmetries (if there is no inversion in the direct space, there is 
   * no inversion in the reciprocal space.).
   * TIME_REVERSAL_ONLY: only inversion.
   * NONE: no symmetries are used to reduce the number of k-points. 
   */
  public static enum SYMMETRY_TYPE {ALL, STRUCTURE_ONLY, TIME_REVERSAL_ONLY, NONE};
  
  public KPointLibrary() {}

  /**
   * 
   * @param arithmeticGroupIndex The arithmetic group number (from 1 to 73) of the underlying 
   *                             symmorphic space group of a general space group.
   * @param numDimensions The number of periodic dimensions.
   * @param includeGamma True if the lattices are gamma-centered, false if they may be shifted.
   * @param factory The factory that generates k-point lattices.
   */
  public void setFactory(int arithmeticGroupIndex, int numDimensions, boolean includeGamma, 
          IKPointFactory factory) {
    if (includeGamma) {
      m_GammaFactories[numDimensions][arithmeticGroupIndex] = factory;
    } else {
      m_ShiftedFactories[numDimensions][arithmeticGroupIndex] = factory;
    }
  }
  
  /**
   * This method automatically generates and stores a KPointLatticeCollection from a 
   * set of structures. 
   * 
   * @param structures  The structures to be used when generating the Pareto frontiers
   * for the lattice collection.  They should all belong to the same symmorphic space group.
   * @param maxNumKPoints The maximum number of total k-points to be considered when
   * generating the Pareto frontiers.
   */
  /*
  public void generateCollection(Structure[] structures, int maxNumKPoints) {
    
    long startTime = System.currentTimeMillis();
    // TODO check to make sure all structures are of the same arithmetic crystal class
    //KPointLibrary library;
    ParetoFrontierGenerator.ParetoFrontier[] gammaFrontiers = new ParetoFrontierGenerator.ParetoFrontier[structures.length];
    ParetoFrontierGenerator.ParetoFrontier[] shiftedFrontiers = new ParetoFrontierGenerator.ParetoFrontier[structures.length];
    
    SpaceGroup kPointGroup = structures[0].getDefiningSpaceGroup().getSymmorphicGroup(true);
    int numDimensions = structures[0].getDefiningLattice().numPeriodicVectors();
    
    PrintStream outStream = System.out;
    Writer writer = new OutputStreamWriter(outStream);
    
    Status.setLogLevelDetail();
    for (int structNum = 0; structNum < structures.length; structNum++) {
      Status.detail("Structure: " + structNum);
      Structure structure = structures[structNum];
      SpaceGroup spaceGroup = structure.getDefiningSpaceGroup();
      ParetoFrontierGenerator generator = new ParetoFrontierGenerator(spaceGroup, maxNumKPoints);
      gammaFrontiers[structNum] = generator.getGammaFrontier();
      shiftedFrontiers[structNum] = generator.getShiftedFrontier();
      
      ParetoFrontier gammaFrontier = gammaFrontiers[structNum];
      ParetoFrontier shiftedFrontier = shiftedFrontiers[structNum];
      for (int entryNum = 0; entryNum < gammaFrontier.numEntries(); entryNum++) {
        SuperLattice realLattice = gammaFrontier.getRealSuperLattice(entryNum);
        Structure kPointStructure = realLattice.getRepresentativeStructure(Species.carbon);
        //kPointStructure = kPointStructure.scaleLatticeConstant(10);
        kPointStructure = kPointStructure.getCompactStructure();
        kPointStructure = kPointStructure.getConventionalPrimStructure();
        BravaisLattice targetLattice = realLattice.getPrimLattice().getConventionalPrimLattice().getInverseLattice();
        POSCAR outfile = new POSCAR(kPointStructure);
        String rootDir = "kpoints/lattices/gamma/";
        outfile.writeFile(rootDir + structNum + "." + realLattice.numPrimCells() + ".vasp");
        //outfile.writeVICSOutFile(rootDir + structNum + "." + realLattice.numPrimCells() + ".out");
        
        SuperLattice reciprocalLattice = gammaFrontier.getInverseLattice(entryNum);
        kPointStructure = reciprocalLattice.getRepresentativeStructure(Species.carbon);
        //kPointStructure = kPointStructure.scaleLatticeConstant(10);
        //kPointStructure = kPointStructure.getCompactStructure();
        //kPointStructure = kPointStructure.getConventionalStructure();
        int[][] superToDirect = kPointStructure.getDefiningLattice().getSuperToDirect(targetLattice);
        kPointStructure = new SuperStructure(kPointStructure, superToDirect);
        outfile = new POSCAR(kPointStructure);
        rootDir = "kpoints/reciprocal/gamma/";
        outfile.writeFile(rootDir + structNum + "." + reciprocalLattice.numPrimCells() + ".vasp");
        //outfile.writeVICSOutFile(rootDir + structNum + "." + reciprocalLattice.numPrimCells() + ".out");

        KPointLattice kPointLattice = gammaFrontier.getKPointLattice(entryNum);
        KPOINTS kPointFile = new KPOINTS(kPointLattice, structure.getDefiningLattice().getInverseLattice().getLatticeBasis());
        
        String outline = "gamma " + gammaFrontier.getMinPeriodicDistance(entryNum) + ", " + gammaFrontier.numDistinctKPoints(entryNum) + ", " + gammaFrontier.numTotalKPoints(entryNum);
        try {
          writer.write(outline + "\n");
        } catch (IOException e) {
          e.printStackTrace();
        }
        kPointFile.write(writer);
        
      }
      
      for (int entryNum = 0; entryNum < shiftedFrontier.numEntries(); entryNum++) {
        
        SuperLattice realLattice = shiftedFrontier.getRealSuperLattice(entryNum);
        Structure kPointStructure = realLattice.getRepresentativeStructure(Species.carbon);
        //kPointStructure = kPointStructure.scaleLatticeConstant(10);
        kPointStructure = kPointStructure.getCompactStructure();
        kPointStructure = kPointStructure.getConventionalPrimStructure();
        BravaisLattice targetLattice = realLattice.getPrimLattice().getConventionalPrimLattice().getInverseLattice();
        POSCAR outfile = new POSCAR(kPointStructure);
        String rootDir = "kpoints/lattices/shifted/";
        outfile.writeFile(rootDir + structNum + "." + realLattice.numPrimCells() + ".vasp");
        //outfile.writeVICSOutFile(rootDir + structNum + "." + realLattice.numPrimCells() + ".out");
        
        SuperLattice reciprocalLattice = shiftedFrontier.getInverseLattice(entryNum);
        kPointStructure = reciprocalLattice.getRepresentativeStructure(Species.carbon);
        //kPointStructure = kPointStructure.scaleLatticeConstant(10);
        //kPointStructure = kPointStructure.getCompactStructure();
        //kPointStructure = kPointStructure.getConventionalStructure();
        int[][] superToDirect = kPointStructure.getDefiningLattice().getSuperToDirect(targetLattice);
        kPointStructure = new SuperStructure(kPointStructure, superToDirect);
        outfile = new POSCAR(kPointStructure);
        rootDir = "kpoints/reciprocal/shifted/";
        outfile.writeFile(rootDir + structNum + "." + reciprocalLattice.numPrimCells() + ".vasp");
        //outfile.writeVICSOutFile(rootDir + structNum + "." + reciprocalLattice.numPrimCells() + ".out");

        KPointLattice kPointLattice = shiftedFrontier.getKPointLattice(entryNum);
        KPOINTS kPointFile = new KPOINTS(kPointLattice, structure.getDefiningLattice().getInverseLattice().getLatticeBasis());
        
        String outline = "shifted " + shiftedFrontier.getMinPeriodicDistance(entryNum) + ", " + shiftedFrontier.numDistinctKPoints(entryNum) + ", " + shiftedFrontier.numTotalKPoints(entryNum);
        try {
          writer.write(outline + "\n");
        } catch (IOException e) {
          e.printStackTrace();
        }
        kPointFile.write(writer);
        
      }
      
      // Don't do this when the output stream is System.out
      if (outStream != System.out){ 
        try {
          writer.flush(); 
          writer.close();
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      }
      
      System.out.println(generator.getGammaFrontier());
      System.out.println(generator.getShiftedFrontier());
      //System.exit(0);
    }
    
    KPointLatticeCollection gammaCollection = (KPointLatticeCollection) this.getFactory(numDimensions, kPointGroup, true);
    KPointLatticeCollection shiftedCollection = (KPointLatticeCollection) this.getFactory(numDimensions, kPointGroup, false);

    Status.basic("Adding Pareto fontiers...");
    gammaCollection.addParetoFrontiers(gammaFrontiers);
    shiftedCollection.addParetoFrontiers(shiftedFrontiers);

    //save collection
    //try {
	//	gammaCollection.writeFile(numDimensions);
	//	shiftedCollection.writeFile(numDimensions);
	//} catch (IOException e) {
	//	e.printStackTrace();
	//} catch (JSONException e) {
	//	e.printStackTrace();
	//}
    
    Status.detail("Took " + (System.currentTimeMillis() - startTime) + " milliseconds to generate all Pareto Frontiers");
    
  }
  */
  
  /*
  public void generateCollection2(Structure[] structures, int maxNumKPoints, String outLoc) {
       
    long startTime = System.currentTimeMillis();
    // TODO check to make sure all structures are of the same arithmetic crystal class
    //KPointLibrary library;
    ParetoFrontierGenerator.ParetoFrontier[] gammaFrontiers = new ParetoFrontierGenerator.ParetoFrontier[structures.length];
    ParetoFrontierGenerator.ParetoFrontier[] shiftedFrontiers = new ParetoFrontierGenerator.ParetoFrontier[structures.length];
   
    SpaceGroup kPointGroup = structures[0].getDefiningSpaceGroup().getSymmorphicGroup(true);
    int numDimensions = structures[0].getDefiningLattice().numPeriodicVectors();
   
    Writer writer = new OutputStreamWriter(System.out);
   
    Status.setLogLevelDetail();

    //Status.setLogLevelQuiet();
    for (int structNum = 0; structNum < structures.length; structNum++) {
      Status.detail("Structure: " + structNum);
      Structure structure = structures[structNum];
      SpaceGroup spaceGroup = structure.getDefiningSpaceGroup();
      //ParetoFrontierGenerator generator = new ParetoFrontierGenerator(spaceGroup, maxNumKPoints);
      //ParetoFrontierGenerator generator = new ParetoFrontierGenerator(spaceGroup, maxNumKPoints, true);
      ParetoFrontierGenerator generator = new ParetoFrontierGenerator(spaceGroup, maxNumKPoints, false, false);
      gammaFrontiers[structNum] = generator.getGammaFrontier();
      shiftedFrontiers[structNum] = generator.getShiftedFrontier();
      
      ParetoFrontier gammaFrontier = gammaFrontiers[structNum];
      ParetoFrontier shiftedFrontier = shiftedFrontiers[structNum];
      for (int entryNum = 0; entryNum < gammaFrontier.numEntries(); entryNum++) {
        SuperLattice realLattice = gammaFrontier.getRealSuperLattice(entryNum);
        Structure kPointStructure = realLattice.getRepresentativeStructure(Species.carbon);
        //kPointStructure = kPointStructure.scaleLatticeConstant(10);
        kPointStructure = kPointStructure.getCompactStructure();
        POSCAR outfile = new POSCAR(kPointStructure);
        String rootDir = outLoc;
        outfile.writeFile(rootDir + "/" + structNum + "." + realLattice.numPrimCells() + ".gamma.real.vasp");
       // outfile.writeVICSOutFile(rootDir + structNum + "." + realLattice.numPrimCells() + ".out");

        SuperLattice reciprocalLattice = gammaFrontier.getInverseLattice(entryNum);
        kPointStructure = reciprocalLattice.getRepresentativeStructure(Species.carbon);
        kPointStructure = kPointStructure.scaleLatticeConstant(10);
        kPointStructure = kPointStructure.getCompactStructure();
        outfile = new POSCAR(kPointStructure);
        rootDir = outLoc;
        outfile.writeFile(rootDir + "/" + structNum + "." + reciprocalLattice.numPrimCells() + "gamma.reciprocal.vasp");
       // outfile.writeVICSOutFile(rootDir + structNum + "." + reciprocalLattice.numPrimCells() + ".out");

        KPointLattice kPointLattice = gammaFrontier.getKPointLattice(entryNum);
        KPOINTS kPointFile = new KPOINTS(kPointLattice, structure.getDefiningLattice().getInverseLattice().getLatticeBasis());
        kPointFile.writeFile(outLoc + "/KPOINTS.structNum." + kPointLattice.numDistinctKPoints() + ".pareto.gamma");;
       
      }
     
      for (int entryNum = 0; entryNum < shiftedFrontier.numEntries(); entryNum++) {
       
        SuperLattice realLattice = shiftedFrontier.getRealSuperLattice(entryNum);
        Structure kPointStructure = realLattice.getRepresentativeStructure(Species.carbon);
        //kPointStructure = kPointStructure.scaleLatticeConstant(10);
        kPointStructure = kPointStructure.getCompactStructure();
        POSCAR outfile = new POSCAR(kPointStructure);
        String rootDir = outLoc;
        outfile.writeFile(rootDir + "/" + structNum + "." + realLattice.numPrimCells() + "shifted.real.vasp");
       // outfile.writeVICSOutFile(rootDir + structNum + "." + realLattice.numPrimCells() + ".out");

        SuperLattice reciprocalLattice = shiftedFrontier.getInverseLattice(entryNum);
        kPointStructure = reciprocalLattice.getRepresentativeStructure(Species.carbon);
        kPointStructure = kPointStructure.scaleLatticeConstant(10);
        kPointStructure = kPointStructure.getCompactStructure();
        outfile = new POSCAR(kPointStructure);
        rootDir = outLoc;
        outfile.writeFile(rootDir + "/" + structNum + "." + reciprocalLattice.numPrimCells() + "shifted.reciprocal.vasp");
      //  outfile.writeVICSOutFile(rootDir + structNum + "." + reciprocalLattice.numPrimCells() + ".out");

        KPointLattice kPointLattice = shiftedFrontier.getKPointLattice(entryNum);
        KPOINTS kPointFile = new KPOINTS(kPointLattice, structure.getDefiningLattice().getInverseLattice().getLatticeBasis());
        kPointFile.writeFile(outLoc + "/KPOINTS.structNum." + kPointLattice.numDistinctKPoints() + ".pareto.shifted");
       
      }
     
    //try {
      //  writer.flush();
      //  writer.close();
      //} catch (IOException e) {
      //  throw new RuntimeException(e);
      //}
     
      System.out.println(generator.getGammaFrontier());
      System.out.println(generator.getShiftedFrontier());
      //System.exit(0);
    }
    
   
    KPointLatticeCollection gammaCollection = (KPointLatticeCollection) this.getFactory(numDimensions, kPointGroup, true);
    KPointLatticeCollection shiftedCollection = (KPointLatticeCollection) this.getFactory(numDimensions, kPointGroup, false);

    Status.basic("Adding Pareto fontiers...");
    gammaCollection.addParetoFrontiers(gammaFrontiers);
    shiftedCollection.addParetoFrontiers(shiftedFrontiers);
   
    KPointLibrary lib = new KPointLibrary();
    lib.setFactory(72, 3, true, gammaCollection);
    lib.setFactory(72, 3, false, shiftedCollection);
   
    //KPointLattice kLat = lib.getKPointLattice(structures[0], false, 35);
    //KPOINTS kpoints = new KPOINTS(kLat);
    //kpoints.writeFile(outLoc + "/KPOINTS.35.pareto-KPointLattice.shifted");
   
    //save collection
    //try {
    //    gammaCollection.writeFileTo(numDimensions, maxNumKPoints, outLoc);
    //    shiftedCollection.writeFileTo(numDimensions, maxNumKPoints,outLoc);
    //} catch (IOException e) {
    //    e.printStackTrace();
    //} catch (JSONException e) {
    //    e.printStackTrace();
    //}
    gammaCollection.writeFile(outLoc + kPointGroup.getArithmeticCrystalClassNumber(3) +  ".gammaFrontier");
    shiftedCollection.writeFile(outLoc + kPointGroup.getArithmeticCrystalClassNumber(3) +  ".shiftedFrontier");
    
    Status.detail("Took " + (System.currentTimeMillis() - startTime) + " milliseconds to generate all Pareto Frontiers");
   
  } 
  */
  
  /**
   * Prints structures to file. Root directory for structures is passed inside as an arg.
   * @param structs
   * @param numPeriodicDimensions
   * @param classNum
   * @throws IOException
   * @throws JSONException
   */
  /*
  public void saveStructures(Structure[] structs, int numPeriodicDimensions, int classNum, String rootdir) throws IOException, JSONException {
	  
	  double[] tailCoord, headCoord, siteCoord;
	  BravaisLattice lattice;
	  Vector[] vectors;
	  JSONArray heads, tails, structTails, structHeads, sitePos, elements, isotopes, oxidations, structSites, structElems, structIsos, structOxs;
	  JSONObject structures;
		
	  structures = new JSONObject();
	  String fileName = rootdir + "structs_" + classNum;
	  
	  File file = new File(fileName);
	  FileWriter output = new FileWriter(file, false);
	  structTails = new JSONArray();
	  structHeads = new JSONArray();
	  structSites = new JSONArray();
	  structElems = new JSONArray();
	  structIsos = new JSONArray();
	  structOxs = new JSONArray();
	  
	  Site[] sites;
	  
	  for (Structure structure : structs) {
		  
		  tails = new JSONArray();
		  heads = new JSONArray();
		  sitePos = new JSONArray();
		  elements = new JSONArray();
		  isotopes = new JSONArray();
		  oxidations = new JSONArray();
		  
		  sites = structure.getDefiningSites();
		  lattice = structure.getDefiningLattice();
		  vectors = lattice.getCellVectors();
		  
		  for (Site site : sites) {
			  
			  siteCoord = site.getCoords().getCartesianArray();
			  sitePos.put(new JSONArray().put(siteCoord[0]).put(siteCoord[1]).put(siteCoord[2]));
			  
			  elements.put(site.getSpecies().getElementSymbol());
			  isotopes.put(site.getSpecies().getIsotope());
			  oxidations.put(site.getSpecies().getOxidationState());
		  }
		  
		  for (Vector vector : vectors) {
			  
			  tailCoord = vector.getTail().getCartesianArray();
			  headCoord = vector.getHead().getCartesianArray();
			  tails.put(new JSONArray().put(tailCoord[0]).put(tailCoord[1]).put(tailCoord[2]));
			  heads.put(new JSONArray().put((Double) headCoord[0]).put(headCoord[1]).put(headCoord[2]));
		  }
		  structTails.put(tails);
		  structHeads.put(heads);
		  structSites.put(sitePos);
		  structElems.put(elements);
		  structIsos.put(isotopes);
		  structOxs.put(oxidations);
		  
	  }
	  structures.append("numStructs", structs.length);
	  structures.append("tails", structTails);
	  structures.append("heads", structHeads);
	  structures.append("sites", structSites);
	  structures.append("elements", structElems);
	  structures.append("isotopes", structIsos);
	  structures.append("oxidations", structOxs);
	  
	  output.write(structures.toString(3));
	  output.close();
  }
  */
 
  /**
   * Loads a structure file according to an index. 
   * @param index
   * @param numDimensions
   * @param arithmeticClass
   * @param rootdir
   * @return
   * @throws FileNotFoundException
   * @throws JSONException
   */
  /*
  public Structure loadStructure(int index, int numDimensions, int arithmeticClass, String rootdir) throws FileNotFoundException, JSONException {
	  
	  double [] tailArray, headArray, siteArray;
	  Vector[] vectors;
	  
	  JSONObject in;
	  JSONArray tails, heads, sites, elements, isotopes, oxidations;
	  JSONTokener stream;
	  Species species;
	  Structure data;
	  StructureBuilder builder;
	  
	  FileReader file = new FileReader(rootdir + "structs_" + arithmeticClass);
	  stream = new JSONTokener(file);
	  tailArray = new double[3];
	  headArray = new double[3];
	  siteArray = new double[3];
	  
	  in = new JSONObject(stream);
	  tails = in.getJSONArray("tails");
	  heads = in.getJSONArray("heads");
	  sites = in.getJSONArray("sites");
	  elements = in.getJSONArray("elements");
	  isotopes = in.getJSONArray("isotopes");
	  oxidations = in.getJSONArray("oxidations");
	  
	  vectors = new Vector[tails.getJSONArray(0).getJSONArray(index).length()];
	  for (int i=0; i<tails.getJSONArray(0).getJSONArray(index).length(); i++) {
	  
		  tailArray[0] = tails.getJSONArray(0).getJSONArray(index).getJSONArray(i).getDouble(0);
		  tailArray[1] = tails.getJSONArray(0).getJSONArray(index).getJSONArray(i).getDouble(1);
		  tailArray[2] = tails.getJSONArray(0).getJSONArray(index).getJSONArray(i).getDouble(2);

		  headArray[0] = heads.getJSONArray(0).getJSONArray(index).getJSONArray(i).getDouble(0);
		  headArray[1] = heads.getJSONArray(0).getJSONArray(index).getJSONArray(i).getDouble(1);
		  headArray[2] = heads.getJSONArray(0).getJSONArray(index).getJSONArray(i).getDouble(2);
		  
		  vectors[i] = new Vector(new Coordinates(tailArray, CartesianBasis.getInstance()), new Coordinates(headArray, CartesianBasis.getInstance()));
	  }
	  builder = new StructureBuilder(new BravaisLattice(vectors));
	  for (int j=0; j<elements.getJSONArray(0).getJSONArray(index).length(); j++) {
		  
		  
		  siteArray[0] = sites.getJSONArray(0).getJSONArray(index).getJSONArray(j).getDouble(0);
		  siteArray[1] = sites.getJSONArray(0).getJSONArray(index).getJSONArray(j).getDouble(1);
		  siteArray[2] = sites.getJSONArray(0).getJSONArray(index).getJSONArray(j).getDouble(2);		  
		  
		  species = Species.get(elements.getJSONArray(0).getJSONArray(index).getString(j),
				  isotopes.getJSONArray(0).getJSONArray(index).getInt(j), oxidations.getJSONArray(0).getJSONArray(index).getDouble(j));
		  builder.addSite(new Coordinates(siteArray, CartesianBasis.getInstance()), species);
	  }
	  builder.setVectorPeriodicity(new boolean[] {true, true, true});
	  data = new Structure(builder);
	  Status.basic("Structure loaded, " + data.numDefiningSites() + " sites detected.");
	  POSCAR poscar = new POSCAR(builder);
	  return data;
  }
  */
  
  /**
   * Writes a Pareto frontier for a given structure file
   * @param index
   * @param struct
   * @param maxNumKPoints
   */
  /*public void generateParetoFrontier(int index, Structure struct, int maxNumKPoints) {
	    	    
	    Status.basic("Generating Pareto fontier...");
	    
    	SpaceGroup spaceGroup = struct.getDefiningSpaceGroup();
    	ParetoFrontierGenerator generator = new ParetoFrontierGenerator(spaceGroup, maxNumKPoints);
    	Status.basic("Generated frontiers, saving...");
	  //save frontiers
	    try {
	    	generator.writeFile(index, 3, "database/3/ParetoFrontiers/" + spaceGroup.getArithmeticCrystalClassNumber(3) + "/");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	  }
	  */
  /**
   *
   * @param numDimensions  The number of periodic dimensions in the lattice.
   * @param symmorphicSpaceGroup  The symmorphic space group for which we need a generator.
   * @param includeGamma True if the lattice is gamma-centered, false if it is not.
   * @return A k-point factory.
   */
  public IKPointFactory getFactory(int numDimensions, SpaceGroup symmorphicSpaceGroup, boolean includeGamma) {
    
    int arithmeticGroupNum = symmorphicSpaceGroup.getArithmeticCrystalClassNumber(numDimensions);
    
    if (includeGamma) {
      IKPointFactory factory = m_GammaFactories[numDimensions][arithmeticGroupNum];
      if (factory == null) {
        factory = new KPointLatticeCollection(symmorphicSpaceGroup, includeGamma);
        m_GammaFactories[numDimensions][arithmeticGroupNum] = factory;
      }
      return factory;
    }

    IKPointFactory factory = m_ShiftedFactories[numDimensions][arithmeticGroupNum];
    if (factory == null) {
      factory = new KPointLatticeCollection(symmorphicSpaceGroup, includeGamma);
      m_ShiftedFactories[numDimensions][arithmeticGroupNum] = factory;
    }
    return factory;
  }
  
  public KPointLattice getKPointLattice(Structure structure, INCLUDE_GAMMA includeGamma, double minPeriodicDistance, int minTotalKPoints, SYMMETRY_TYPE symmetryType) {
	  return this.getKPointLattice(structure.getDefiningSpaceGroup(), includeGamma, minPeriodicDistance, minTotalKPoints, symmetryType);
  }
  
  public KPointLattice getKPointLattice(SpaceGroup space, INCLUDE_GAMMA includeGamma, double minPeriodicDistance, int minTotalKPoints, SYMMETRY_TYPE symmetryType) {

	    if (includeGamma == INCLUDE_GAMMA.TRUE) {
	      return this.getKPointLattice(space, true, minPeriodicDistance, minTotalKPoints, Integer.MAX_VALUE, symmetryType);
	    } else if (includeGamma == INCLUDE_GAMMA.FALSE) {
	      return this.getKPointLattice(space, false, minPeriodicDistance, minTotalKPoints, Integer.MAX_VALUE, symmetryType);
	    } 

	    KPointLattice shiftedLattice = this.getKPointLattice(space, false, minPeriodicDistance, minTotalKPoints, Integer.MAX_VALUE, symmetryType);
	    int gammaMaxDistinctPoints = (shiftedLattice == null) ? Integer.MAX_VALUE : shiftedLattice.numDistinctKPoints();
	    KPointLattice gammaLattice = this.getKPointLattice(space, true, minPeriodicDistance, minTotalKPoints, gammaMaxDistinctPoints, symmetryType);
	    
	    if (gammaLattice == null) {
	      return shiftedLattice;
	    } else if (shiftedLattice == null) {
		  return gammaLattice;
	    } else if (gammaLattice.numDistinctKPoints() < shiftedLattice.numDistinctKPoints()) {
	      return gammaLattice;
	    } else if (shiftedLattice.numDistinctKPoints() < gammaLattice.numDistinctKPoints()) {
	      return shiftedLattice; // This is necessary b/c of ambiguity regarding the actual number of distinct k-points when using scaling
	    } else if (shiftedLattice.getRealSpaceMinPeriodicDistance() - CartesianBasis.getPrecision() > gammaLattice.getRealSpaceMinPeriodicDistance()) {
	      return shiftedLattice;
	    } else if (gammaLattice.getRealSpaceMinPeriodicDistance() - CartesianBasis.getPrecision() > shiftedLattice.getRealSpaceMinPeriodicDistance()) {
	      return gammaLattice;
	    } else if (gammaLattice.numTotalKPoints() > shiftedLattice.numTotalKPoints()) {
	      return gammaLattice;
	    }
	    return shiftedLattice;
	    
	  }
  
  /**
   * 
   * @param structure The structure for which we are generating the k-point lattice.
   * @param includeGamma True if the lattice should be gamma-centered, false if it is not.
   * @param minPeriodicDistance  Every k-point grid corresponds to a real-space supercell.
   * The min periodic distance is the shortest distance between two lattice points of that 
   * supercell.
   * @return  A reciprocal-space superlattice containing all of the k-points.  The
   * returned lattice will be the reciprocal lattice of the defining lattice for the given
   * structure, and the underlying primitive lattice (accessed by getPrimLattice()) will 
   * return the k-point lattice.
   */
/*  protected KPointLattice getKPointLattice(Structure structure, boolean includeGamma, double minPeriodicDistance, int minTotalKPoints, int maxDistinctKPoints, SYMMETRY_TYPE symmetryType) {
    long st = System.currentTimeMillis();
    long st2 = System.currentTimeMillis();
    SpaceGroup arithmeticGroup = this.getArithmeticGroup(structure, symmetryType);
    int numDimensions = structure.getDefiningLattice().numPeriodicVectors();
    Status.detail("getArithGroup @ getKPointLattice in KPointLibrary: " + (System.currentTimeMillis() - st2) + " ms.");
    // We use the centrosymmetric factories even if time-reversal symmetry is disabled
    // unless all symmetry is disabled
    long st3 = System.currentTimeMillis();
    int groupNumber = (symmetryType == SYMMETRY_TYPE.STRUCTURE_ONLY) ? 
        arithmeticGroup.getSymmorphicGroup(true).getArithmeticCrystalClassNumber(numDimensions) :
        arithmeticGroup.getArithmeticCrystalClassNumber(numDimensions);
    Status.detail("getGroupNumber: " + (System.currentTimeMillis() - st3) + " ms.");
    IKPointFactory[][] factoryArray = includeGamma ? m_GammaFactories : m_ShiftedFactories;
    Status.detail("getKPointLattice in KPointLattice before return: " + (System.currentTimeMillis() - st) + " ms.");
    return factoryArray[numDimensions][groupNumber].getKPointLattice(arithmeticGroup, minPeriodicDistance, minTotalKPoints, Integer.MAX_VALUE);
    
    /*BravaisLattice reciprocalLattice = structure.getDefiningLattice().getInverseLattice();
    reciprocalLattice = reciprocalLattice.translateTo(kPointLattice.getOrigin());
    return kPointLattice.getPrimLattice().getSuperLattice(reciprocalLattice);*/
  /*  
  }*/
  
  
  protected KPointLattice getKPointLattice(Structure structure, boolean includeGamma, double minPeriodicDistance, int minTotalKPoints, int maxDistinctKPoints, SYMMETRY_TYPE symmetryType) {
	  return getKPointLattice (structure.getDefiningSpaceGroup(), includeGamma, minPeriodicDistance, minTotalKPoints, maxDistinctKPoints, symmetryType);
  }
  
  protected KPointLattice getKPointLattice(SpaceGroup modifiedStructureSpace, boolean includeGamma, double minPeriodicDistance, int minTotalKPoints, int maxDistinctKPoints, SYMMETRY_TYPE symmetryType) {
	    long st = System.currentTimeMillis();
	    long st2 = System.currentTimeMillis();
	    
	    SpaceGroup arithmeticGroup = getArithmeticGroup(modifiedStructureSpace, symmetryType);
	    int numDimensions =arithmeticGroup.getLattice().numPeriodicVectors();
	    // System.out.println("getArithGroup @ getKPointLattice in KPointLibrary: " + (System.currentTimeMillis() - st2) + " ms.");
	    // We use the centrosymmetric factories even if time-reversal symmetry is disabled
	    // unless all symmetry is disabled
	    long st3 = System.currentTimeMillis();
	    
	    // Since we use centrosymmorphic collections for now. We can only load 
        // lattice with symmetry. Hence, although Structure_only, we need to add
        // inversion.
	    int groupNumber = (symmetryType == SYMMETRY_TYPE.STRUCTURE_ONLY) ? 
	        arithmeticGroup.getSymmorphicGroup(true).getArithmeticCrystalClassNumber(numDimensions) :
	        arithmeticGroup.getArithmeticCrystalClassNumber(numDimensions);
	    
	    //Status.detail("getGroupNumber: " + (System.currentTimeMillis() - st3) + " ms.");
	    IKPointFactory[][] factoryArray = includeGamma ? m_GammaFactories : m_ShiftedFactories;
	    //Status.detail("getKPointLattice in KPointLattice before return: " + (System.currentTimeMillis() - st) + " ms.");
	    
	    // There's no use looking for gamma lattices if we couldn't possibly beat the shifted result.
        int maxAllowedTotalKPoints = this.getMaxAllowedTotalKPoints(maxDistinctKPoints, arithmeticGroup, includeGamma);
        if (maxAllowedTotalKPoints < minTotalKPoints) {
          return null;
        }
        return factoryArray[numDimensions][groupNumber].getKPointLattice(arithmeticGroup, minPeriodicDistance, minTotalKPoints, Integer.MAX_VALUE);
	    
	    /*BravaisLattice reciprocalLattice = structure.getDefiningLattice().getInverseLattice();
	    reciprocalLattice = reciprocalLattice.translateTo(kPointLattice.getOrigin());
	    return kPointLattice.getPrimLattice().getSuperLattice(reciprocalLattice);*/
	    
	  }
  
  private int getMaxAllowedTotalKPoints(int maxDistinctKPoints, SpaceGroup arithmeticGroup, boolean includeGamma) {
      
      if (maxDistinctKPoints == Integer.MAX_VALUE) {
        return Integer.MAX_VALUE;
      }
      int gammaFactor = includeGamma ? 1 : 0;
      int numOps = arithmeticGroup.getLatticePointOperators(true).length;
      
      return (maxDistinctKPoints - gammaFactor) * numOps + gammaFactor;
      
    }
  
    public static SpaceGroup getArithmeticGroup(SpaceGroup structureSpaceGroup, 
            SYMMETRY_TYPE symmetryType) {

        boolean addInversion = (symmetryType == SYMMETRY_TYPE.ALL 
                             || symmetryType == SYMMETRY_TYPE.TIME_REVERSAL_ONLY);
        long a = System.currentTimeMillis();
        SpaceGroup arithmeticGroup = structureSpaceGroup.getSymmorphicGroup(addInversion);
        Status.detail("*getSymmorphicGroup: " + (System.currentTimeMillis() - a) + " ms.");
        // TODO: Make sure this works for less than 3 dimensions. op.isInversion() might need to 
        // be changed to something that only inverts periodic vectors.
        if (symmetryType == SYMMETRY_TYPE.TIME_REVERSAL_ONLY) {
            SymmetryOperation[] operations = new SymmetryOperation[2];
            for (int opNum = 0; opNum < arithmeticGroup.numOperations(); opNum++) {
                SymmetryOperation op = arithmeticGroup.getOperation(opNum);
                if (op.isIdentity()) { operations[0] = op; }
                if (op.isInversion() != null) { operations[1] = op; }
            }
            arithmeticGroup = new SpaceGroup(operations, arithmeticGroup.getLattice());
        } 
        if (symmetryType == SYMMETRY_TYPE.NONE) {
            SymmetryOperation[] operations = new SymmetryOperation[1];
            for (int opNum = 0; opNum < arithmeticGroup.numOperations(); opNum++) {
                SymmetryOperation op = arithmeticGroup.getOperation(opNum);
                if (op.isIdentity()) {operations[0] = op;}
            }
            arithmeticGroup = new SpaceGroup(operations, arithmeticGroup.getLattice());
        } 
        return arithmeticGroup;
    }
  
/*  private SpaceGroup getArithmeticGroup(Structure structure, SYMMETRY_TYPE symmetryType) {

    boolean addInversion = (symmetryType == SYMMETRY_TYPE.ALL || symmetryType == SYMMETRY_TYPE.TIME_REVERSAL_ONLY);
    long a = System.currentTimeMillis();
    SpaceGroup arithmeticGroup = structure.getDefiningSpaceGroup().getSymmorphicGroup(addInversion);
    System.out.println("*getSymmorphicGroup: " + (System.currentTimeMillis() - a) + " ms.");
    // TODO make sure this works for less than 3 dimensions.  op.isInversion() might need to be changed to something that only inverts periodic vectors
    if (symmetryType == SYMMETRY_TYPE.TIME_REVERSAL_ONLY) {
      SymmetryOperation[] operations = new SymmetryOperation[2];
      for (int opNum = 0; opNum < arithmeticGroup.numOperations(); opNum++) {
        SymmetryOperation op = arithmeticGroup.getOperation(opNum);
        if (op.isIdentity()) {operations[0] = op;}
        if (op.isInversion() != null) {operations[1] = op;}
      }
      arithmeticGroup = new SpaceGroup(operations, arithmeticGroup.getLattice());
    } 
    long b = System.currentTimeMillis();
    if (symmetryType == SYMMETRY_TYPE.NONE) {
      SymmetryOperation[] operations = new SymmetryOperation[1];
      for (int opNum = 0; opNum < arithmeticGroup.numOperations(); opNum++) {
        SymmetryOperation op = arithmeticGroup.getOperation(opNum);
        if (op.isIdentity()) {operations[0] = op;}
      }
      arithmeticGroup = new SpaceGroup(operations, arithmeticGroup.getLattice());
    } 
    System.out.println("*symOp: " + (System.currentTimeMillis() - b) + " ms.");
    return arithmeticGroup;
  }*/
  
  /**
   * 
   * @param structure The structure for which we are generating the k-point lattice
   * @param includeGamma True if the lattice should be gamma-centered, false if it is not.
   * @param minPeriodicDistance  Every k-point grid corresponds to a real-space supercell.
   * The min periodic distance is the shortest distance between two lattice points of that 
   * supercell.
   * @return The real-space superlattice that corresponds to the generated k-point lattice.
   */
  public SuperLattice getRealSuperLattice(Structure structure, INCLUDE_GAMMA includeGamma, double minPeriodicDistance, int minTotalKPoints, SYMMETRY_TYPE symmetryType) {
    
    SuperLattice kPointLattice = this.getKPointLattice(structure, includeGamma, minPeriodicDistance, minTotalKPoints, symmetryType).getReciprocalSpaceLattice();
    BravaisLattice realSuperLattice = kPointLattice.getPrimLattice().getInverseLattice();
    return structure.getDefiningLattice().getSuperLattice(realSuperLattice);
    
  }
  
  /**
   * 
   * @param spaceGroup The space group of the structure for which the k-point lattice
   * was generated.
   * @param kPointLattice  The reciprocal lattice for the structure, where the underlying
   * prim lattice is the k-point lattice.
   * @return  The number of distinct k-points in the Brillouin zone (or any unit cell of 
   * the k-point lattice).
   */
  /*public static int numDistinctKPoints(SpaceGroup spaceGroup, SuperLattice kPointLattice) {
    
    return numDistinctKPoints(getWeights(spaceGroup, kPointLattice));
  }*/
  
  /**
   * 
   * @param spaceGroup The space group of the structure for which the k-point lattice
   * was generated.
   * @param kPointLattice  The reciprocal lattice for the structure, where the underlying
   * prim lattice is the k-point lattice.
   * @return  The weights (unnormalized) for the given k-points when reduced by symmetry
   * operations.  The index of the weight array corresponds to the primCell number the 
   * corresponding k-point in kPointLattice.
   */
  /*public static double[] getWeights(SpaceGroup spaceGroup, SuperLattice kPointLattice, SYMMETRY_TYPE symmetryType) {
    
    double[] weights = new double[kPointLattice.numPrimCells()];
    SpaceGroup symmorphicGroup = spaceGroup.getSymmorphicGroup(true);
    
    for (int pointNum = 0; pointNum < kPointLattice.numPrimCells(); pointNum++) {
      Coordinates coords = kPointLattice.getPrimCellOriginCoords(pointNum);
      int minMappedKPointNum = Integer.MAX_VALUE;
      for (int opNum = 0; opNum < symmorphicGroup.numOperations(); opNum++) {
        SymmetryOperation op = symmorphicGroup.getOperation(opNum);
        Coordinates oppedCoords = op.operate(coords);
        int mappedPointNum = kPointLattice.getNearbyPrimCellOriginNumber(oppedCoords);
        minMappedKPointNum = Math.min(minMappedKPointNum, mappedPointNum);
      }
      weights[minMappedKPointNum]++;
    }
    
    return weights;
  }*/
  
  /**
   * 
   * @param weights A set of weights for k-points
   * @return The number of non-zero weights
   */
  /*public static int numDistinctKPoints(double[] weights) {

    int returnValue = 0;
    for (int pointNum = 0; pointNum < weights.length; pointNum++) {
      if (weights[pointNum] > 0) {returnValue++;}
    }
    return returnValue;
  }*/

  /*
  public void getTotalLatticeNum() throws JSONException, IOException {
		
	  Status.detail("There are a total of " + totalGammaLatticeNum + " gamma-centered lattices and " + totalShiftedLatticeNum + " shifted lattices.");
  }*/
}
