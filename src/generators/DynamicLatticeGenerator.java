/*
 * Created on Mar 24, 2019
 *
 */
package generators;

import matsci.location.Vector;
import matsci.location.basis.CartesianBasis;
import matsci.location.symmetry.operations.SpaceGroup;
import matsci.structure.BravaisLattice;
import matsci.structure.reciprocal.KPointLattice;
import matsci.structure.superstructure.SuperLattice;
import matsci.util.MSMath;

public class DynamicLatticeGenerator implements IKPointFactory {
  
  private KPointLibrary.INCLUDE_GAMMA m_IncludeGamma = KPointLibrary.INCLUDE_GAMMA.AUTO;
  private int m_MaxAllowedKPoints;
  private final int m_MaxScaleFactor = 3;

  public DynamicLatticeGenerator(KPointLibrary.INCLUDE_GAMMA includeGamma, int maxAllowedKPoints) {
    m_IncludeGamma = includeGamma;
    m_MaxAllowedKPoints = maxAllowedKPoints;
  }

  @Override
  public KPointLattice getKPointLattice(SpaceGroup spaceGroup, double minPeriodicDistance, int minTotalKPoints, int maxDistinctKPoints) {
    
    minPeriodicDistance -= CartesianBasis.getPrecision();
    
    SpaceGroup symmorphicGroup = spaceGroup.getSymmorphicGroup();
    KPointLatticeGenerator generator = new KPointLatticeGenerator(symmorphicGroup);
    generator.includeGamma(m_IncludeGamma);
    int numDimensions = symmorphicGroup.getLattice().numPeriodicVectors();
    int maxTotalKPoints = Math.max(maxDistinctKPoints * generator.numPointOperators3D(), maxDistinctKPoints); // To deal with overflow errors

    for (int scaleFactor = 1; scaleFactor <= m_MaxScaleFactor; scaleFactor++) {
    
      double minScaledDistance = minPeriodicDistance / scaleFactor;
      int minScaledTotalKPoints = (int) Math.ceil(minTotalKPoints / Math.pow(scaleFactor,  numDimensions));
      int maxScaledTotalKPoints = Math.min(m_MaxAllowedKPoints, (int) Math.floor(maxTotalKPoints / Math.pow(scaleFactor, numDimensions)));
      
      KPointLatticeGenerator.KPointLattice lattice = generator.getKPointLattice(minScaledDistance, minScaledTotalKPoints, maxScaledTotalKPoints, scaleFactor);
      if (lattice == null || lattice.getSuperToDirect() == null) {continue;}
      
      int[][] superToDirect = lattice.getSuperToDirect();
      SuperLattice superLattice = new SuperLattice(symmorphicGroup.getLattice(), superToDirect);
      BravaisLattice kPointLattice = superLattice.getInverseLattice();
      kPointLattice = kPointLattice.translateBy(new Vector(lattice.getShift(), kPointLattice.getLatticeBasis()));
      SuperLattice reciprocalLattice = new SuperLattice(kPointLattice, MSMath.transpose(superToDirect));
      
      return new KPointLattice(reciprocalLattice, symmorphicGroup.getOperations());
    }
    
    return null;
  }

}
