/*
 * Created on Jun 17, 2013
 *
 */
package generators;

import matsci.location.symmetry.operations.SpaceGroup;
import matsci.structure.reciprocal.KPointLattice;
import matsci.structure.superstructure.SuperLattice;

public interface IKPointFactory {

    /**
     * This method can be implemented in a way that dynamically generates k-point grids,
     * looks up existing k-point grids, or a combination of the two. 
     * 
     * @param spaceGroup  A space group for which we want to generate a k-point grid.
     * @param minPeriodicDistance  Every k-point grid corresponds to a real-space superlattice.
     * The minimum periodic distance is the shortest distance between two lattice points of that 
     * superlattice.
     * @return A reciprocal-space superlattice containing all of the k-points.  The
     * returned lattice will represent the repciprocal lattice of the given space group,
     * and the underlying primitive lattice (accessed by getPrimLattice()) will return 
     * the k-point lattice.
     */
    public KPointLattice getKPointLattice(SpaceGroup spaceGroup, double minPeriodicDistance,  
            int minTotalKPoints, int maxDistinctKPoints);
  
}
