#!/bin/bash

# This script runs all three 4 tests in the slim_test forlder and
# put output in one log file

cd ..
timestr=$( date +%b.%d.%k.%M )
log="./test_log/run.log.${timestr}"
touch $log
counter=0

starttime=$( date +%s )  # in seconds
for i in ./slim_testSet/[0-3]*; do
    echo "***********" $i "*********" | tee -a $log
    echo "" | tee -a $log

    while read -r path; do 
        echo "$path" | tee -a $log
        ./bin/GridGenerator $path 2>&1 | tee -a $log
        echo "" | tee -a $log
        let counter+=1
    done < $i/list.txt
   
    if [ -e $i/error_list.txt ]; then
        while read -r path; do
            echo "$path" | tee -a $log
            ./bin/GridGenerator $path 2>&1 | tee -a $log
            echo "" | tee -a $log
            let counter+=1
        done < $i/error_list.txt
    fi
done
endtime=$( date +%s ) 
echo "Running " $counter " cases takes " \
    $( echo "$endtime - $starttime" | bc -l ) " seconds." | tee -a $log


