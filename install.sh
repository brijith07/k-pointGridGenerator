#!/bin/bash

# Extract the matsci library
cd lib
jar xf matsci.jar
rm -r META-INF
if [ -e ../bin/matsci ]; then 
  rm -r ../bin/matsci; 
fi
mv ./matsci ../bin/matsci
cd ..

# Write the Manifest
if [ ! -e ./bin/META-INF ]; then
  mkdir -p ./bin/META-INF
fi
cat <<EOF > ./bin/META-INF/MANIFEST.MF
Manifest-Version: 1.0
Class-Path: .
Main-Class: GridGenerator

EOF

# Generate the executable jar
cd ./bin
jar cfm GridGenerator.jar ./META-INF/MANIFEST.MF ./*
mv GridGenerator.jar ..
cd ..


